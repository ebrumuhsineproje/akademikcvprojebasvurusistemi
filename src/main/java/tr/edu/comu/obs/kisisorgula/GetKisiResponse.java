
package tr.edu.comu.obs.kisisorgula;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sonuc" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sonuc"
})
@XmlRootElement(name = "getKisiResponse")
public class GetKisiResponse {

    protected boolean sonuc;

    /**
     * Gets the value of the sonuc property.
     * 
     */
    public boolean isSonuc() {
        return sonuc;
    }

    /**
     * Sets the value of the sonuc property.
     * 
     */
    public void setSonuc(boolean value) {
        this.sonuc = value;
    }

}
