
package tr.edu.comu.obs.kisisorgula;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="kullaniciAdi" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="parola" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "kullaniciAdi",
    "parola"
})
@XmlRootElement(name = "getKisiRequest")
public class GetKisiRequest {

    @XmlElement(required = true)
    protected String kullaniciAdi;
    @XmlElement(required = true)
    protected String parola;

    /**
     * Gets the value of the kullaniciAdi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKullaniciAdi() {
        return kullaniciAdi;
    }

    /**
     * Sets the value of the kullaniciAdi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKullaniciAdi(String value) {
        this.kullaniciAdi = value;
    }

    /**
     * Gets the value of the parola property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParola() {
        return parola;
    }

    /**
     * Sets the value of the parola property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParola(String value) {
        this.parola = value;
    }

}
