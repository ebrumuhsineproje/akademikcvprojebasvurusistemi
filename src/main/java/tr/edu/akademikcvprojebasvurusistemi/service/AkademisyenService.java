package tr.edu.akademikcvprojebasvurusistemi.service;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Grid;
import org.neo4j.cypher.internal.frontend.v2_3.ast.functions.Str;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.template.Neo4jOperations;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tr.edu.akademikcvprojebasvurusistemi.Util;
import tr.edu.akademikcvprojebasvurusistemi.domain.*;
import tr.edu.akademikcvprojebasvurusistemi.domain.Akademisyen;
import tr.edu.akademikcvprojebasvurusistemi.exceptions.AkademisyenZatenEkliException;
import tr.edu.akademikcvprojebasvurusistemi.repository.*;

import java.util.Date;
import java.util.List;


@Service
@Transactional
public class AkademisyenService {

    @Autowired
    AkademisyenRepository akademisyenRepository;

    @Autowired
    GorevliRepository gorevliRepository;

    @Autowired
    LiseRepository liseRepository;

    @Autowired
    LisansRepository lisansRepository;

    @Autowired
    YuksekLisansRepository yuksekLisansRepository;

    @Autowired
    DoktoraRepository doktoraRepository;

    @Autowired
    YayinRepository yayinRepository;

    @Autowired
    ProjeRepository projeRepository;

    @Autowired
    Neo4jOperations neo4jOperations;


    public static <T> void refreshGrid(List<T> tList, final Class<T> classType, Grid grid) {
        BeanItemContainer<T> tBeanItemContainer;
        tBeanItemContainer = Util.getFilledBeanItemContainer(classType, tList);
        grid.setContainerDataSource(tBeanItemContainer);
    }


    public List<Akademisyen> getTumAkademisyenler() {
        return akademisyenRepository.getTumAkademisyenler();
    }

    public void iliskisil(Long ID, Long Id) {
        projeRepository.iliskisil(ID, Id);
    }

    public void iliskisil2(Long aid, Long pid) {
        projeRepository.iliskisil2(aid, pid);
    }

    public void iliskisil3(Long aid, Long pid) {
        projeRepository.iliskisil3(aid, pid);
    }

    public void iliskisil4(Long aid, Long pid) {
        projeRepository.iliskisil4(aid, pid);
    }

    public void iliskisil5(Long aid, Long pid) {
        projeRepository.iliskisil5(aid, pid);
    }

    public void iliskisil6(Long aid, Long pid) {
        projeRepository.iliskisil6(aid, pid);
    }

    public Iterable<Proje> guncelle1(Long aid, Long pid) {
        return projeRepository.guncelle1(aid, pid);
    }

    public List<String> iliskigetir(Long ID, Long Id) {
        return projeRepository.iliskigetir(ID, Id);
    }

    public void iliskiyarat5(Long ID, Long id) {
        projeRepository.iliskiyarat5(ID, id);
    }

    public void iliskiyaratproje(Long ID,Long id){projeRepository.iliskiyaratproje(ID,id);}


    public void iliskiyarat1(Long ID, Long id) {
        projeRepository.iliskiyarat1(ID, id);
    }

    public void iliskiyarat2(Long ID, Long id) {
        projeRepository.iliskiyarat2(ID, id);
    }

    public void iliskiyarat3(Long ID, Long id) {
        projeRepository.iliskiyarat3(ID, id);
    }

    public void iliskiyarat4(Long ID, Long id) {
        projeRepository.iliskiyarat4(ID, id);
    }

    public List<Proje> tumprojeler() {
        return projeRepository.tumprojeler();
    }

    public List<Proje> tumkabuledilenler()
    {
        return projeRepository.tumkabuledilenler();
    }

    public List<Proje> onayliprojeler(Long ID) {
        return projeRepository.onaylanmisprojeler(ID);
    }

    public List<Proje> kabuledilmisprojeler(Long ID){return projeRepository.kabuledilmisprojeler(ID);}
    public List<Proje> onayagoregetir(String onaydurumu) {

        return projeRepository.onayagoregetir(onaydurumu);
    }

    public List<Proje> turegoregetir(String projeturu) {

        return projeRepository.turegoregetir(projeturu);
    }

    public List<Proje> turveonayagoregetir(String onay, String projeturu) {
        return projeRepository.onayvedurum(onay, projeturu);
    }

    public List<Proje> turkabul( String projeturu) {
        return projeRepository.turkabul( projeturu);
    }



    public List<Proje> onayveyaimza(Long ID) {
        return projeRepository.onayveyaimza(ID);
    }

    public Yayinlar yayinGetir(Long ID) {
        return yayinRepository.akademisyeneaityayinlariyayinGetir(ID);
    }

    public List<Yayinlar> adaveakademisyenegoregetir(Long id) {
        return yayinRepository.tumyayinlarinadinigetir(id);
    }


    public List<Yayinlar> gettumyayinlar(Long ID) {
        return yayinRepository.tumyayinlarigetir(ID);
    }

    public Yayinlar yayingetir(Long id) {
        return yayinRepository.yayingetir(id);
    }

    public Yayinlar yayinguncelleicingetir(Yayinlar yayinlar) {
        return yayinRepository.yayingetir(yayinlar.getId());
    }


    public List<Lise> gettumliseler(Long ID) {
        return liseRepository.akademisyeneaitliseler(ID);
    }

    public List<Lisans> gettumlisanslar(Long ID) {
        return lisansRepository.akademisyeneaitlisanslarigetir(ID);
    }

    public List<YuksekLisans> gettumyuksekler(Long ID) {
        return yuksekLisansRepository.akademisyeneaityukseklisanslarigetir(ID);
    }

    public List<Doktora> gettumdoktoralar(Long ID) {
        return doktoraRepository.akademisyeneaitdoktoralarigetir(ID);
    }

    public List<Proje> gettumprojeler(Long ID) {
        return projeRepository.akademisyeneaitprojeler(ID);
    }

    public List<Proje> ekibigetir2(Akademisyen akademisyen) {
        return projeRepository.ekibigetir2(akademisyen.getId());
    }



    public List<Akademisyen> ekibigetir(Proje proje) {
        return projeRepository.ekibigetir(proje.getId());
    }

    public Lise teklisegetir(Lise lise) {
        return liseRepository.teklisegetir(lise.getId());
    }


    public Lisans teklisansgetir(Lisans lisans) {
        return lisansRepository.teklisansgetir(lisans.getId());
    }

    public YuksekLisans tekyuksekgetir(YuksekLisans yuksekLisans) {
        return yuksekLisansRepository.tekyuksekgetir(yuksekLisans.getId());
    }

    public Doktora tekdoktoragetir(Doktora doktora) {
        return doktoraRepository.tekdoktoragetir(doktora.getId());
    }

    public Proje tekprojegetir(Proje proje) {
        return projeRepository.tekprojegetir(proje.getId());
    }


    public void yayinsil(List<Yayinlar> yayinlar) {
        for (Yayinlar yayinlar1 : yayinlar) {
            neo4jOperations.delete(yayingetir(yayinlar1.getId()));
        }
    }

    public void lisanssil(List<Lisans> lisanslar) {
        for (Lisans lisans1 : lisanslar) {
            neo4jOperations.delete(teklisansgetir(lisans1));
        }
    }

    public void yukseksil(List<YuksekLisans> yukseklisanslar) {
        for (YuksekLisans yukseklisans1 : yukseklisanslar) {
            neo4jOperations.delete(tekyuksekgetir(yukseklisans1));
        }
    }

    public void lisesil(List<Lise> liseler) {
        for (Lise lise1 : liseler) {
            neo4jOperations.delete(teklisegetir(lise1));
        }
    }

    public void doktorasil(List<Doktora> doktoralar) {
        for (Doktora doktora1 : doktoralar) {
            neo4jOperations.delete(tekdoktoragetir(doktora1));
        }
    }

    public void projesil(List<Proje> projeler) {
        for (Proje proje1 : projeler) {
            neo4jOperations.delete(tekprojegetir(proje1));
        }
    }


    public void yayinguncelle(Yayinlar yayinlar) {
        yayinRepository.yayingetir(yayinlar.getId());
        Date simdikiZaman = new Date();
        yayinlar.setSimdikiZaman(simdikiZaman);
        yayinRepository.save(yayinlar);

    }

    public void liseguncelle(Lise lise) {

        //liseRepository.teklisegetir(lise.getId());
        liseRepository.liseGetir(lise.getId());

        liseRepository.save(lise);
    }

    public void lisansguncelle(Lisans lisans) {
        lisansRepository.lisansGetir(lisans.getId());
        lisansRepository.save(lisans);
    }

    public void yukseklisansguncelle(YuksekLisans yukseklisans) {
        yuksekLisansRepository.yukseklisansGetir(yukseklisans.getId());
        yuksekLisansRepository.save(yukseklisans);
    }

    public void doktoraguncelle(Doktora doktora) {
        doktoraRepository.doktoraGetir(doktora.getId());
        doktoraRepository.save(doktora);
    }

    public void projeguncelle(Proje proje) {
        projeRepository.projegetir(proje.getId());
        projeRepository.save(proje);
    }


    public List<Yayinlar> gettumyayinlaradi(Long Id) {
        return yayinRepository.tumyayinlarinadinigetir(Id);
    }

    public void ekleAkademisyen(Akademisyen akademisyen) throws AkademisyenZatenEkliException {
        if (akademisyenVarMi(akademisyen.getTc()) != null) {
            throw new AkademisyenZatenEkliException();
        } else {
            akademisyenRepository.save(akademisyen);
        }
    }

    public Akademisyen akademisyenVarMi(String akademisyenTc) {
        return akademisyenRepository.akademisyenVarMi(akademisyenTc);
    }

    public Yayinlar yayinvarmi(String yayinadi) {
        return yayinRepository.yayinvarmi(yayinadi);
    }

    public Proje projevarmi(String projeadi) {
        return projeRepository.projevarmi(projeadi);
    }

    public Lise liseGetir(Long id) {
        return liseRepository.liseGetir(id);
    }

    public Lisans lisansGetir(Long akademisyenId) {

        return lisansRepository.lisansGetir(akademisyenId);
    }

    public YuksekLisans yukseklisansGetir(Long akademisyenID) {
        return yuksekLisansRepository.yukseklisansGetir(akademisyenID);
    }

    public Doktora doktoraGetir(Long akademisyenId) {

        return doktoraRepository.doktoraGetir(akademisyenId);
    }


    /**
     * @param email getirmek istedi�iniz akademisyenin mail adresini giriniz.
     */
    public Akademisyen akademisyenGetir(String email) {
        return akademisyenRepository.akademisyenGetir(email);

    }


    public Akademisyen adagoreakademisyengetir(String ad) {
        return akademisyenRepository.adagoregetir(ad);
    }

    public List<Akademisyen> adsoyadbolumegoregetir(String ad,String soyad,String bolum) {
        return akademisyenRepository.adsoyadbolumegoregetir(ad,soyad,bolum);
    }
    public List<Akademisyen> adasoyadagoregetir(String ad,String soyad) {
        return akademisyenRepository.adasoyadagoregetir(ad,soyad);
    }
    public List<Akademisyen> adabolume(String ad,String bolum) {
        return akademisyenRepository.adabolume(ad,bolum);
    }
    public List<Akademisyen> soyadabolume(String soyad,String bolum) {
        return akademisyenRepository.soyadabolume(soyad,bolum);
    }
    public List <Akademisyen>soyadagoregetir(String soyad) {
        return akademisyenRepository.soyadagoregetir(soyad);
    }

    public List <Akademisyen>adagore2(String ad) {
        return akademisyenRepository.adagore2(ad);
    }

    public List<Akademisyen> bolumegoregetir(String bolum) {
        return akademisyenRepository.bolumegoregetir(bolum);
    }


    public Gorevli gorevliGetir(String email) {
        return gorevliRepository.gorevliGetir(email);
    }


    public Gorevli gorevligetir(String tc) {
        return gorevliRepository.gorevligetir(tc);
    }

    public List<Yayinlar> adagoregetir(String ad) {
        return yayinRepository.adagoregetir(ad);
    }
    public List<Yayinlar> turegoreyayingetir(String tur,Long id) {
        return yayinRepository.turegoregetir(tur,id);
    }

    public void onayla(List<Proje> projeler) {
        for (Proje proje1 : projeler) {

            proje1.setOnaydurumu("onaylanmış");
            projeRepository.save(proje1);
        }
    }

    public void kabulet(List<Proje> projeler) {
        for (Proje proje1 : projeler) {

            proje1.setOnaydurumu("KABUL EDİLMİŞ");
            projeRepository.save(proje1);
        }
    }


    public void imzabekliyor(List<Proje> projeler) {
        for (Proje proje1 : projeler) {

            proje1.setOnaydurumu("imza bekliyor");
            projeRepository.save(proje1);
        }
    }

    public Akademisyen akademisyengetir(String tc) {
        return akademisyenRepository.akademisyengetir(tc);

    }


    public Akademisyen akademisyenEkle(Akademisyen ak) {
        return akademisyenRepository.save(ak);
    }

    public Gorevli gorevliekle(Gorevli go) {
        return gorevliRepository.save(go);
    }


    public boolean Control() {
        int kactane = akademisyenRepository.getTumAkademisyenler().size();
        if (kactane == 0) return true;
        return false;
    }

    public boolean Control2() {
        int kac = yayinRepository.tumyayinlar().size();
        if (kac == 0) return true;
        return false;
    }


}
