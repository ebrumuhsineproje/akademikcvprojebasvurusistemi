package tr.edu.akademikcvprojebasvurusistemi;

import com.vaadin.data.util.BeanItemContainer;

import java.text.CollationElementIterator;
import java.util.Collection;

/**
 * Created by LENOVO on 25.1.2017.
 */
public class Util {
    public  static <T>BeanItemContainer<T> getFilledBeanItemContainer(final Class<T> cla, Collection<T> c){
        final BeanItemContainer<T>bic=new BeanItemContainer<T>(cla);
        bic.addAll(c);
        return bic;
    }
}
