package tr.edu.akademikcvprojebasvurusistemi.raporlama;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LENOVO on 28.2.2017.
 */
public class ProjeModelFactory {

    public static List<ProjeModel> get() {

        ProjeModel projeModel = new ProjeModel();
        projeModel.setAdi("ebru");
        projeModel.setProjeadi("proje adı");
        projeModel.setOzeti("özet");

        List<ProjeModel> projeModels = new ArrayList<>();
        projeModels.add(projeModel);

        return projeModels;
    }
}
