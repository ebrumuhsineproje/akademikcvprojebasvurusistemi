package tr.edu.akademikcvprojebasvurusistemi.raporlama;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by LENOVO on 28.2.2017.
 */
@NoArgsConstructor
@Getter
@Setter
public class ProjeModel {
    private String projePatenti , projeadi, ozeti, ar_geNiteligi, projeKapsamaAlani, projeTuru, sanayiIsbirligiDurumu, paraBirimi, projedeAlinanGorev, baslangic, bitis, ulke, sehir, projeButcesi, KurumKatkiPayi, adi, soyadi, tc, sifre, email, birim, bolum, anabilimdali, kadrounvani, ceptel, istel;
}
