package tr.edu.akademikcvprojebasvurusistemi;

import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.session.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.neo4j.config.Neo4jConfiguration;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = {"tr.edu.akademikcvprojebasvurusistemi"})
@EnableTransactionManagement
@EnableNeo4jRepositories(basePackages = {"tr.edu.akademikcvprojebasvurusistemi"})
public class Neo4jConfig extends Neo4jConfiguration {
   @Bean
    public org.neo4j.ogm.config.Configuration getConfiguration(){
        org.neo4j.ogm.config.Configuration configuration = new org.neo4j.ogm.config.Configuration();
        configuration.driverConfiguration()
                .setDriverClassName("org.neo4j.ogm.drivers.bolt.driver.BoltDriver")
                .setURI("bolt://localhost:7687")
                .setCredentials("neo4j","neo4j1")
                .setEncryptionLevel("NONE");

        return configuration;
    }

    @Override
    public SessionFactory getSessionFactory() {
        return new SessionFactory(getConfiguration() , "tr.edu.akademikcvprojebasvurusistemi.domain");
    }


}
