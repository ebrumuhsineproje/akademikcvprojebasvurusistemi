package tr.edu.akademikcvprojebasvurusistemi.vaadin;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.template.Neo4jOperations;
import tr.edu.akademikcvprojebasvurusistemi.service.AkademisyenService;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.views.MyErrorView;

import java.util.Locale;


@SpringUI(path = "")
@Theme("cTheme")
public class MainUI extends UI {

    @Autowired
    AkademisyenService akademisyenService;

    @Autowired
    Neo4jOperations neo4jOperations;

    @Autowired
    private SpringViewProvider viewProvider;



    @Override
    protected void init(VaadinRequest vaadinRequest) {
        setSizeFull();
        //akademisyenKaydet();

        setLocale(new Locale("tr" , "TR"));
        final VerticalLayout root = new VerticalLayout();
        root.setSizeFull();
        root.setMargin(false);
        root.setSpacing(true);
        setContent(root);
        final Panel viewContainer = new Panel();
        viewContainer.setSizeFull();
        root.addComponent(viewContainer);
        root.setExpandRatio(viewContainer, 1.0f);

        Navigator navigator = new Navigator(this, viewContainer);
        navigator.addProvider(viewProvider);

        navigator.setErrorView(MyErrorView.class);
    }
}
