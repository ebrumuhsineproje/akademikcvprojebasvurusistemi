/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.declarative.Design;
import tr.edu.akademikcvprojebasvurusistemi.service.AkademisyenService;

/**
 *
 * @author ubuntu
 */
@DesignRoot
public class DeclarativeLiseKaydet extends Panel{
    public ComboBox liseadi;
    public DateField lisebaslangic,lisebitis;
    public Button btnLiseKaydet;
    
    public DeclarativeLiseKaydet() {
        Design.read(this);
        setSizeUndefined();
        btnLiseKaydet.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        btnLiseKaydet.setIcon(FontAwesome.CHECK);

        liseadi.setRequired(false);
        liseadi.setRequiredError("lise Seçmelisiniz!");

        lisebaslangic.setRequired(false);
        lisebaslangic.setRequiredError("başlangıç tarihi Seçmelisiniz!");

        lisebitis.setRequired(false);
        lisebitis.setRequiredError("bitiş tarihi Seçmelisiniz!");








    }
    
}
