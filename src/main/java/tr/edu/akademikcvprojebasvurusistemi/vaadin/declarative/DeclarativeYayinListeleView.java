package tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.*;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import com.vaadin.ui.declarative.Design;
import com.vaadin.ui.themes.ValoTheme;
import org.neo4j.csv.reader.SourceTraceability;
import org.springframework.beans.factory.annotation.Autowired;
import tr.edu.akademikcvprojebasvurusistemi.domain.Akademisyen;
import tr.edu.akademikcvprojebasvurusistemi.domain.Yayinlar;
import tr.edu.akademikcvprojebasvurusistemi.exceptions.AkademisyenZatenEkliException;
import tr.edu.akademikcvprojebasvurusistemi.service.AkademisyenService;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.extras.MyNotification;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.extras.enums.MessageType;

import java.awt.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

import static com.sun.xml.internal.fastinfoset.alphabet.BuiltInRestrictedAlphabets.table;

/**
 * Created by LENOVO on 16.12.2016.
 */

@DesignRoot
public class DeclarativeYayinListeleView extends Panel {
    public Grid tumyayinlar,kitaplar,makaleler;
    public Button btnyayinguncelle, btnyayinsil,kaydetyayin;
    public TextField ara;
    public  Button btnara;



    /*
        public DeclarativeYayinListeleView(){


           Design.read(this);
           setSizeFull();

           tumyayinlar.setSelectionMode(Grid.SelectionMode.SINGLE);
           tumyayinlar.setStyleName("mygrid");
           btnyayinguncelle.setIcon(FontAwesome.EDIT);
           btnyayinsil.setIcon(FontAwesome.CLOSE);


        }*/
    @Autowired
    AkademisyenService akademisyenService;
    public DeclarativeYayinListeleView(AkademisyenService _akademisyen) {
        setSizeFull();
        Akademisyen a = (Akademisyen) VaadinSession.getCurrent().getSession().getAttribute("akademisyen");


        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        akademisyenService = _akademisyen;
        Design.read(this);

        tumyayinlar.setSelectionMode(Grid.SelectionMode.SINGLE);
        tumyayinlar.setSizeFull();
        tumyayinlar.setVisible(false);

        kitaplar.setSelectionMode(Grid.SelectionMode.SINGLE);
        kitaplar.setSizeFull();

        makaleler.setSelectionMode(Grid.SelectionMode.SINGLE);
        makaleler.setSizeFull();

        btnyayinguncelle.setIcon(FontAwesome.EDIT);
        btnyayinsil.setIcon(FontAwesome.CLOSE);
        ara.setInputPrompt("ada göre ara!");


        java.util.List<Yayinlar> yayinlarList = null;
        yayinlarList = akademisyenService.gettumyayinlar(a.getId());


        BeanItemContainer<Yayinlar> yayinlarBeanItemContainer = new BeanItemContainer<>(Yayinlar.class);
        tumyayinlar.setContainerDataSource(yayinlarBeanItemContainer);
        tumyayinlar.getContainerDataSource().removeAllItems();
        yayinlarBeanItemContainer.addAll(yayinlarList);
        tumyayinlar.setContainerDataSource(yayinlarBeanItemContainer);

        tumyayinlar.removeAllColumns();
        tumyayinlar.addColumn("yayinadi");
        tumyayinlar.addColumn("simdikiZaman");
        tumyayinlar.getColumn("yayinadi").setHeaderCaption("YAYIN ADI");
        tumyayinlar.getColumn("simdikiZaman").setHeaderCaption("YAYINLANMA ZAMANI");

        if(akademisyenService.turegoreyayingetir("kitap",a.getId()).size()==0){
            kitaplar.setVisible(false);
        }
        else{
            java.util.List<Yayinlar> yayinlarList2 = null;
            yayinlarList2 = akademisyenService.turegoreyayingetir("kitap", a.getId());
            BeanItemContainer<Yayinlar> yayinlarBeanItemContainer2 = new BeanItemContainer<>(Yayinlar.class);
            kitaplar.setContainerDataSource(yayinlarBeanItemContainer2);
            kitaplar.getContainerDataSource().removeAllItems();
            yayinlarBeanItemContainer2.addAll(yayinlarList2);
            kitaplar.setContainerDataSource(yayinlarBeanItemContainer2);

            kitaplar.removeAllColumns();
            kitaplar.addColumn("ulusallik");
            kitaplar.addColumn("kbaslik");
            kitaplar.addColumn("ktur");
            kitaplar.addColumn("kkonu");
            kitaplar.getColumn("ulusallik").setHeaderCaption("YAYIN TİPİ");
            kitaplar.getColumn("kbaslik").setHeaderCaption("KİTAP BAŞLIĞI");
            kitaplar.getColumn("ktur").setHeaderCaption("KİTAP TÜRÜ");
            kitaplar.getColumn("kkonu").setHeaderCaption("KİTAP KONUSU");


        }
        if(akademisyenService.turegoreyayingetir("dergide makale",a.getId()).size()==0){
            makaleler.setVisible(false);
        }else{
            java.util.List<Yayinlar> yayinlarList3 = null;
            yayinlarList3 =  akademisyenService.turegoreyayingetir("dergide makale",a.getId());
            BeanItemContainer<Yayinlar> yayinlarBeanItemContainer3 = new BeanItemContainer<>(Yayinlar.class);
            makaleler.setContainerDataSource(yayinlarBeanItemContainer3);
            makaleler.getContainerDataSource().removeAllItems();
            yayinlarBeanItemContainer3.addAll(yayinlarList3);
            makaleler.setContainerDataSource(yayinlarBeanItemContainer3);

            makaleler.removeAllColumns();
            makaleler.addColumn("ulusallik");
            makaleler.addColumn("mbaslik");
            makaleler.addColumn("mtur");
            makaleler.addColumn("mkonu");
            makaleler.getColumn("ulusallik").setHeaderCaption("YAYIN TİPİ");
            makaleler.getColumn("mbaslik").setHeaderCaption("MAKALE BAŞLIĞI");
            makaleler.getColumn("mtur").setHeaderCaption("MAKALE TÜRÜ");
            makaleler.getColumn("mkonu").setHeaderCaption("MAKALE KONUSU");

        }

        //declarativeYayinListeleView.tumyayinlar.getColumn("id").setHidden(true);


        btnyayinsil.addClickListener(clickEvent -> {

            Collection<Object> seciliYayinlar = kitaplar.getSelectedRows();
            Collection<Object> seciliYayinlar2=makaleler.getSelectedRows();
            if (seciliYayinlar.size() != 0) {
                List<Yayinlar> yayinlars = new ArrayList<>();

                for (Object o : seciliYayinlar) {
                    yayinlars.add((Yayinlar) o);
                }
                akademisyenService.yayinsil(yayinlars);
                new MyNotification("silme başarılı", MessageType.SUCCESS);
                java.util.List<Yayinlar> yeni = null;
                yeni = akademisyenService.turegoreyayingetir("kitap",a.getId());


                akademisyenService.refreshGrid(yeni,Yayinlar.class,kitaplar);

            }
            else if (seciliYayinlar2.size() != 0) {
                List<Yayinlar> yayinlars = new ArrayList<>();

                for (Object o : seciliYayinlar2) {
                    yayinlars.add((Yayinlar) o);
                }
                akademisyenService.yayinsil(yayinlars);
                new MyNotification("silme başarılı", MessageType.SUCCESS);
                java.util.List<Yayinlar> yeni = null;
                yeni = akademisyenService.turegoreyayingetir("dergide makale",a.getId());


                akademisyenService.refreshGrid(yeni,Yayinlar.class,makaleler);

            }else {
                new MyNotification("silmek istediğiniz yayını seçin", MessageType.FAIL);
            }
            //declarativeYayinListeleView.tumyayinlar.getSelectionModel().reset();


        });
        btnyayinguncelle.addClickListener(clickEvent -> {
            DeclarativeYeniYayinGuncelle declarativeYeniYayinGuncelle=new DeclarativeYeniYayinGuncelle();
            Yayinlar seciliyayin= (Yayinlar) kitaplar.getSelectedRow();
            Yayinlar seciliyayin2= (Yayinlar) makaleler.getSelectedRow();

            if(seciliyayin!=null){
                declarativeYeniYayinGuncelle.pnlkitap.setVisible(true);
                declarativeYeniYayinGuncelle.kbaslik.setValue(akademisyenService.yayinguncelleicingetir(seciliyayin).getKbaslik().toString());
                declarativeYeniYayinGuncelle.kbaski.setValue(akademisyenService.yayinguncelleicingetir(seciliyayin).getKbaski().toString());
                declarativeYeniYayinGuncelle.kdil.setValue(akademisyenService.yayinguncelleicingetir(seciliyayin).getKdil().toString());
                declarativeYeniYayinGuncelle.kkonu.setValue(akademisyenService.yayinguncelleicingetir(seciliyayin).getKkonu().toString());
                declarativeYeniYayinGuncelle.kisbn.setValue(akademisyenService.yayinguncelleicingetir(seciliyayin).getKisbn().toString());
                declarativeYeniYayinGuncelle.klink.setValue(akademisyenService.yayinguncelleicingetir(seciliyayin).getKlink().toString());
                declarativeYeniYayinGuncelle.kdoi.setValue(akademisyenService.yayinguncelleicingetir(seciliyayin).getKdoi().toString());
                declarativeYeniYayinGuncelle.krol.setValue(akademisyenService.yayinguncelleicingetir(seciliyayin).getKrol().toString());
                declarativeYeniYayinGuncelle.ksayfa.setValue(akademisyenService.yayinguncelleicingetir(seciliyayin).getKsayfa().toString());
                declarativeYeniYayinGuncelle.ksehir.setValue(akademisyenService.yayinguncelleicingetir(seciliyayin).getKsehir().toString());
                declarativeYeniYayinGuncelle.kulke.setValue(akademisyenService.yayinguncelleicingetir(seciliyayin).getKulke().toString());
                declarativeYeniYayinGuncelle.ktur.setValue(akademisyenService.yayinguncelleicingetir(seciliyayin).getKtur().toString());
                declarativeYeniYayinGuncelle.kyayinevi.setValue(akademisyenService.yayinguncelleicingetir(seciliyayin).getKyayinevi().toString());
                declarativeYeniYayinGuncelle.kyayinevigrup.setValue(akademisyenService.yayinguncelleicingetir(seciliyayin).getKyayinevigrup().toString());

                if(akademisyenService.yayinguncelleicingetir(seciliyayin).getUlusallik().toString().equals("ulusal")){
                    declarativeYeniYayinGuncelle.ulusal.setValue(true);
                    declarativeYeniYayinGuncelle.uluslararasi.setReadOnly(true);
                }
                else{
                    declarativeYeniYayinGuncelle.uluslararasi.setValue(true);
                    declarativeYeniYayinGuncelle.ulusal.setReadOnly(true);
                }
                declarativeYeniYayinGuncelle.yayinturu.setValue("kitap");

                declarativeYeniYayinGuncelle.kbasim.setDateFormat("dd-MM-yyyy");
                Date date = null;
                try {
                    date = simpleDateFormat.parse(akademisyenService.yayinguncelleicingetir(seciliyayin).getKbasim());
                    declarativeYeniYayinGuncelle.kbasim.setValue(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                declarativeYeniYayinGuncelle.ksonbaski.setDateFormat("dd-MM-yyyy");
                Date date2 = null;
                try {
                    date2 = simpleDateFormat.parse(akademisyenService.yayinguncelleicingetir(seciliyayin).getKsonbaski());
                    declarativeYeniYayinGuncelle.ksonbaski.setValue(date2);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                final Window window = new Window();
                window.setModal(true);
                window.setWidth(630.0f, Unit.PIXELS);
                window.setHeight(650.0f, Unit.PIXELS);
                window.setDraggable(false);
                window.setResizable(false);
                window.setContent(declarativeYeniYayinGuncelle);

                UI.getCurrent().addWindow(window);
            }


            else if(seciliyayin2!=null){
                declarativeYeniYayinGuncelle.pnlmakale1.setVisible(true);
                declarativeYeniYayinGuncelle.dtur.setValue(akademisyenService.yayinguncelleicingetir(seciliyayin2).getDtur().toString());
                declarativeYeniYayinGuncelle.dadi.setValue(akademisyenService.yayinguncelleicingetir(seciliyayin2).getDadi().toString());
                declarativeYeniYayinGuncelle.mbaslik.setValue(akademisyenService.yayinguncelleicingetir(seciliyayin2).getMbaslik().toString());
                declarativeYeniYayinGuncelle.mdil.setValue(akademisyenService.yayinguncelleicingetir(seciliyayin2).getMdil().toString());
                declarativeYeniYayinGuncelle.mdoi.setValue(akademisyenService.yayinguncelleicingetir(seciliyayin2).getMdoi().toString());
                declarativeYeniYayinGuncelle.mkonu.setValue(akademisyenService.yayinguncelleicingetir(seciliyayin2).getMkonu().toString());
                declarativeYeniYayinGuncelle.mlink.setValue(akademisyenService.yayinguncelleicingetir(seciliyayin2).getMlink().toString());
                declarativeYeniYayinGuncelle.mtur.setValue(akademisyenService.yayinguncelleicingetir(seciliyayin2).getMtur().toString());
                declarativeYeniYayinGuncelle.btur.setValue(akademisyenService.yayinguncelleicingetir(seciliyayin2).getBtur().toString());

                declarativeYeniYayinGuncelle.cilt.setValue(akademisyenService.yayinguncelleicingetir(seciliyayin2).getCilt().toString());
                declarativeYeniYayinGuncelle.sayi.setValue(akademisyenService.yayinguncelleicingetir(seciliyayin2).getSayi().toString());
                declarativeYeniYayinGuncelle.no.setValue(akademisyenService.yayinguncelleicingetir(seciliyayin2).getNo().toString());

                declarativeYeniYayinGuncelle.btarih.setDateFormat("dd-MM-yyyy");
                Date date = null;
                try {
                    date = simpleDateFormat.parse(akademisyenService.yayinguncelleicingetir(seciliyayin2).getBtarih());
                    declarativeYeniYayinGuncelle.btarih.setValue(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if(akademisyenService.yayinguncelleicingetir(seciliyayin2).getUlusallik().toString().equals("ulusal")){
                    declarativeYeniYayinGuncelle.ulusal.setValue(true);
                    declarativeYeniYayinGuncelle.uluslararasi.setReadOnly(true);
                }
                else{
                    declarativeYeniYayinGuncelle.uluslararasi.setValue(true);
                    declarativeYeniYayinGuncelle.ulusal.setReadOnly(true);
                }
                declarativeYeniYayinGuncelle.yayinturu.setValue("dergide makale");

                final Window window = new Window();
                window.setModal(true);
                window.setWidth(630.0f, Unit.PIXELS);
                window.setHeight(650.0f, Unit.PIXELS);
                window.setDraggable(false);
                window.setResizable(false);
                window.setContent(declarativeYeniYayinGuncelle);

                UI.getCurrent().addWindow(window);

            }
            else{

                new MyNotification("güncellemek istediğiniz yayını seçin", MessageType.FAIL);

            }


                    declarativeYeniYayinGuncelle.btnkaydet.addClickListener(new Button.ClickListener() {
                        @Override
                        public void buttonClick(Button.ClickEvent clickEvent) {

                            if(seciliyayin!=null) {


                                seciliyayin.kbaslik = declarativeYeniYayinGuncelle.kbaslik.getValue().toString();
                                seciliyayin.kdil = declarativeYeniYayinGuncelle.kdil.getValue().toString();
                                seciliyayin.ksehir = declarativeYeniYayinGuncelle.ksehir.getValue().toString();
                                seciliyayin.kulke = declarativeYeniYayinGuncelle.kulke.getValue().toString();
                                seciliyayin.kdoi = declarativeYeniYayinGuncelle.kdoi.getValue().toString();
                                seciliyayin.kisbn = declarativeYeniYayinGuncelle.kisbn.getValue().toString();
                                seciliyayin.klink = declarativeYeniYayinGuncelle.klink.getValue().toString();
                                seciliyayin.kkonu = declarativeYeniYayinGuncelle.kkonu.getValue().toString();
                                seciliyayin.krol = declarativeYeniYayinGuncelle.krol.getValue().toString();
                                seciliyayin.ksayfa = declarativeYeniYayinGuncelle.ksayfa.getValue().toString();
                                seciliyayin.ktur = declarativeYeniYayinGuncelle.ktur.getValue().toString();
                                seciliyayin.kyayinevi = declarativeYeniYayinGuncelle.kyayinevi.getValue().toString();
                                seciliyayin.kyayinevigrup = declarativeYeniYayinGuncelle.kyayinevigrup.getValue().toString();
                                seciliyayin.kbaski = declarativeYeniYayinGuncelle.kbaski.getValue().toString();


                                seciliyayin.kbasim = simpleDateFormat.format(declarativeYeniYayinGuncelle.kbasim.getValue());
                                seciliyayin.ksonbaski = simpleDateFormat.format(declarativeYeniYayinGuncelle.ksonbaski.getValue());

                                akademisyenService.yayinguncelle(seciliyayin);


                                Iterator<Window> window = getUI().getWindows().iterator();

                                getUI().removeWindow(window.next());
                                new MyNotification("guncellme başarılı", MessageType.SUCCESS);
                                java.util.List<Yayinlar> yeniguncel = null;
                                yeniguncel = akademisyenService.turegoreyayingetir("kitap", a.getId());


                                akademisyenService.refreshGrid(yeniguncel, Yayinlar.class, kitaplar);

                            }
                            if(seciliyayin2!=null){
                                seciliyayin2.mdoi=declarativeYeniYayinGuncelle.mdoi.getValue().toString();
                                seciliyayin2.mtur=declarativeYeniYayinGuncelle.mtur.getValue().toString();
                                seciliyayin2.dadi=declarativeYeniYayinGuncelle.dadi.getValue().toString();
                                seciliyayin2.dtur=declarativeYeniYayinGuncelle.dtur.getValue().toString();
                                seciliyayin2.mbaslik=declarativeYeniYayinGuncelle.mbaslik.getValue().toString();
                                seciliyayin2.mdil=declarativeYeniYayinGuncelle.mdil.getValue().toString();
                                seciliyayin2.mkonu=declarativeYeniYayinGuncelle.mkonu.getValue().toString();
                                seciliyayin2.mlink=declarativeYeniYayinGuncelle.mlink.getValue().toString();
                                seciliyayin2.cilt=declarativeYeniYayinGuncelle.cilt.getValue().toString();
                                seciliyayin2.sayi=declarativeYeniYayinGuncelle.sayi.getValue().toString();
                                seciliyayin2.no=declarativeYeniYayinGuncelle.no.getValue().toString();
                                seciliyayin2.btur=declarativeYeniYayinGuncelle.btur.getValue().toString();
                                seciliyayin2.btarih=simpleDateFormat.format(declarativeYeniYayinGuncelle.btarih.getValue());

                                akademisyenService.yayinguncelle(seciliyayin2);


                                Iterator<Window> window = getUI().getWindows().iterator();

                                getUI().removeWindow(window.next());
                                new MyNotification("guncellme başarılı", MessageType.SUCCESS);
                                java.util.List<Yayinlar> yeniguncel = null;
                                yeniguncel = akademisyenService.turegoreyayingetir("dergide makale", a.getId());


                            }

                        }
                    });




            /*

            DeclarativeYayinGuncelle declarativeYayinGuncelle = new DeclarativeYayinGuncelle();
            Yayinlar seciliYayin = (Yayinlar) tumyayinlar.getSelectedRow();
            if (seciliYayin != null) {


                declarativeYayinGuncelle.txtyayin.setValue(akademisyenService.yayinguncelleicingetir(seciliYayin).getYayinadi().toString());
                final Window window = new Window("guncelleme ekranı");
                window.setModal(true);
                window.setWidth(400.0f, Unit.PIXELS);
                window.setHeight(600.0f, Unit.PIXELS);
                window.setDraggable(false);
                window.setResizable(false);
                window.setContent(declarativeYayinGuncelle);

                UI.getCurrent().addWindow(window);
            } else {

                new MyNotification("güncellemek istediğiniz yayını seçin", MessageType.FAIL);

            }

            declarativeYayinGuncelle.btnyayinguncelle.addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent clickEvent) {


                    seciliYayin.yayinadi = declarativeYayinGuncelle.txtyayin.getValue().toString();

                    akademisyenService.yayinguncelle(seciliYayin);


                    Iterator<Window> window = getUI().getWindows().iterator();

                    getUI().removeWindow(window.next());
                    new MyNotification("guncellme başarılı", MessageType.SUCCESS);
                    java.util.List<Yayinlar> yeniguncel = null;
                    yeniguncel = akademisyenService.gettumyayinlar(a.getId());


                    akademisyenService.refreshGrid(yeniguncel,Yayinlar.class,tumyayinlar);



                }
            });*/


        });


        kaydetyayin.addClickListener(clickEvent -> {
            getUI().getPage().open("http:#!YeniYayinView", "_blank",false);//formu yenı sekmede acıyor
            /*

            DeclarativeYayinKaydet declarativeYayinKaydet = new DeclarativeYayinKaydet();
            //Akademisyen a= (Akademisyen) VaadinSession.getCurrent().getSession().getAttribute("akademisyen");

            final Window window = new Window("ekleme ekranı");
            window.setModal(true);
            window.setWidth(400.0f, Unit.PIXELS);
            window.setHeight(600.0f, Unit.PIXELS);
            window.setDraggable(false);
            window.setResizable(false);
            window.setContent(declarativeYayinKaydet);
            UI.getCurrent().addWindow(window);

            declarativeYayinKaydet.btnyayinkaydet.addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent clickEvent) {
                    int ilk = akademisyenService.gettumyayinlar(a.getId()).size();
                    Yayinlar yayin = new Yayinlar();
                    yayin.setYayinadi(declarativeYayinKaydet.txtyayin.getValue().toString());
                    if (declarativeYayinKaydet.txtyayin.getValue().toString() == "")
                        declarativeYayinKaydet.txtyayin.setRequired(true);
                    else {

                        if (akademisyenService.yayinvarmi(declarativeYayinKaydet.txtyayin.getValue())!= null){
                            new MyNotification("zaten ekli",MessageType.FAIL);
                        }
                        else {

                            a.getYayinlarList().add(yayin);

                            akademisyenService.akademisyenEkle(a);

                            int son = akademisyenService.gettumyayinlar(a.getId()).size();

                            if (son > ilk) {

                                Iterator<Window> window = getUI().getWindows().iterator();

                                getUI().removeWindow(window.next());
                                new MyNotification("kaydetme başarılı", MessageType.SUCCESS);
                                java.util.List<Yayinlar> yeniyayinlarList = null;
                                yeniyayinlarList = akademisyenService.gettumyayinlar(a.getId());
                                akademisyenService.refreshGrid(yeniyayinlarList, Yayinlar.class, tumyayinlar);





                            } else {

                                new MyNotification("kaydetme basarısız", MessageType.WARNING);
                            }
                        }

                    }
                }


            });*/


        });

        btnara.addClickListener(clickEvent -> {

            String ad=ara.getValue();
            java.util.List<Yayinlar> yeniyayinlarList = null;
            yeniyayinlarList = akademisyenService.adagoregetir(ad);
            akademisyenService.refreshGrid(yeniyayinlarList,Yayinlar.class,tumyayinlar);


        });


    }
}


