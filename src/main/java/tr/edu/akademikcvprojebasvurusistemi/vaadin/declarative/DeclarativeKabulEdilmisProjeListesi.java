package tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.StreamResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.*;
import com.vaadin.ui.declarative.Design;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import tr.edu.akademikcvprojebasvurusistemi.domain.Akademisyen;
import tr.edu.akademikcvprojebasvurusistemi.domain.Proje;
import tr.edu.akademikcvprojebasvurusistemi.raporlama.ProjeModel;
import tr.edu.akademikcvprojebasvurusistemi.service.AkademisyenService;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.extras.MyNotification;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.extras.enums.MessageType;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by ubuntu on 3/14/17.
 */
@DesignRoot
public class DeclarativeKabulEdilmisProjeListesi  extends Panel {
    public Button ozet;
    public Grid tumprojeler;
    public Label lblyazi;
    Proje seciliproje;
    DeclarativeProjeOzet declarativeProjeOzet = new DeclarativeProjeOzet();


    AkademisyenService akademisyenService;

    public DeclarativeKabulEdilmisProjeListesi(AkademisyenService _akademisyen) {

        Akademisyen a = (Akademisyen) VaadinSession.getCurrent().getSession().getAttribute("akademisyen");


        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        akademisyenService = _akademisyen;
        Design.read(this);
        tumprojeler.setSelectionMode(Grid.SelectionMode.SINGLE);
        //tumprojeler.setSelectionMode(Grid.SelectionMode.MULTI);
        tumprojeler.setStyleName("anasayfagrid");

        tumprojeler.getSelectionModel().reset();
        tumprojeler.setSizeFull();

        java.util.List<Proje> projeler = null;
        projeler = akademisyenService.onayliprojeler(a.getId());

        BeanItemContainer<Proje> projeBeanItemContainer = new BeanItemContainer<>(Proje.class);
        tumprojeler.setContainerDataSource(projeBeanItemContainer);
        tumprojeler.getContainerDataSource().removeAllItems();
        projeBeanItemContainer.addAll(projeler);
        tumprojeler.setContainerDataSource(projeBeanItemContainer);

        tumprojeler.removeAllColumns();
        tumprojeler.addColumn("projeadi");
        tumprojeler.addColumn("onaydurumu");

        tumprojeler.getColumn("projeadi").setHeaderCaption("PROJE ADI");
        tumprojeler.getColumn("onaydurumu").setHeaderCaption("ONAY DURUMU ");




    }



    }

