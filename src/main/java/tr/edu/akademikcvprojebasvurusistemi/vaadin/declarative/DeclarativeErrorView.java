package tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;


@DesignRoot
public class DeclarativeErrorView extends Panel {

    public VerticalLayout vRoot;
    public Label vLabel;

    public DeclarativeErrorView() {
        Design.read(this);

        vLabel.setSizeUndefined();
        vRoot.setSizeUndefined();

        vRoot.setComponentAlignment(vLabel, Alignment.MIDDLE_CENTER);
    }
}
