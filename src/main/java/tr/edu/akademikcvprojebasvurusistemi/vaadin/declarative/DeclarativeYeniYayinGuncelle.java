package tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.*;
import com.vaadin.ui.declarative.Design;

/**
 * Created by LENOVO on 26.4.2017.
 */
@DesignRoot
public class DeclarativeYeniYayinGuncelle extends VerticalLayout {

    public CheckBox ulusal,uluslararasi;
    public TextField yayinturu;
    public Panel pnlkitap,pnlkitaptabolum,pnlbildiri,pnlmakale1;

    public ComboBox kdil,ktur,kkonu,krol,kyayinevigrup,kbaski;
    public TextField kyayinevi,kbaslik,ksayfa,kisbn,ksehir,kulke,kdoi,klink;
    public DateField kbasim,ksonbaski,btarih;
    public  Button btnkaydet;
    public ComboBox dtur,mtur,mkonu,mdil,btur;
    public TextField dadi,mbaslik,cilt,sayi,no,mdoi,mlink;


    public  DeclarativeYeniYayinGuncelle(){
        Design.read(this);


        setSizeUndefined();
        pnlkitap.setVisible(false);
        pnlmakale1.setVisible(false);
        pnlbildiri.setVisible(false);
        pnlkitaptabolum.setVisible(false);
    }
}
