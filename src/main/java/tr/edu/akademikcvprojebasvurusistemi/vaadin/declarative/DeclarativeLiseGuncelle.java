package tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Panel;
import com.vaadin.ui.declarative.Design;

import java.lang.management.BufferPoolMXBean;
import java.text.DateFormat;

/**
 * Created by LENOVO on 22.12.2016.
 */
@DesignRoot
public class DeclarativeLiseGuncelle extends Panel {
    public DateField guncellisebaslangic,guncellisebitis;
    public ComboBox guncelliseadi;
    public Button btnliseguncelle;

    public DeclarativeLiseGuncelle() {
        Design.read(this);
        setSizeUndefined();
        btnliseguncelle.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        btnliseguncelle.setIcon(FontAwesome.CHECK);

        guncelliseadi.setRequired(false);
        guncelliseadi.setRequiredError("lise Seçmelisiniz!");

        guncellisebaslangic.setRequired(false);
        guncellisebaslangic.setRequiredError("başlangıç tarihi Seçmelisiniz!");

        guncellisebitis.setRequired(false);
        guncellisebitis.setRequiredError("bitiş tarihi Seçmelisiniz!");


    }
}
