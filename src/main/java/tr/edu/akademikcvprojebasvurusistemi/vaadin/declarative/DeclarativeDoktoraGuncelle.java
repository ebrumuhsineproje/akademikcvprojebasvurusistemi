/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;
import com.vaadin.ui.declarative.Design;

/**
 *
 * @author ubuntu
 */
@DesignRoot
public class DeclarativeDoktoraGuncelle extends Panel{
    public DateField gunceldoktorabitis,gunceldoktorabaslangic;
    public ComboBox gunceldoktorauniv,gunceldoktorafakulte,gunceldoktorabolum;
    public Button btndoktoraguncelle;
    public CheckBox devam;

    public DeclarativeDoktoraGuncelle() {
        Design.read(this);
        setSizeUndefined();
        btndoktoraguncelle.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        btndoktoraguncelle.setIcon(FontAwesome.CHECK);

        gunceldoktorauniv.setRequired(false);
        gunceldoktorauniv.setRequiredError("üniversite Seçmelisiniz!");

        gunceldoktorafakulte.setRequired(false);
        gunceldoktorafakulte.setRequiredError("fakülte Seçmelisiniz!");

        gunceldoktorabolum.setRequired(false);
        gunceldoktorabolum.setRequiredError("bölüm Seçmelisiniz!");

        gunceldoktorabaslangic.setRequired(false);
        gunceldoktorabaslangic.setRequiredError("başlangıç tarihi seçmelisini!");

        gunceldoktorabaslangic.setRequired(false);
        gunceldoktorabitis.setRequiredError("bitiş tarihi seçmelisiniz");





    }
    
}
