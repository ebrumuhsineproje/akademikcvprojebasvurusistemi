/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Panel;
import com.vaadin.ui.declarative.Design;

/**
 *
 * @author ubuntu
 */
@DesignRoot
public class DeclarativeYuksekLisansKaydet extends Panel {
    public ComboBox yukseklisansuniv,yukseklisansfakulte,yukseklisansbolum;
    public DateField yukseklisansbaslangic,yukseklisansbitis;
    public Button btnYuksekLisansKaydet;
    
    public DeclarativeYuksekLisansKaydet() {
        Design.read(this);
        setSizeUndefined();
        btnYuksekLisansKaydet.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        btnYuksekLisansKaydet.setIcon(FontAwesome.CHECK);
        yukseklisansuniv.setRequired(false);
        yukseklisansuniv.setRequiredError("üniversite Seçmelisiniz!");

        yukseklisansfakulte.setRequired(false);
        yukseklisansfakulte.setRequiredError("fakülte Seçmelisiniz!");

        yukseklisansbolum.setRequired(false);
        yukseklisansbolum.setRequiredError("bölüm Seçmelisiniz!");


    }
    
}
