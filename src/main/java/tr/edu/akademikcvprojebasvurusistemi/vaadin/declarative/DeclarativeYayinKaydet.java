/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;
import com.vaadin.ui.declarative.Design;

/**
 *
 * @author ubuntu
 */
@DesignRoot
public class DeclarativeYayinKaydet extends Panel{
    public TextArea txtyayin;
    public Button btnyayinkaydet;
   
    
    public DeclarativeYayinKaydet() {
        Design.read(this);
        setSizeUndefined();
        btnyayinkaydet.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        btnyayinkaydet.setIcon(FontAwesome.CHECK);

        txtyayin.setRequired(false);
        txtyayin.setRequiredError("yayın adı giriniz!");



    }
}
