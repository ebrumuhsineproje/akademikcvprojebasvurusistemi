package tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.*;
import com.vaadin.ui.declarative.Design;
import com.vaadin.ui.themes.ValoTheme;

/**
 * Created by ubuntu on 3/6/17.
 */
@DesignRoot
public class DeclarativeEskiProjeBilgisi extends VerticalLayout {
    public Panel pnlKisi,pnlProje;
    public TextField txtAdi,txtSoyadi,txtTC,txtBirim,txtBolum,txtAnaBilimDali,txtKadroUnvani,txtEposta,txtCepTel,txtIsTel;
    public  TextField txtProjeAdi,txtProjeOzeti,txtUlke,txtSehir,txtButcesi,txtKurumKatkiPayi,
            txtPatentAdi,txtPatentKodu,txtFirmaAdi,txtSagladigiFon;

    public Panel pnlPatent,pnlSanayi;
    public DateField txtBaslangicTarihi,txtBitisTarihi,txtPatentYili;

    public ComboBox txtProjedeAlinanGorev,cbPatentKapsami,cbsanayiIsbirligiDurumu,cbProjePatenti,cbParaBirimi,cbProjeKapsamaAlani,cbAr_geNiteligi,cbFirmaParaBirimi,cbProjeTuru;
    public Button btnKaydet,don,don2;

    public CheckBox imzabekliyor,onaylanmis;


    public Panel pnlekip;
    public Grid grdgorevliakademisyen;
    public TextField txtTcgorevli;
    public ComboBox cbgorev,projeekibi;
    public Button btnekle,btncikar;


    public DeclarativeEskiProjeBilgisi() {
        Design.read(this);
        setSizeUndefined();



        //txtProjeAdi.setVisible(false);
        pnlKisi.setVisible(false);
        pnlPatent.setVisible(false);
        pnlSanayi.setVisible(false);
        don.setStyleName(ValoTheme.BUTTON_LINK);

        don2.setStyleName(ValoTheme.BUTTON_LINK);
        pnlekip.setVisible(false);





    }
}
