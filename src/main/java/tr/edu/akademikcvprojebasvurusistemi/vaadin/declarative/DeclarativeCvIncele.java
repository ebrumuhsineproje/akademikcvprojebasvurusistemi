package tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.*;
import com.vaadin.ui.declarative.Design;
import org.jfree.chart.plot.AbstractPieLabelDistributor;
import org.springframework.beans.factory.annotation.Autowired;
import tr.edu.akademikcvprojebasvurusistemi.domain.Akademisyen;
import tr.edu.akademikcvprojebasvurusistemi.domain.Lise;
import tr.edu.akademikcvprojebasvurusistemi.domain.Proje;
import tr.edu.akademikcvprojebasvurusistemi.service.AkademisyenService;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.extras.MyNotification;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.extras.enums.MessageType;

import java.util.Iterator;
import java.util.List;

/**
 * Created by LENOVO on 11.5.2017.
 */
@DesignRoot
public class DeclarativeCvIncele extends Panel {

    public TextField ad,soyadi;
    public  ComboBox bolum;
    public Button ara;
    public  VerticalLayout vroot;
    public Grid cvgrid;

    @Autowired
    AkademisyenService akademisyenService;
    public DeclarativeCvIncele(AkademisyenService _akademisyen) {

        akademisyenService=_akademisyen;
        Design.read(this);
        cvgrid.setSelectionMode(Grid.SelectionMode.SINGLE);
        Akademisyen a = (Akademisyen) VaadinSession.getCurrent().getSession().getAttribute("akademisyen");


        cvgrid.setSizeFull();
        java.util.List<Akademisyen> akademisyenList = null;
        akademisyenList= akademisyenService.getTumAkademisyenler();


        BeanItemContainer<Akademisyen> akademisyenBeanItemContainer = new BeanItemContainer<>(Akademisyen.class);
        cvgrid.setContainerDataSource(akademisyenBeanItemContainer);
        cvgrid.getContainerDataSource().removeAllItems();
        akademisyenBeanItemContainer.addAll(akademisyenList);
        cvgrid.setContainerDataSource(akademisyenBeanItemContainer);

        cvgrid.removeAllColumns();
        cvgrid.addColumn("adi");
        cvgrid.addColumn("soyadi");
        cvgrid.addColumn("bolum");

        cvgrid.getColumn("adi").setHeaderCaption("ADI");
        cvgrid.getColumn("soyadi").setHeaderCaption("SOYADI");
        cvgrid.getColumn("bolum").setHeaderCaption("BÖLÜMÜ");

        cvgrid.setImmediate(true);

        ara.addClickListener(clickEvent -> {
            if(!ad.getValue().equals("") && !soyadi.getValue().equals("") && bolum.getValue()!=null){

                List<Akademisyen> akademisyenList1=null;

                akademisyenList1=akademisyenService.adsoyadbolumegoregetir(ad.getValue().toString(),soyadi.getValue().toString(),bolum.getValue().toString());

                akademisyenService.refreshGrid(akademisyenList1, Akademisyen.class,cvgrid);

            }
            if(!ad.getValue().equals("") && !soyadi.getValue().equals("") && bolum.getValue()==null){
                List<Akademisyen> akademisyenList1=null;

                akademisyenList1=akademisyenService.adasoyadagoregetir(ad.getValue().toString(),soyadi.getValue().toString());

                akademisyenService.refreshGrid(akademisyenList1, Akademisyen.class,cvgrid);

            }
            if(!ad.getValue().equals("") && soyadi.getValue().equals("") && bolum.getValue()!=null){
                List<Akademisyen> akademisyenList1=null;

                akademisyenList1=akademisyenService.adabolume(ad.getValue().toString(),bolum.getValue().toString());

                akademisyenService.refreshGrid(akademisyenList1, Akademisyen.class,cvgrid);

            }
            if(ad.getValue().equals("") && !soyadi.getValue().equals("") && bolum.getValue()!=null){
                List<Akademisyen> akademisyenList1=null;

                akademisyenList1=akademisyenService.soyadabolume(soyadi.getValue().toString(),bolum.getValue().toString());

                akademisyenService.refreshGrid(akademisyenList1, Akademisyen.class,cvgrid);

            }
            if(ad.getValue().equals("") && !soyadi.getValue().equals("") && bolum.getValue()==null){
                List<Akademisyen> akademisyenList1=null;

                akademisyenList1=akademisyenService.soyadagoregetir(soyadi.getValue().toString());

                akademisyenService.refreshGrid(akademisyenList1, Akademisyen.class,cvgrid);

            }
            if(ad.getValue().equals("") && soyadi.getValue().equals("") && bolum.getValue()!=null){
                List<Akademisyen> akademisyenList1=null;

                akademisyenList1=akademisyenService.bolumegoregetir(bolum.getValue().toString());

                akademisyenService.refreshGrid(akademisyenList1, Akademisyen.class,cvgrid);

            }
            if(!ad.getValue().equals("") && soyadi.getValue().equals("") && bolum.getValue()==null){
                List<Akademisyen> akademisyenList1=null;

                akademisyenList1=akademisyenService.adagore2(ad.getValue().toString());

                akademisyenService.refreshGrid(akademisyenList1, Akademisyen.class,cvgrid);

            }

        });

        cvgrid.addSelectionListener(selectionEvent -> {

            Object selected = ((Grid.SingleSelectionModel) cvgrid.getSelectionModel()).getSelectedRow();

            Akademisyen ab=new Akademisyen();
            ab= (Akademisyen) selected;

            DeclarativeCvBilgiler declarativeCvBilgiler=new DeclarativeCvBilgiler(akademisyenService, (Akademisyen) selected);

            Window window1=new Window("");
            window1.setModal(true);
            window1.setWidth(1300.0f,Unit.PIXELS);
            window1.setHeight(700.0f,Unit.PIXELS);
            window1.setDraggable(false);
            window1.setResizable(false);
            window1.setContent(declarativeCvBilgiler);
            UI.getCurrent().addWindow(window1);




        });


    }
}
