package tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative;

import com.sun.jersey.core.spi.scanning.PackageNamesScanner;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.Property;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;
import org.springframework.beans.factory.annotation.Autowired;
import tr.edu.akademikcvprojebasvurusistemi.domain.Proje;
import tr.edu.akademikcvprojebasvurusistemi.service.AkademisyenService;

import java.util.List;

/**
 * Created by LENOVO on 16.5.2017.
 */
@DesignRoot
public class DeclarativeIstatistik extends VerticalLayout {
    public Label sayi1,sayi2,sayi3;

    public ComboBox cbprojeturu;
    @Autowired
    AkademisyenService akademisyenService;

    public  DeclarativeIstatistik(AkademisyenService _akademisyen){
        akademisyenService = _akademisyen;
        Design.read(this);
        setSizeFull();



        cbprojeturu.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                if (cbprojeturu.getValue().toString().equals("TÜMÜ")) {
                   List<Proje> projeList=null;
                   projeList=akademisyenService.tumprojeler();
                   List<Proje> projeList1=null;
                   projeList1=akademisyenService.tumkabuledilenler();


                    int sayi22=projeList.size();
                    if(sayi22==0){
                        sayi2.setValue("Proje Başvurusu bulunmamaktadır.");

                    }
                    else{
                        String b = String.valueOf(sayi22);
                        sayi2.setValue("Toplam "+b+" tane projeye başvuruldu");

                    }

                    int sayi11=projeList1.size();
                    if(sayi11==0){
                        sayi1.setValue("kabul edilen proje bulunmamaktadır");
                    }
                    if(sayi11==0 && sayi22==0){
                        sayi1.setValue("");
                    }
                    if(sayi11!=0){
                        String c=String.valueOf(sayi11);
                        sayi1.setValue(c+" tanesi kabul edildi");

                    }

                    if(sayi22!=0){
                        int sayi33= (sayi11*100)/sayi22;

                        String d=String.valueOf(sayi33);
                        sayi3.setValue("Başarı oranı %"+d);
                    }
                    else{
                        sayi3.setValue("");
                    }




                }

                if (cbprojeturu.getValue().toString().equals("AB")) {
                    List<Proje> projeList=null;
                    projeList=akademisyenService.turegoregetir("AB");
                    List<Proje> projeList1=null;
                    projeList1=akademisyenService.turkabul("AB");


                    int sayi22=projeList.size();
                    if(sayi22==0){
                        sayi2.setValue("Proje Başvurusu bulunmamaktadır.");

                    }
                    else{
                        String b = String.valueOf(sayi22);
                        sayi2.setValue("Toplam "+b+" tane projeye başvuruldu");

                    }

                    int sayi11=projeList1.size();
                    if(sayi11==0){
                        sayi1.setValue("kabul edilen proje bulunmamaktadır");
                    }
                    if(sayi11==0 && sayi22==0){
                        sayi1.setValue("");
                    }
                    if(sayi11!=0){
                        String c=String.valueOf(sayi11);
                        sayi1.setValue(c+" tanesi kabul edildi");

                    }

                    if(sayi22!=0){
                        int sayi33= (sayi11*100)/sayi22;

                        String d=String.valueOf(sayi33);
                        sayi3.setValue("Başarı oranı %"+d);
                    }
                    else{
                        sayi3.setValue("");
                    }




                }

                if (cbprojeturu.getValue().toString().equals("TÜBİTAK")) {
                    List<Proje> projeList=null;
                    projeList=akademisyenService.turegoregetir("TÜBİTAK");
                    List<Proje> projeList1=null;
                    projeList1=akademisyenService.turkabul("TÜBİTAK");


                    int sayi22=projeList.size();
                    if(sayi22==0){
                        sayi2.setValue("Proje Başvurusu bulunmamaktadır.");

                    }
                    else{
                        String b = String.valueOf(sayi22);
                        sayi2.setValue("Toplam "+b+" tane projeye başvuruldu");

                    }

                    int sayi11=projeList1.size();
                    if(sayi11==0){
                        sayi1.setValue("kabul edilen proje bulunmamaktadır");
                    }
                    if(sayi11==0 && sayi22==0){
                        sayi1.setValue("");
                    }
                    if(sayi11!=0){
                        String c=String.valueOf(sayi11);
                        sayi1.setValue(c+" tanesi kabul edildi");

                    }

                    if(sayi22!=0){
                        int sayi33= (sayi11*100)/sayi22;

                        String d=String.valueOf(sayi33);
                        sayi3.setValue("Başarı oranı %"+d);
                    }
                    else{
                        sayi3.setValue("");
                    }




                }


                if (cbprojeturu.getValue().toString().equals("SAN-TEZ")) {
                    List<Proje> projeList=null;
                    projeList=akademisyenService.turegoregetir("SAN-TEZ");
                    List<Proje> projeList1=null;
                    projeList1=akademisyenService.turkabul("SAN-TEZ");


                    int sayi22=projeList.size();
                    if(sayi22==0){
                        sayi2.setValue("Proje Başvurusu bulunmamaktadır.");

                    }
                    else{
                        String b = String.valueOf(sayi22);
                        sayi2.setValue("Toplam "+b+" tane projeye başvuruldu");

                    }

                    int sayi11=projeList1.size();
                    if(sayi11==0){
                        sayi1.setValue("kabul edilen proje bulunmamaktadır");
                    }
                    if(sayi11==0 && sayi22==0){
                        sayi1.setValue("");
                    }
                    if(sayi11!=0){
                        String c=String.valueOf(sayi11);
                        sayi1.setValue(c+" tanesi kabul edildi");

                    }

                    if(sayi22!=0){
                        int sayi33= (sayi11*100)/sayi22;

                        String d=String.valueOf(sayi33);
                        sayi3.setValue("Başarı oranı %"+d);
                    }
                    else{
                        sayi3.setValue("");
                    }




                }


                if (cbprojeturu.getValue().toString().equals("GMK")) {
                    List<Proje> projeList=null;
                    projeList=akademisyenService.turegoregetir("GMK");
                    List<Proje> projeList1=null;
                    projeList1=akademisyenService.turkabul("GMK");


                    int sayi22=projeList.size();
                    if(sayi22==0){
                        sayi2.setValue("Proje Başvurusu bulunmamaktadır.");

                    }
                    else{
                        String b = String.valueOf(sayi22);
                        sayi2.setValue("Toplam "+b+" tane projeye başvuruldu");

                    }

                    int sayi11=projeList1.size();
                    if(sayi11==0){
                        sayi1.setValue("kabul edilen proje bulunmamaktadır");
                    }
                    if(sayi11==0 && sayi22==0){
                        sayi1.setValue("");
                    }
                    if(sayi11!=0){
                        String c=String.valueOf(sayi11);
                        sayi1.setValue(c+" tanesi kabul edildi");

                    }

                    if(sayi22!=0){
                        int sayi33= (sayi11*100)/sayi22;

                        String d=String.valueOf(sayi33);
                        sayi3.setValue("Başarı oranı %"+d);
                    }
                    else{
                        sayi3.setValue("");
                    }




                }


                if (cbprojeturu.getValue().toString().equals("DPT")) {
                    List<Proje> projeList=null;
                    projeList=akademisyenService.turegoregetir("DPT");
                    List<Proje> projeList1=null;
                    projeList1=akademisyenService.turkabul("DPT");


                    int sayi22=projeList.size();
                    if(sayi22==0){
                        sayi2.setValue("Proje Başvurusu bulunmamaktadır.");

                    }
                    else{
                        String b = String.valueOf(sayi22);
                        sayi2.setValue("Toplam "+b+" tane projeye başvuruldu");

                    }

                    int sayi11=projeList1.size();
                    if(sayi11==0){
                        sayi1.setValue("kabul edilen proje bulunmamaktadır");
                    }
                    if(sayi11==0 && sayi22==0){
                        sayi1.setValue("");
                    }
                    if(sayi11!=0){
                        String c=String.valueOf(sayi11);
                        sayi1.setValue(c+" tanesi kabul edildi");

                    }

                    if(sayi22!=0){
                        int sayi33= (sayi11*100)/sayi22;

                        String d=String.valueOf(sayi33);
                        sayi3.setValue("Başarı oranı %"+d);
                    }
                    else{
                        sayi3.setValue("");
                    }




                }


            }
        });

    }
}
