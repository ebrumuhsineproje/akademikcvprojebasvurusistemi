package tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.*;


import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;


@DesignRoot
public class DeclarativeYeniYayin extends VerticalLayout {

   public CheckBox ulusal,uluslararasi;
   public ComboBox yayinturu;
   public Panel pnlkitap,pnlkitaptabolum,pnlbildiri,pnlmakale1;

  public ComboBox kdil,ktur,kkonu,krol,kyayinevigrup,kbaski;
  public TextField kyayinevi,kbaslik,ksayfa,kisbn,ksehir,kulke,kdoi,klink;
  public DateField kbasim,ksonbaski,btarih;
  public  Button btnkaydet;
  public ComboBox dtur,mtur,mkonu,mdil,btur;
  public TextField dadi,mbaslik,cilt,sayi,no,mdoi,mlink;



    public DeclarativeYeniYayin() {
        Design.read(this);

    setSizeUndefined();
    pnlkitap.setVisible(false);
    pnlmakale1.setVisible(false);
    pnlbildiri.setVisible(false);
    pnlkitaptabolum.setVisible(false);





    }
}
