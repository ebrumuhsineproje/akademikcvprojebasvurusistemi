package tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;
import com.vaadin.ui.declarative.Design;
import tr.edu.akademikcvprojebasvurusistemi.domain.Yayinlar;

/**
 * Created by LENOVO on 22.12.2016.
 */
@DesignRoot
public class DeclarativeYayinGuncelle extends VerticalLayout {
    public TextArea txtyayin;
    public Button btnyayinguncelle;

    public DeclarativeYayinGuncelle() {
        Design.read(this);
        setSizeUndefined();
        btnyayinguncelle.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        btnyayinguncelle.setIcon(FontAwesome.CHECK);

        txtyayin.setRequired(false);
        txtyayin.setRequiredError("yayın adı giriniz!");



    }



}
