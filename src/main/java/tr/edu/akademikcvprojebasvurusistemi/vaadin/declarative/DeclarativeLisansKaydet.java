/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Panel;
import com.vaadin.ui.declarative.Design;

/**
 *
 * @author ubuntu
 */
@DesignRoot
public class DeclarativeLisansKaydet extends Panel{
    
    public ComboBox lisansuniv,lisansfakulte,lisansbolum;
    public DateField lisansbaslangic,lisansbitis;
    public Button btnLisansKaydet;
    
    public DeclarativeLisansKaydet() {
        Design.read(this);
        setSizeUndefined();
        btnLisansKaydet.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        btnLisansKaydet.setIcon(FontAwesome.CHECK);

        lisansuniv.setRequired(false);
        lisansuniv.setRequiredError("üniversite Seçmelisiniz!");

        lisansfakulte.setRequired(false);
        lisansfakulte.setRequiredError("fakülte Seçmelisiniz!");

        lisansbolum.setRequired(false);
        lisansbolum.setRequiredError("bölüm Seçmelisiniz!");


    }
    
}
