/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Panel;
import com.vaadin.ui.declarative.Design;

/**
 *
 * @author ubuntu
 */
@DesignRoot
public class DeclarativeLisansGuncelle extends Panel {
    public DateField guncellisansbaslangic,guncellisansbitis;
    public ComboBox guncellisansuniv,guncellisansfakulte,guncellisansbolum;
    public Button btnlisansguncelle;

    public DeclarativeLisansGuncelle() {
        Design.read(this);
        setSizeUndefined();
        btnlisansguncelle.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        btnlisansguncelle.setIcon(FontAwesome.CHECK);

        guncellisansuniv.setRequired(false);
        guncellisansuniv.setRequiredError("üniversite Seçmelisiniz!");

        guncellisansfakulte.setRequired(false);
        guncellisansfakulte.setRequiredError("fakülte Seçmelisiniz!");

        guncellisansbolum.setRequired(false);
        guncellisansbolum.setRequiredError("bölüm Seçmelisiniz!");


    }
    
}
