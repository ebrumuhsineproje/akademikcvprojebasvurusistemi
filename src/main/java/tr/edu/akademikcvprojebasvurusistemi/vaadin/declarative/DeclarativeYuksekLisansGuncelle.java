/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Panel;
import com.vaadin.ui.declarative.Design;

/**
 *
 * @author ubuntu
 */
@DesignRoot
public class DeclarativeYuksekLisansGuncelle extends Panel{
    public DateField guncelyukseklisansbitis,guncelyukseklisansbaslangic;
    public ComboBox guncelyukseklisansuniv,guncelyukseklisansfakulte,guncelyukseklisansbolum;
    public Button btnyukseklisansguncelle;

    public DeclarativeYuksekLisansGuncelle() {
        Design.read(this);
        setSizeUndefined();
        btnyukseklisansguncelle.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        btnyukseklisansguncelle.setIcon(FontAwesome.CHECK);
        guncelyukseklisansuniv.setRequired(false);
        guncelyukseklisansuniv.setRequiredError("üniversite Seçmelisiniz!");

        guncelyukseklisansfakulte.setRequired(false);
        guncelyukseklisansfakulte.setRequiredError("fakülte Seçmelisiniz!");

        guncelyukseklisansbolum.setRequired(false);
        guncelyukseklisansbolum.setRequiredError("bölüm Seçmelisiniz!");


    }
    
}
