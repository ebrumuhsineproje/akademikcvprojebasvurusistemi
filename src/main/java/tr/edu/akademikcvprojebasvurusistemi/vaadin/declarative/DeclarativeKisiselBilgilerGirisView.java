/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.declarative.Design;

/**
 *
 * @author LENOVO
 */
@DesignRoot
public class DeclarativeKisiselBilgilerGirisView extends Panel {
    public TextField txtAd;
    public TextField txtSoyad;
    public TextField txtTc;
    public Button btnKisiKaydet;
    
    public DeclarativeKisiselBilgilerGirisView() {
        Design.read(this);
        setSizeUndefined();
         btnKisiKaydet.setClickShortcut(ShortcutAction.KeyCode.ENTER);
  
        btnKisiKaydet.setIcon(FontAwesome.CHECK);
       
        
    }
    
}
