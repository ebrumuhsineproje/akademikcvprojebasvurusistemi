package tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.*;
import com.vaadin.ui.declarative.Design;
import org.springframework.beans.factory.annotation.Autowired;
import tr.edu.akademikcvprojebasvurusistemi.domain.*;
import tr.edu.akademikcvprojebasvurusistemi.service.AkademisyenService;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by LENOVO on 11.5.2017.
 */
@DesignRoot
public class DeclarativeCvBilgiler extends VerticalLayout {


 // public Grid lisebilgisi,lisansbilgisi,yükseklisansbilgisi,doktorabilgisi;

 public Label liseadi, lisebaslangic, lisebitis;
 public Label univ, fakulte, bolum, baslangicuniv, bitisuniv;
 public Label yuksekuniv, yuksekbaslangic, yuksekbitis, yuksekfakulte, yuksekbolum;
 public Label doktorauniv, doktorafakulte, doktorabaslangic, doktorabitis, doktorabolum;
 public Grid yayin, yayin2, proje;
 public FormLayout v1, v2, v3, v4;

 @Autowired
 AkademisyenService akademisyenService;
 Akademisyen abc = new Akademisyen();

 public DeclarativeCvBilgiler(AkademisyenService _akademisyen, Akademisyen _abc) {

  abc = _abc;
  akademisyenService = _akademisyen;


  Design.read(this);

  yayin.setSizeFull();
  yayin2.setSizeFull();
  proje.setSizeFull();
  DeclarativeCvIncele declarativeCvIncele = new DeclarativeCvIncele(akademisyenService);
  Object selected = ((Grid.SingleSelectionModel) declarativeCvIncele.cvgrid.getSelectionModel()).getSelectedRow();


  Akademisyen secilen = (Akademisyen) selected;

  SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");



   List<Lise> liseler = null;
   liseler = akademisyenService.gettumliseler(abc.getId());

   if (liseler.size() != 0) {


    String ad = liseler.get(0).getLiseadi();
    liseadi.setValue(ad);
    lisebaslangic.setValue(liseler.get(0).getLisebaslangic().toString());
    lisebitis.setValue(liseler.get(0).getLisebitis().toString());

   } else {
    v1.setVisible(false);
   }
   List<Lisans> lisanslar = null;
   lisanslar = akademisyenService.gettumlisanslar(abc.getId());
   if (lisanslar.size() != 0) {


    univ.setValue(lisanslar.get(0).getLisansuniv().toString());
    fakulte.setValue(lisanslar.get(0).getLisansfakulte().toString());
    bolum.setValue(lisanslar.get(0).getLisansbolum().toString());
    baslangicuniv.setValue(lisanslar.get(0).getLisansbaslangic().toString());
    bitisuniv.setValue(lisanslar.get(0).getLisansbitis().toString());
   } else {
    v2.setVisible(false);

   }
   List<YuksekLisans> yukseklisanslar = null;
   yukseklisanslar = akademisyenService.gettumyuksekler(abc.getId());
   if (yukseklisanslar.size() != 0) {


    yuksekuniv.setValue(yukseklisanslar.get(0).getYuksekLisansuniv().toString());
    yuksekfakulte.setValue(yukseklisanslar.get(0).getYuksekLisansfakulte().toString());
    yuksekbolum.setValue(yukseklisanslar.get(0).getYuksekLisansbolum().toString());
    yuksekbaslangic.setValue(yukseklisanslar.get(0).getYuksekLisansbaslangic().toString());
    yuksekbitis.setValue(yukseklisanslar.get(0).getYuksekLisansbitis().toString());
   } else {
    v3.setVisible(false);
   }
   List<Doktora> doktoralar = null;
   doktoralar = akademisyenService.gettumdoktoralar(abc.getId());
   if (doktoralar.size() != 0) {
    doktorauniv.setValue(doktoralar.get(0).getDoktorauniv().toString());
    doktorafakulte.setValue(doktoralar.get(0).getDoktorafakulte().toString());
    doktorabolum.setValue(doktoralar.get(0).getDoktorabolum().toString());
    doktorabaslangic.setValue(doktoralar.get(0).getDoktorabaslangic().toString());
    doktorabitis.setValue(doktoralar.get(0).getDoktorabitis().toString());

   } else {
    v4.setVisible(false);
   }


   if (akademisyenService.turegoreyayingetir("kitap", abc.getId()).size() == 0) {
    yayin.setVisible(false);
   } else {
    java.util.List<Yayinlar> yayinlarList2 = null;
    yayinlarList2 = akademisyenService.turegoreyayingetir("kitap", abc.getId());
    BeanItemContainer<Yayinlar> yayinlarBeanItemContainer2 = new BeanItemContainer<>(Yayinlar.class);
    yayin.setContainerDataSource(yayinlarBeanItemContainer2);
    yayin.getContainerDataSource().removeAllItems();
    yayinlarBeanItemContainer2.addAll(yayinlarList2);
    yayin.setContainerDataSource(yayinlarBeanItemContainer2);

    yayin.removeAllColumns();
    yayin.addColumn("ulusallik");
    yayin.addColumn("kbaslik");
    yayin.addColumn("ktur");
    yayin.addColumn("kkonu");
    yayin.getColumn("ulusallik").setHeaderCaption("YAYIN TİPİ");
    yayin.getColumn("kbaslik").setHeaderCaption("KİTAP BAŞLIĞI");
    yayin.getColumn("ktur").setHeaderCaption("KİTAP TÜRÜ");
    yayin.getColumn("kkonu").setHeaderCaption("KİTAP KONUSU");


   }
   if (akademisyenService.turegoreyayingetir("dergide makale", abc.getId()).size() == 0) {
    yayin2.setVisible(false);
   } else {
    java.util.List<Yayinlar> yayinlarList3 = null;
    yayinlarList3 = akademisyenService.turegoreyayingetir("dergide makale", abc.getId());
    BeanItemContainer<Yayinlar> yayinlarBeanItemContainer3 = new BeanItemContainer<>(Yayinlar.class);
    yayin2.setContainerDataSource(yayinlarBeanItemContainer3);
    yayin2.getContainerDataSource().removeAllItems();
    yayinlarBeanItemContainer3.addAll(yayinlarList3);
    yayin2.setContainerDataSource(yayinlarBeanItemContainer3);

    yayin2.removeAllColumns();
    yayin2.addColumn("ulusallik");
    yayin2.addColumn("mbaslik");
    yayin2.addColumn("mtur");
    yayin2.addColumn("mkonu");
    yayin2.getColumn("ulusallik").setHeaderCaption("YAYIN TİPİ");
    yayin2.getColumn("mbaslik").setHeaderCaption("MAKALE BAŞLIĞI");
    yayin2.getColumn("mtur").setHeaderCaption("MAKALE TÜRÜ");
    yayin2.getColumn("mkonu").setHeaderCaption("MAKALE KONUSU");

   }

   java.util.List<Proje> projeList = null;
   projeList = akademisyenService.gettumprojeler(abc.getId());
   if (projeList.size() != 0) {


    BeanItemContainer<Proje> projeBeanItemContainer = new BeanItemContainer<>(Proje.class);
    proje.setContainerDataSource(projeBeanItemContainer);
    proje.getContainerDataSource().removeAllItems();
    projeBeanItemContainer.addAll(projeList);
    proje.setContainerDataSource(projeBeanItemContainer);

    proje.removeAllColumns();
    proje.addColumn("projeadi");
    proje.addColumn("projeTuru");

    proje.getColumn("projeadi").setHeaderCaption("PROJE ADI");
    proje.getColumn("projeTuru").setHeaderCaption("PROJE TÜRÜ");

   } else {
    proje.setVisible(false);
   }









        /*
        lisebilgisi.setSizeFull();

        lisansbilgisi.setSizeFull();

        yükseklisansbilgisi.setSizeFull();

        doktorabilgisi.setSizeFull();

        java.util.List<Lise> liseler = null;
        liseler= akademisyenService.gettumliseler(abc.getId());


        BeanItemContainer<Lise> liseBeanItemContainer = new BeanItemContainer<>(Lise.class);
        lisebilgisi.setContainerDataSource(liseBeanItemContainer);
        lisebilgisi.getContainerDataSource().removeAllItems();
        liseBeanItemContainer.addAll(liseler);
        lisebilgisi.setContainerDataSource(liseBeanItemContainer);

        lisebilgisi.removeAllColumns();
        lisebilgisi.addColumn("liseadi");
        lisebilgisi.addColumn("lisebaslangic");
        lisebilgisi.addColumn("lisebitis");

        lisebilgisi.getColumn("liseadi").setHeaderCaption("ADI");
        lisebilgisi.getColumn("lisebaslangic").setHeaderCaption("BAŞLANGIÇ");
        lisebilgisi.getColumn("lisebitis").setHeaderCaption("BİTİŞ");

        lisebilgisi.setImmediate(true);

        java.util.List<Lisans> lisanslar = null;
       lisanslar= akademisyenService.gettumlisanslar(abc.getId());


        BeanItemContainer<Lisans> lisansBeanItemContainer = new BeanItemContainer<>(Lisans.class);
        lisansbilgisi.setContainerDataSource(lisansBeanItemContainer);
        lisansbilgisi.getContainerDataSource().removeAllItems();
        lisansBeanItemContainer.addAll(lisanslar);
        lisansbilgisi.setContainerDataSource(lisansBeanItemContainer);

        lisansbilgisi.removeAllColumns();
        lisansbilgisi.addColumn("lisansuniv");
        lisansbilgisi.addColumn("lisansfakulte");
        lisansbilgisi.addColumn("lisansbolum");
        lisansbilgisi.addColumn("lisansbaslangic");
        lisansbilgisi.addColumn("lisansbitis");
        lisansbilgisi.getColumn("lisansuniv").setHeaderCaption("ÜNİVERSİTE");
        lisansbilgisi.getColumn("lisansfakulte").setHeaderCaption("FAKÜLTE");
        lisansbilgisi.getColumn("lisansbolum").setHeaderCaption("BÖLÜM");
        lisansbilgisi.getColumn("lisansbaslangic").setHeaderCaption("BAŞLANGIÇ");
        lisansbilgisi.getColumn("lisansbitis").setHeaderCaption("BİTİŞ");

        java.util.List<YuksekLisans> yukseklisanslar = null;
        yukseklisanslar= akademisyenService.gettumyuksekler(abc.getId());


        BeanItemContainer<YuksekLisans> yuksekLisansBeanItemContainer = new BeanItemContainer<>(YuksekLisans.class);
        yükseklisansbilgisi.setContainerDataSource(yuksekLisansBeanItemContainer);
        yükseklisansbilgisi.getContainerDataSource().removeAllItems();
        yuksekLisansBeanItemContainer.addAll(yukseklisanslar);
        yükseklisansbilgisi.setContainerDataSource(yuksekLisansBeanItemContainer);

        yükseklisansbilgisi.removeAllColumns();
        yükseklisansbilgisi.addColumn("yuksekLisansuniv");
        yükseklisansbilgisi.addColumn("yuksekLisansfakulte");
        yükseklisansbilgisi.addColumn("yuksekLisansbolum");

        yükseklisansbilgisi.addColumn("yuksekLisansbaslangic");
        yükseklisansbilgisi.addColumn("yuksekLisansbitis");

        yükseklisansbilgisi.getColumn("yuksekLisansuniv").setHeaderCaption("ÜNİVERSİTE");
        yükseklisansbilgisi.getColumn("yuksekLisansfakulte").setHeaderCaption("FAKÜLTE");
        yükseklisansbilgisi.getColumn("yuksekLisansbolum").setHeaderCaption("BÖLÜM");
        yükseklisansbilgisi.getColumn("yuksekLisansbaslangic").setHeaderCaption("BAŞLANGIÇ");
        yükseklisansbilgisi.getColumn("yuksekLisansbitis").setHeaderCaption("BİTİŞ");



        java.util.List<Doktora> doktoralar = null;
        doktoralar= akademisyenService.gettumdoktoralar(abc.getId());


        BeanItemContainer<Doktora> doktoraBeanItemContainer = new BeanItemContainer<>(Doktora.class);
        doktorabilgisi.setContainerDataSource(doktoraBeanItemContainer);
        doktorabilgisi.getContainerDataSource().removeAllItems();
        doktoraBeanItemContainer.addAll(doktoralar);
        doktorabilgisi.setContainerDataSource(doktoraBeanItemContainer);

        doktorabilgisi.removeAllColumns();
        doktorabilgisi.addColumn("doktorauniv");
        doktorabilgisi.addColumn("doktorafakulte");
        doktorabilgisi.addColumn("doktorabolum");
        doktorabilgisi.addColumn("doktorabaslangic");
        doktorabilgisi.addColumn("doktorabitis");
        doktorabilgisi.getColumn("doktorauniv").setHeaderCaption("ÜNİVERSİTE");
        doktorabilgisi.getColumn("doktorafakulte").setHeaderCaption("FAKÜLTE");
        doktorabilgisi.getColumn("doktorabolum").setHeaderCaption("BÖLÜM");
        doktorabilgisi.getColumn("doktorabaslangic").setHeaderCaption("BAŞLANGIÇ");
        doktorabilgisi.getColumn("doktorabitis").setHeaderCaption("BİTİŞ");


*/
  }

 }

