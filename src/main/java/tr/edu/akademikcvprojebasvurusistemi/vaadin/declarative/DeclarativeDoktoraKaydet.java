/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;
import com.vaadin.ui.declarative.Design;

/**
 *
 * @author ubuntu
 */
@DesignRoot
public class DeclarativeDoktoraKaydet extends Panel {
    public ComboBox doktorauniv,doktorafakulte,doktorabolum;
    public DateField doktorabaslangic,doktorabitis;
    public Button btnDoktoraKaydet;
    public CheckBox devam;
    
    public DeclarativeDoktoraKaydet() {
        Design.read(this);
        setSizeUndefined();
        btnDoktoraKaydet.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        btnDoktoraKaydet.setIcon(FontAwesome.CHECK);
        doktorauniv.setRequired(false);
        doktorauniv.setRequiredError("üniversite Seçmelisiniz!");

        doktorafakulte.setRequired(false);
        doktorafakulte.setRequiredError("fakülte Seçmelisiniz!");

        doktorabolum.setRequired(false);
        doktorabolum.setRequiredError("bölüm Seçmelisiniz!");

        doktorabaslangic.setRequired(false);
        doktorabaslangic.setRequiredError("başlangıç tarihi seçmelisini!");

        doktorabitis.setRequired(false);
        doktorabitis.setRequiredError("bitiş tarihi seçmelisiniz");


    }
    
}
