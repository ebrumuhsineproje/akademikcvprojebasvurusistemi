package tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.*;
import com.vaadin.ui.declarative.Design;

/**
 * Created by LENOVO on 19.1.2017.
 */
@DesignRoot
public class DeclarativeProjeBasvuruGuncelle extends VerticalLayout {

    public Panel pnlProje;
    public  TextField txtProjeAdi,txtProjeOzeti,txtUlke,txtSehir,txtButcesi,txtKurumKatkiPayi,
            txtPatentAdi,txtPatentKodu,txtFirmaAdi,txtSagladigiFon;

    public Panel pnlPatent,pnlSanayi,pnlekip;
    public  DateField txtBaslangicTarihi,txtBitisTarihi,txtPatentYili;

    public ComboBox txtProjedeAlinanGorev, cbPatentKapsami,cbsanayiIsbirligiDurumu,cbProjePatenti,cbParaBirimi,cbProjeKapsamaAlani,cbAr_geNiteligi,cbFirmaParaBirimi,cbProjeTuru,projeekibi;

    public Grid grdgorevliakademisyen;
    public TextField txtTcgorevli;
    public ComboBox cbgorev;


    public  Button gbtnKaydet,btnekle,btncikar;
    public DeclarativeProjeBasvuruGuncelle() {
        Design.read(this);
        setSizeUndefined();
        pnlPatent.setVisible(false);

        pnlSanayi.setVisible(false);
        pnlekip.setVisible(false);




    }
}
