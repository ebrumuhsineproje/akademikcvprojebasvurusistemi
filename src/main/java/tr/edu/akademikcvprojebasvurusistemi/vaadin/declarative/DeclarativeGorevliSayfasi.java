package tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.*;
import com.vaadin.ui.declarative.Design;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import tr.edu.akademikcvprojebasvurusistemi.domain.Akademisyen;
import tr.edu.akademikcvprojebasvurusistemi.domain.Yayinlar;
import tr.edu.akademikcvprojebasvurusistemi.repository.YayinRepository;
import tr.edu.akademikcvprojebasvurusistemi.service.AkademisyenService;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by LENOVO on 23.2.2017.
 */
@DesignRoot
public class DeclarativeGorevliSayfasi extends VerticalLayout {
    public Grid projeveakademisyenlistesi ;
    public DateField baslangic,bitis;
    public Button btn,raporal,kabuledilmis;
    public Button onayla,imzabekliyor,btnara,istatistik;
    public ComboBox cbprojeturu,cbonaydurumu;

    @Autowired
    AkademisyenService akademisyenService;

    @Autowired
    YayinRepository yayinRepository;
    public DeclarativeGorevliSayfasi(AkademisyenService _akademisyen) {
        akademisyenService = _akademisyen;
        //setSizeFull();
        Design.read(this);
        
       // btn.addStyleName(ValoTheme.BUTTON_LINK);

        projeveakademisyenlistesi.setSizeFull();
        projeveakademisyenlistesi.setSelectionMode(Grid.SelectionMode.MULTI);
       //baslangic.setInputPrompt("başlangıç seçiniz");
       // bitis.setInputPrompt("bitiş seçiniz");
    }


}
