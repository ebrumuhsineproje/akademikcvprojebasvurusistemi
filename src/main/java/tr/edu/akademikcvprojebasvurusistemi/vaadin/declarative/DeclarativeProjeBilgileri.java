/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.*;
import com.vaadin.ui.declarative.Design;
import com.vaadin.ui.themes.ValoTheme;

/**
 *
 * @author ubuntu
 */
@DesignRoot
public class DeclarativeProjeBilgileri extends VerticalLayout{

    public Panel pnlKisi,pnlProje;
    public TextField txtAdi,txtSoyadi,txtTC,txtBirim,txtBolum,txtAnaBilimDali,txtKadroUnvani,txtEposta,txtCepTel,txtIsTel;
    public  TextField txtProjeAdi,txtProjeOzeti,txtUlke,txtSehir,txtButcesi,txtKurumKatkiPayi,
            txtPatentAdi,txtPatentKodu,txtFirmaAdi,txtSagladigiFon;

    public Panel pnlPatent,pnlSanayi,pnlekip;
    public  DateField txtBaslangicTarihi,txtBitisTarihi,txtPatentYili;

    public ComboBox txtProjedeAlinanGorev,cbPatentKapsami,cbsanayiIsbirligiDurumu,cbProjePatenti,cbParaBirimi,cbProjeKapsamaAlani,cbAr_geNiteligi,cbFirmaParaBirimi,cbProjeTuru;
    public Button btnKaydet,don,don2;
    public ComboBox projeekibi,cbgorev;
    public Grid grdgorevliakademisyen;

    public TextField txtTcgorevli;

    public Button btnekle,btncikar;


    public DeclarativeProjeBilgileri() {
        Design.read(this);
        setSizeUndefined();


        grdgorevliakademisyen.setStyleName("anasayfagrid");

        grdgorevliakademisyen.setSelectionMode(Grid.SelectionMode.SINGLE);
        //txtProjeAdi.setVisible(false);
        pnlPatent.setVisible(false);
        pnlSanayi.setVisible(false);
       // pnlKisi.setVisible(false);

        pnlekip.setVisible(false);
        don.setStyleName(ValoTheme.BUTTON_LINK);

        don2.setStyleName(ValoTheme.BUTTON_LINK);





    }

}
