package tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;
import com.vaadin.ui.declarative.Design;
import sun.security.util.Password;

@DesignRoot
public class DeclarativeLoginView extends VerticalLayout {

    public TextField txtTc;
    public PasswordField txtPass;
    public Button btnLogin;

    public DeclarativeLoginView(){
        Design.read(this);
        setSizeUndefined();
        btnLogin.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        btnLogin.addStyleName("btnlogin");
        txtTc.setIcon(FontAwesome.USER);
        txtPass.setIcon(FontAwesome.KEY);

        txtTc.setRequired(false);
        txtTc.setRequiredError("lütfen geçerli Tc giriniz");

        txtPass.setRequired(false);
        txtPass.setRequiredError("lütfen geçerli şifrenizi giriniz");

    }
}
