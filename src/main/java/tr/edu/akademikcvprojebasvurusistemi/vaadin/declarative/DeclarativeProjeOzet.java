package tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.*;
import com.vaadin.ui.declarative.Design;

/**
 * Created by LENOVO on 19.1.2017.
 */
@DesignRoot
public class DeclarativeProjeOzet extends VerticalLayout {


    public Panel pnlProje;
    public  TextField txtProjeAdi,txtProjeOzeti,txtProjedeAlinanGorev,txtUlke,txtSehir,txtButcesi,txtKurumKatkiPayi;

    public  DateField txtBaslangicTarihi,txtBitisTarihi;

    public ComboBox cbProjePatenti,cbParaBirimi,cbProjeKapsamaAlani,cbAr_geNiteligi,cbsanayiIsbirligiDurumu,cbProjeTuru;


    public Button btncikti;
    public DeclarativeProjeOzet() {
        Design.read(this);

        setSizeUndefined();
    }

}
