/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.*;
import com.vaadin.ui.declarative.Design;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import tr.edu.akademikcvprojebasvurusistemi.domain.*;
import tr.edu.akademikcvprojebasvurusistemi.service.AkademisyenService;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.extras.MyNotification;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.extras.enums.MessageType;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 *
 * @author LENOVO
 */
@DesignRoot
public class DeclarativeEgitimBilgileriGirisView extends Panel{
    public Button kaydetlise,kaydetlisans,kaydetyukseklisans,kaydetdoktora,guncellelise,guncellelisans,guncelleyukseklisans,guncelledoktora,sillise,silyuksek,sillisans,sildoktora;
    public VerticalLayout vRoot;



    public Grid lisebilgisi,lisansbilgisi,yükseklisansbilgisi,doktorabilgisi;



    /*
        public DeclarativeEgitimBilgileriGirisView() {
            Design.read(this);
            setSizeFull();

            lisebilgisi.setSizeFull();

            lisansbilgisi.setSizeFull();

            doktorabilgisi.setSizeFull();

            yükseklisansbilgisi.setSizeFull();
            sildoktora.addStyleName(ValoTheme.BUTTON_LINK);
            sillisans.addStyleName(ValoTheme.BUTTON_LINK);
            sillise.addStyleName(ValoTheme.BUTTON_LINK);
            silyuksek.addStyleName(ValoTheme.BUTTON_LINK);
            silyuksek.setIcon(FontAwesome.CLOSE);
            sillise.setIcon(FontAwesome.CLOSE);
            sillisans.setIcon(FontAwesome.CLOSE);
            sildoktora.setIcon(FontAwesome.CLOSE);


            lisebilgisi.setSelectionMode(Grid.SelectionMode.SINGLE);
            lisansbilgisi.setSelectionMode(Grid.SelectionMode.SINGLE);
            yükseklisansbilgisi.setSelectionMode(Grid.SelectionMode.SINGLE);
            doktorabilgisi.setSelectionMode(Grid.SelectionMode.SINGLE);
            kaydetlise.setIcon(FontAwesome.PLUS);
            kaydetlise.addStyleName(ValoTheme.BUTTON_LINK);
            kaydetlisans.setIcon(FontAwesome.PLUS);
            kaydetlisans.addStyleName(ValoTheme.BUTTON_LINK);
            kaydetyukseklisans.setIcon(FontAwesome.PLUS);
            kaydetyukseklisans.addStyleName(ValoTheme.BUTTON_LINK);
            kaydetdoktora.setIcon(FontAwesome.PLUS);
            kaydetdoktora.addStyleName(ValoTheme.BUTTON_LINK);
            guncellelisans.setIcon(FontAwesome.EDIT);
            guncellelisans.addStyleName(ValoTheme.BUTTON_LINK);
            guncelleyukseklisans.setIcon(FontAwesome.EDIT);
            guncelleyukseklisans.addStyleName(ValoTheme.BUTTON_LINK);
            guncelledoktora.setIcon(FontAwesome.EDIT);
            guncelledoktora.addStyleName(ValoTheme.BUTTON_LINK);
            guncellelise.setIcon(FontAwesome.EDIT);
            guncellelise.addStyleName(ValoTheme.BUTTON_LINK);


            vRoot.setSizeUndefined();



        }*/
    @Autowired
    AkademisyenService akademisyenService;
    public DeclarativeEgitimBilgileriGirisView(AkademisyenService _akademisyen)
    {
        Akademisyen a = (Akademisyen) VaadinSession.getCurrent().getSession().getAttribute("akademisyen");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        akademisyenService=_akademisyen;
        Design.read(this);
        lisebilgisi.setSizeFull();

        lisansbilgisi.setSizeFull();

        doktorabilgisi.setSizeFull();

        yükseklisansbilgisi.setSizeFull();
        sildoktora.addStyleName(ValoTheme.BUTTON_LINK);
        sillisans.addStyleName(ValoTheme.BUTTON_LINK);
        sillise.addStyleName(ValoTheme.BUTTON_LINK);
        silyuksek.addStyleName(ValoTheme.BUTTON_LINK);
        silyuksek.setIcon(FontAwesome.CLOSE);
        sillise.setIcon(FontAwesome.CLOSE);
        sillisans.setIcon(FontAwesome.CLOSE);
        sildoktora.setIcon(FontAwesome.CLOSE);


        lisebilgisi.setSelectionMode(Grid.SelectionMode.SINGLE);
        lisansbilgisi.setSelectionMode(Grid.SelectionMode.SINGLE);
        yükseklisansbilgisi.setSelectionMode(Grid.SelectionMode.SINGLE);
        doktorabilgisi.setSelectionMode(Grid.SelectionMode.SINGLE);
        kaydetlise.setIcon(FontAwesome.PLUS);
        kaydetlise.addStyleName(ValoTheme.BUTTON_LINK);
        kaydetlisans.setIcon(FontAwesome.PLUS);
        kaydetlisans.addStyleName(ValoTheme.BUTTON_LINK);
        kaydetyukseklisans.setIcon(FontAwesome.PLUS);
        kaydetyukseklisans.addStyleName(ValoTheme.BUTTON_LINK);
        kaydetdoktora.setIcon(FontAwesome.PLUS);
        kaydetdoktora.addStyleName(ValoTheme.BUTTON_LINK);
        guncellelisans.setIcon(FontAwesome.EDIT);
        guncellelisans.addStyleName(ValoTheme.BUTTON_LINK);
        guncelleyukseklisans.setIcon(FontAwesome.EDIT);
        guncelleyukseklisans.addStyleName(ValoTheme.BUTTON_LINK);
        guncelledoktora.setIcon(FontAwesome.EDIT);
        guncelledoktora.addStyleName(ValoTheme.BUTTON_LINK);
        guncellelise.setIcon(FontAwesome.EDIT);
        guncellelise.addStyleName(ValoTheme.BUTTON_LINK);


        java.util.List<Lise> liseler = null;
        liseler= akademisyenService.gettumliseler(a.getId());


        BeanItemContainer<Lise> liseBeanItemContainer = new BeanItemContainer<>(Lise.class);
        lisebilgisi.setContainerDataSource(liseBeanItemContainer);
        lisebilgisi.getContainerDataSource().removeAllItems();
        liseBeanItemContainer.addAll(liseler);
        lisebilgisi.setContainerDataSource(liseBeanItemContainer);

        lisebilgisi.removeAllColumns();
        lisebilgisi.addColumn("liseadi");
        lisebilgisi.addColumn("lisebaslangic");
        lisebilgisi.addColumn("lisebitis");

        lisebilgisi.getColumn("liseadi").setHeaderCaption("ADI");
        lisebilgisi.getColumn("lisebaslangic").setHeaderCaption("BAŞLANGIÇ");
        lisebilgisi.getColumn("lisebitis").setHeaderCaption("BİTİŞ");

        lisebilgisi.setImmediate(true);

        java.util.List<Lisans> lisanslar = null;
        lisanslar= akademisyenService.gettumlisanslar(a.getId());


        BeanItemContainer<Lisans> lisansBeanItemContainer = new BeanItemContainer<>(Lisans.class);
        lisansbilgisi.setContainerDataSource(lisansBeanItemContainer);
        lisansbilgisi.getContainerDataSource().removeAllItems();
        lisansBeanItemContainer.addAll(lisanslar);
        lisansbilgisi.setContainerDataSource(lisansBeanItemContainer);

        lisansbilgisi.removeAllColumns();
        lisansbilgisi.addColumn("lisansuniv");
        lisansbilgisi.addColumn("lisansfakulte");
        lisansbilgisi.addColumn("lisansbolum");
        lisansbilgisi.addColumn("lisansbaslangic");
        lisansbilgisi.addColumn("lisansbitis");
        lisansbilgisi.getColumn("lisansuniv").setHeaderCaption("ÜNİVERSİTE");
        lisansbilgisi.getColumn("lisansfakulte").setHeaderCaption("FAKÜLTE");
        lisansbilgisi.getColumn("lisansbolum").setHeaderCaption("BÖLÜM");
        lisansbilgisi.getColumn("lisansbaslangic").setHeaderCaption("BAŞLANGIÇ");
        lisansbilgisi.getColumn("lisansbitis").setHeaderCaption("BİTİŞ");

        java.util.List<YuksekLisans> yukseklisanslar = null;
        yukseklisanslar= akademisyenService.gettumyuksekler(a.getId());


        BeanItemContainer<YuksekLisans> yuksekLisansBeanItemContainer = new BeanItemContainer<>(YuksekLisans.class);
        yükseklisansbilgisi.setContainerDataSource(yuksekLisansBeanItemContainer);
        yükseklisansbilgisi.getContainerDataSource().removeAllItems();
        yuksekLisansBeanItemContainer.addAll(yukseklisanslar);
        yükseklisansbilgisi.setContainerDataSource(yuksekLisansBeanItemContainer);

        yükseklisansbilgisi.removeAllColumns();
        yükseklisansbilgisi.addColumn("yuksekLisansuniv");
        yükseklisansbilgisi.addColumn("yuksekLisansfakulte");
        yükseklisansbilgisi.addColumn("yuksekLisansbolum");

        yükseklisansbilgisi.addColumn("yuksekLisansbaslangic");
        yükseklisansbilgisi.addColumn("yuksekLisansbitis");

        yükseklisansbilgisi.getColumn("yuksekLisansuniv").setHeaderCaption("ÜNİVERSİTE");
        yükseklisansbilgisi.getColumn("yuksekLisansfakulte").setHeaderCaption("FAKÜLTE");
        yükseklisansbilgisi.getColumn("yuksekLisansbolum").setHeaderCaption("BÖLÜM");
        yükseklisansbilgisi.getColumn("yuksekLisansbaslangic").setHeaderCaption("BAŞLANGIÇ");
        yükseklisansbilgisi.getColumn("yuksekLisansbitis").setHeaderCaption("BİTİŞ");



        java.util.List<Doktora> doktoralar = null;
        doktoralar= akademisyenService.gettumdoktoralar(a.getId());


        BeanItemContainer<Doktora> doktoraBeanItemContainer = new BeanItemContainer<>(Doktora.class);
        doktorabilgisi.setContainerDataSource(doktoraBeanItemContainer);
        doktorabilgisi.getContainerDataSource().removeAllItems();
        doktoraBeanItemContainer.addAll(doktoralar);
        doktorabilgisi.setContainerDataSource(doktoraBeanItemContainer);

        doktorabilgisi.removeAllColumns();
        doktorabilgisi.addColumn("doktorauniv");
        doktorabilgisi.addColumn("doktorafakulte");
        doktorabilgisi.addColumn("doktorabolum");
        doktorabilgisi.addColumn("doktorabaslangic");
        doktorabilgisi.addColumn("doktorabitis");
        doktorabilgisi.getColumn("doktorauniv").setHeaderCaption("ÜNİVERSİTE");
        doktorabilgisi.getColumn("doktorafakulte").setHeaderCaption("FAKÜLTE");
        doktorabilgisi.getColumn("doktorabolum").setHeaderCaption("BÖLÜM");
        doktorabilgisi.getColumn("doktorabaslangic").setHeaderCaption("BAŞLANGIÇ");
        doktorabilgisi.getColumn("doktorabitis").setHeaderCaption("BİTİŞ");




        kaydetlise.addClickListener(clickEvent -> {

            DeclarativeLiseKaydet declarativeLiseKaydet=new DeclarativeLiseKaydet();

            final Window window=new Window("ekleme ekranı");
            window.setModal(true);
            window.setWidth(400.0f,Unit.PIXELS);
            window.setHeight(550.0f,Unit.PIXELS);
            window.setDraggable(false);
            window.setResizable(false);
            window.setContent(declarativeLiseKaydet);
            UI.getCurrent().addWindow(window);


            int ilksayi=akademisyenService.gettumliseler(a.getId()).size();


            declarativeLiseKaydet.btnLiseKaydet.addClickListener(new Button.ClickListener() {


                @Override
                public void buttonClick(Button.ClickEvent clickEvent) {
                    Lise lise=new Lise();

                    if(declarativeLiseKaydet.liseadi.getValue()==null){
                        declarativeLiseKaydet.liseadi.setRequired(true);
                    }
                    else if(declarativeLiseKaydet.lisebaslangic.getValue()==null){
                        declarativeLiseKaydet.lisebaslangic.setRequired(true);
                    }
                    else if(declarativeLiseKaydet.lisebitis.getValue()==null){
                        declarativeLiseKaydet.lisebitis.setRequired(true);
                    }
                    else{
                        lise.setLiseadi(declarativeLiseKaydet.liseadi.getValue().toString());
                        lise.setLisebaslangic(simpleDateFormat.format(declarativeLiseKaydet.lisebaslangic.getValue()));

                        lise.setLisebitis(simpleDateFormat.format(declarativeLiseKaydet.lisebitis.getValue()));
                        a.getLiseler().add(lise);

                        akademisyenService.akademisyenEkle(a);

                        int sonsayi=akademisyenService.gettumliseler(a.getId()).size();

                        if(sonsayi>ilksayi){
                            for(int i=0;i<2;i++){
                                Iterator<Window> window = getUI().getWindows().iterator();

                                getUI().removeWindow(window.next());
                                java.util.List<Lise> yeni = null;
                                yeni= akademisyenService.gettumliseler(a.getId());
                                akademisyenService.refreshGrid(yeni,Lise.class,lisebilgisi);

                                new MyNotification("kaydetme başarılı", MessageType.SUCCESS);



                            }
                        }

                    }



                }
            });





        });


        kaydetlisans.addClickListener(clickEvent -> {
            DeclarativeLisansKaydet declarativeLisansKaydet =new DeclarativeLisansKaydet();
            final Window window=new Window("ekleme ekranı");
            window.setModal(true);
            window.setWidth(400.0f,Unit.PIXELS);
            window.setHeight(550.0f,Unit.PIXELS);
            window.setDraggable(false);
            window.setResizable(false);
            window.setContent(declarativeLisansKaydet);
            UI.getCurrent().addWindow(window);
            int ilksayilisan=akademisyenService.gettumlisanslar(a.getId()).size();

            declarativeLisansKaydet.btnLisansKaydet.addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent clickEvent) {
                    Lisans lisans=new Lisans();

                    if(declarativeLisansKaydet.lisansuniv.getValue()==null){
                        declarativeLisansKaydet.lisansuniv.setRequired(true);
                    }
                    else if(declarativeLisansKaydet.lisansfakulte.getValue()==null){
                        declarativeLisansKaydet.lisansfakulte.setRequired(true);
                    }
                    else if(declarativeLisansKaydet.lisansbolum.getValue()==null){
                        declarativeLisansKaydet.lisansbolum.setRequired(true);
                    }
                    else{
                        lisans.setLisansuniv(declarativeLisansKaydet.lisansuniv.getValue().toString());
                        lisans.setLisansfakulte(declarativeLisansKaydet.lisansfakulte.getValue().toString());
                        lisans.setLisansbolum(declarativeLisansKaydet.lisansbolum.getValue().toString());
                        lisans.setLisansbaslangic(simpleDateFormat.format(declarativeLisansKaydet.lisansbaslangic.getValue()));
                        lisans.setLisansbitis(simpleDateFormat.format(declarativeLisansKaydet.lisansbitis.getValue()));
                        a.getLisanslar().add(lisans);
                        akademisyenService.akademisyenEkle(a);
                        int sonsayilisans=akademisyenService.gettumlisanslar(a.getId()).size();


                        if(sonsayilisans>ilksayilisan){
                            for(int i=0;i<2;i++){
                                Iterator<Window> window = getUI().getWindows().iterator();

                                getUI().removeWindow(window.next());
                                java.util.List<Lisans> yeni = null;
                                yeni= akademisyenService.gettumlisanslar(a.getId());
                                akademisyenService.refreshGrid(yeni,Lisans.class,lisansbilgisi);

                                new MyNotification("kaydetme başarılı", MessageType.SUCCESS);



                            }
                        }



                    }

                }

            });

        });


        kaydetyukseklisans.addClickListener(clickEvent -> {
            DeclarativeYuksekLisansKaydet declarativeYuksekLisansKaydet=new DeclarativeYuksekLisansKaydet();

            final Window window=new Window("ekleme ekranı");
            window.setModal(true);
            window.setWidth(400.0f,Unit.PIXELS);
            window.setHeight(550.0f,Unit.PIXELS);
            window.setDraggable(false);
            window.setResizable(false);
            window.setContent(declarativeYuksekLisansKaydet);
            UI.getCurrent().addWindow(window);
            int ilksayiyuksek=akademisyenService.gettumyuksekler(a.getId()).size();

            declarativeYuksekLisansKaydet.btnYuksekLisansKaydet.addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent clickEvent) {

                    YuksekLisans yuksekLisans=new YuksekLisans();
                    if(declarativeYuksekLisansKaydet.yukseklisansuniv.getValue()==null){
                        declarativeYuksekLisansKaydet.yukseklisansuniv.setRequired(true);
                    }
                    else if(declarativeYuksekLisansKaydet.yukseklisansfakulte.getValue()==null){
                        declarativeYuksekLisansKaydet.yukseklisansfakulte.setRequired(true);
                    }
                    else if(declarativeYuksekLisansKaydet.yukseklisansbolum.getValue()==null){
                        declarativeYuksekLisansKaydet.yukseklisansbolum.setRequired(true);
                    }
                    else {


                        yuksekLisans.setYuksekLisansuniv(declarativeYuksekLisansKaydet.yukseklisansuniv.getValue().toString());
                        yuksekLisans.setYuksekLisansfakulte(declarativeYuksekLisansKaydet.yukseklisansfakulte.getValue().toString());
                        yuksekLisans.setYuksekLisansbolum(declarativeYuksekLisansKaydet.yukseklisansbolum.getValue().toString());
                        yuksekLisans.setYuksekLisansbaslangic(simpleDateFormat.format(declarativeYuksekLisansKaydet.yukseklisansbaslangic.getValue()));
                        yuksekLisans.setYuksekLisansbitis(simpleDateFormat.format(declarativeYuksekLisansKaydet.yukseklisansbitis.getValue()));
                        a.getYükseklisanslar().add(yuksekLisans);
                        akademisyenService.akademisyenEkle(a);

                        int sonsayiyuksek = akademisyenService.gettumyuksekler(a.getId()).size();

                        if (sonsayiyuksek > ilksayiyuksek) {
                            for (int i = 0; i < 2; i++) {
                                Iterator<Window> window = getUI().getWindows().iterator();

                                getUI().removeWindow(window.next());
                                java.util.List<YuksekLisans> yeni = null;
                                yeni= akademisyenService.gettumyuksekler(a.getId());
                                akademisyenService.refreshGrid(yeni,YuksekLisans.class,yükseklisansbilgisi);

                                new MyNotification("kaydetme başarılı", MessageType.SUCCESS);


                            }
                        }
                    }



                }

            });

        });
        kaydetdoktora.addClickListener(clickEvent -> {

            DeclarativeDoktoraKaydet declarativeDoktoraKaydet=new DeclarativeDoktoraKaydet();
            final Window window=new Window("ekleme ekranı");
            window.setModal(true);
            window.setWidth(400.0f,Unit.PIXELS);
            window.setHeight(600.0f,Unit.PIXELS);
            window.setDraggable(false);
            window.setResizable(false);
            window.setContent(declarativeDoktoraKaydet);
            UI.getCurrent().addWindow(window);
            int  ilksayidoktora=akademisyenService.gettumdoktoralar(a.getId()).size();

            declarativeDoktoraKaydet.btnDoktoraKaydet.addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent clickEvent) {

                    Doktora doktora = new Doktora();
                    if(declarativeDoktoraKaydet.doktorauniv.getValue()==null){
                        declarativeDoktoraKaydet.doktorauniv.setRequired(true);
                    }
                    else if(declarativeDoktoraKaydet.doktorafakulte.getValue()==null){
                        declarativeDoktoraKaydet.doktorafakulte.setRequired(true);
                    }
                    else if(declarativeDoktoraKaydet.doktorabolum.getValue()==null){
                        declarativeDoktoraKaydet.doktorabolum.setRequired(true);
                    }
                    else if(declarativeDoktoraKaydet.doktorabaslangic.getValue()==null){
                        declarativeDoktoraKaydet.doktorabaslangic.setRequired(true);

                    }
                    else if(declarativeDoktoraKaydet.doktorabitis.getValue()==null && declarativeDoktoraKaydet.devam.getValue()==false){
                        declarativeDoktoraKaydet.doktorabitis.setRequired(true);
                    }

                    else{
                        doktora.setDevam(declarativeDoktoraKaydet.devam.getValue());
                        if(declarativeDoktoraKaydet.devam.getValue()==true){
                            doktora.setDoktorabitis("devam ediyor");
                        }
                        else {
                            doktora.setDoktorabitis(simpleDateFormat.format(declarativeDoktoraKaydet.doktorabitis.getValue()));
                        }
                        doktora.setDoktorauniv(declarativeDoktoraKaydet.doktorauniv.getValue().toString());
                        doktora.setDoktorafakulte(declarativeDoktoraKaydet.doktorafakulte.getValue().toString());
                        doktora.setDoktorabolum(declarativeDoktoraKaydet.doktorabolum.getValue().toString());
                        doktora.setDoktorabaslangic(simpleDateFormat.format(declarativeDoktoraKaydet.doktorabaslangic.getValue()));


                        a.getDoktoralar().add(doktora);
                        akademisyenService.akademisyenEkle(a);

                        int sonsayidoktora=akademisyenService.gettumdoktoralar(a.getId()).size();

                        if(sonsayidoktora>ilksayidoktora){
                            for(int i=0;i<2;i++){
                                Iterator<Window> window = getUI().getWindows().iterator();

                                getUI().removeWindow(window.next());
                                java.util.List<Doktora> yeni = null;
                                yeni= akademisyenService.gettumdoktoralar(a.getId());
                                akademisyenService.refreshGrid(yeni,Doktora.class,doktorabilgisi);

                                new MyNotification("kaydetme başarılı", MessageType.SUCCESS);



                            }
                        }


                    }


                }

            });


        });



        guncellelise.addClickListener(clickEvent-> {

            Lise secililise = (Lise) lisebilgisi.getSelectedRow();

            if(secililise!=null) {


                DeclarativeLiseGuncelle declarativeLiseGuncelle = new DeclarativeLiseGuncelle();

                declarativeLiseGuncelle.guncelliseadi.setValue(akademisyenService.teklisegetir(secililise).getLiseadi().toString());


                //declarativeLiseGuncelle.guncellisebaslangic.setValue(new Date());


                declarativeLiseGuncelle.guncellisebaslangic.setDateFormat("dd-MM-yyyy");
                Date date = null;
                try {
                    date = simpleDateFormat.parse(akademisyenService.teklisegetir(secililise).getLisebaslangic());
                    declarativeLiseGuncelle.guncellisebaslangic.setValue(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                declarativeLiseGuncelle.guncellisebitis.setDateFormat("dd-MM-yyyy");
                try {
                    date = simpleDateFormat.parse(akademisyenService.teklisegetir(secililise).getLisebitis());
                    declarativeLiseGuncelle.guncellisebitis.setValue(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                final Window window = new Window("guncelleme ekranı");
                window.setModal(true);
                window.setWidth(400.0f, Unit.PIXELS);
                window.setHeight(550.0f, Unit.PIXELS);
                window.setDraggable(false);
                window.setResizable(false);
                window.setContent(declarativeLiseGuncelle);


                UI.getCurrent().addWindow(window);


                declarativeLiseGuncelle.btnliseguncelle.addClickListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {

                        if (declarativeLiseGuncelle.guncelliseadi.getValue() == null) {
                            declarativeLiseGuncelle.guncelliseadi.setRequired(true);
                        } else if (declarativeLiseGuncelle.guncellisebaslangic.getValue() == null) {
                            declarativeLiseGuncelle.guncellisebaslangic.setRequired(true);
                        } else if (declarativeLiseGuncelle.guncellisebitis.getValue() == null) {
                            declarativeLiseGuncelle.guncellisebitis.setRequired(true);
                        } else {
                            secililise.liseadi = declarativeLiseGuncelle.guncelliseadi.getValue().toString();

                            secililise.lisebaslangic = simpleDateFormat.format(declarativeLiseGuncelle.guncellisebaslangic.getValue());

                            secililise.lisebitis = simpleDateFormat.format(declarativeLiseGuncelle.guncellisebitis.getValue());
                            akademisyenService.liseguncelle(secililise);
                            for (int i = 0; i < 2; i++) {
                                Iterator<Window> window = getUI().getWindows().iterator();

                                getUI().removeWindow(window.next());
                                java.util.List<Lise> yeni = null;
                                yeni= akademisyenService.gettumliseler(a.getId());
                                akademisyenService.refreshGrid(yeni,Lise.class,lisebilgisi);

                                new MyNotification("güncelleme başarılı", MessageType.SUCCESS);


                            }
                        }


                    }
                    /*
                    Lise l=new Lise();
                    l=akademisyenService.liseGetir(a.getId());
                    l.liseadi=declarativeLiseGuncelle.guncelliseadi.getValue().toString();
                    l.lisebaslangic=simpleDateFormat.format(declarativeLiseGuncelle.guncellisebaslangic.getValue());
                    l.lisebitis=simpleDateFormat.format(declarativeLiseGuncelle.guncellisebitis.getValue());
                   // a.getLise().liseadi=declarativeLiseGuncelle.guncelliseadi.getValue().toString();

                   // akademisyenService.liseGetir(a.getId()).lisebaslangic=simpleDateFormat.format(declarativeLiseGuncelle.guncellisebaslangic.getValue());
                   // akademisyenService.liseGetir(a.getId()).lisebitis=simpleDateFormat.format(declarativeLiseGuncelle.guncellisebitis.getValue());

                    akademisyenService.liseguncelle(l);
                    */


                });
            }
            else {
                new MyNotification("güncellemek istediğiniz liseyi seçin",MessageType.FAIL);
            }



        });
        guncellelisans.addClickListener(clickEvent -> {


            DeclarativeLisansGuncelle declarativeLisansGuncelle= new DeclarativeLisansGuncelle();

            Lisans secililisans = (Lisans) lisansbilgisi.getSelectedRow();

            if(secililisans!=null) {


                declarativeLisansGuncelle.guncellisansuniv.setValue(akademisyenService.teklisansgetir(secililisans).getLisansuniv().toString());
                declarativeLisansGuncelle.guncellisansfakulte.setValue(akademisyenService.teklisansgetir(secililisans).getLisansfakulte().toString());
                declarativeLisansGuncelle.guncellisansbolum.setValue(akademisyenService.teklisansgetir(secililisans).getLisansbolum().toString());


                declarativeLisansGuncelle.guncellisansbaslangic.setDateFormat("dd-MM-yyyy");
                Date date = null;
                try {
                    date = simpleDateFormat.parse(akademisyenService.teklisansgetir(secililisans).getLisansbaslangic());
                    declarativeLisansGuncelle.guncellisansbaslangic.setValue(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                declarativeLisansGuncelle.guncellisansbitis.setDateFormat("dd-MM-yyyy");


                try {
                    date = simpleDateFormat.parse(akademisyenService.teklisansgetir(secililisans).getLisansbitis());
                    declarativeLisansGuncelle.guncellisansbitis.setValue(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                final Window window = new Window("guncelleme ekranı");
                window.setModal(true);
                window.setWidth(400.0f, Unit.PIXELS);
                window.setHeight(550.0f, Unit.PIXELS);
                window.setDraggable(false);
                window.setResizable(false);
                window.setContent(declarativeLisansGuncelle);

                UI.getCurrent().addWindow(window);
                declarativeLisansGuncelle.btnlisansguncelle.addClickListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {

                        //Lisans lisans = new Lisans();

                        if (declarativeLisansGuncelle.guncellisansuniv.getValue() == null) {
                            declarativeLisansGuncelle.guncellisansuniv.setRequired(true);
                        } else if (declarativeLisansGuncelle.guncellisansfakulte.getValue() == null) {
                            declarativeLisansGuncelle.guncellisansfakulte.setRequired(true);
                        } else if (declarativeLisansGuncelle.guncellisansbolum.getValue() == null) {
                            declarativeLisansGuncelle.guncellisansbolum.setRequired(true);
                        } else {


                            secililisans.Lisansuniv = declarativeLisansGuncelle.guncellisansuniv.getValue().toString();
                            secililisans.Lisansbolum = declarativeLisansGuncelle.guncellisansbolum.getValue().toString();
                            secililisans.Lisansfakulte = declarativeLisansGuncelle.guncellisansfakulte.getValue().toString();


                            secililisans.Lisansbaslangic = simpleDateFormat.format(declarativeLisansGuncelle.guncellisansbaslangic.getValue());
                            secililisans.Lisansbitis = simpleDateFormat.format(declarativeLisansGuncelle.guncellisansbitis.getValue());

                            akademisyenService.lisansguncelle(secililisans);
                            for (int i = 0; i < 2; i++) {
                                Iterator<Window> window = getUI().getWindows().iterator();

                                getUI().removeWindow(window.next());
                                java.util.List<Lisans> yeni = null;
                                yeni= akademisyenService.gettumlisanslar(a.getId());
                                akademisyenService.refreshGrid(yeni,Lisans.class,lisansbilgisi);

                                new MyNotification("güncelleme başarılı", MessageType.SUCCESS);


                            }

                        }
                    }


                    /*
                    lisans=akademisyenService.lisansGetir(a.getId());
                    lisans.Lisansuniv=declarativeLisansGuncelle.guncellisansuniv.getValue().toString();
                    lisans.Lisansbolum=declarativeLisansGuncelle.guncellisansbolum.getValue().toString();
                    lisans.Lisansfakulte=declarativeLisansGuncelle.guncellisansfakulte.getValue().toString();
                    lisans.Lisansbaslangic=simpleDateFormat.format(declarativeLisansGuncelle.guncellisansbaslangic.getValue());
                    lisans.Lisansbitis=simpleDateFormat.format(declarativeLisansGuncelle.guncellisansbitis.getValue());
                    akademisyenService.lisansguncelle(lisans);
                    */


                    //a.getLisans().Lisansuniv=declarativeLisansGuncelle.guncellisansuniv.getValue().toString();
                    //akademisyenService.lisansguncelle(a.getLisans());

                });
            }
            else {
                new MyNotification("güncellemek istediğiniz lisansı seçin",MessageType.FAIL);
            }



        });
        guncelleyukseklisans.addClickListener(clickEvent -> {


            DeclarativeYuksekLisansGuncelle declarativeYuksekLisansGuncelle= new DeclarativeYuksekLisansGuncelle();
            YuksekLisans seciliyukseklisans = (YuksekLisans) yükseklisansbilgisi.getSelectedRow();

            if(seciliyukseklisans!=null) {


                declarativeYuksekLisansGuncelle.guncelyukseklisansuniv.setValue(akademisyenService.tekyuksekgetir(seciliyukseklisans).getYuksekLisansuniv().toString());
                declarativeYuksekLisansGuncelle.guncelyukseklisansbolum.setValue(akademisyenService.tekyuksekgetir(seciliyukseklisans).getYuksekLisansbolum().toString());
                declarativeYuksekLisansGuncelle.guncelyukseklisansfakulte.setValue(akademisyenService.tekyuksekgetir(seciliyukseklisans).getYuksekLisansfakulte().toString());

                declarativeYuksekLisansGuncelle.guncelyukseklisansbaslangic.setDateFormat("dd-MM-yyyy");
                Date date = null;
                try {
                    date = simpleDateFormat.parse(akademisyenService.tekyuksekgetir(seciliyukseklisans).getYuksekLisansbaslangic());
                    declarativeYuksekLisansGuncelle.guncelyukseklisansbaslangic.setValue(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                declarativeYuksekLisansGuncelle.guncelyukseklisansbitis.setDateFormat("dd-MM-yyyy");


                try {
                    date = simpleDateFormat.parse(akademisyenService.tekyuksekgetir(seciliyukseklisans).getYuksekLisansbitis());
                    declarativeYuksekLisansGuncelle.guncelyukseklisansbitis.setValue(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                final Window window = new Window("guncelleme ekranı");
                window.setModal(true);
                window.setWidth(400.0f, Unit.PIXELS);
                window.setHeight(550.0f, Unit.PIXELS);
                window.setDraggable(false);
                window.setResizable(false);
                window.setContent(declarativeYuksekLisansGuncelle);

                UI.getCurrent().addWindow(window);
                declarativeYuksekLisansGuncelle.btnyukseklisansguncelle.addClickListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {

                        YuksekLisans yuksekLisans = new YuksekLisans();

                        if (declarativeYuksekLisansGuncelle.guncelyukseklisansuniv.getValue() == null) {
                            declarativeYuksekLisansGuncelle.guncelyukseklisansuniv.setRequired(true);
                        } else if (declarativeYuksekLisansGuncelle.guncelyukseklisansfakulte.getValue() == null) {
                            declarativeYuksekLisansGuncelle.guncelyukseklisansfakulte.setRequired(true);
                        } else if (declarativeYuksekLisansGuncelle.guncelyukseklisansbolum.getValue() == null) {
                            declarativeYuksekLisansGuncelle.guncelyukseklisansbolum.setRequired(true);
                        } else {


                            seciliyukseklisans.YuksekLisansuniv = declarativeYuksekLisansGuncelle.guncelyukseklisansuniv.getValue().toString();
                            seciliyukseklisans.YuksekLisansbolum = declarativeYuksekLisansGuncelle.guncelyukseklisansbolum.getValue().toString();
                            seciliyukseklisans.YuksekLisansfakulte = declarativeYuksekLisansGuncelle.guncelyukseklisansfakulte.getValue().toString();

                            seciliyukseklisans.YuksekLisansbaslangic = simpleDateFormat.format(declarativeYuksekLisansGuncelle.guncelyukseklisansbaslangic.getValue());
                            seciliyukseklisans.YuksekLisansbitis = simpleDateFormat.format(declarativeYuksekLisansGuncelle.guncelyukseklisansbitis.getValue());

                            akademisyenService.yukseklisansguncelle(seciliyukseklisans);

                            for (int i = 0; i < 2; i++) {
                                Iterator<Window> window = getUI().getWindows().iterator();

                                getUI().removeWindow(window.next());
                                java.util.List<YuksekLisans> yeni = null;
                                yeni= akademisyenService.gettumyuksekler(a.getId());
                                akademisyenService.refreshGrid(yeni,YuksekLisans.class,yükseklisansbilgisi);

                                new MyNotification("güncelleme başarılı", MessageType.SUCCESS);


                            }
                        }



                    /*
                    yuksekLisans=akademisyenService.yukseklisansGetir(a.getId());
                    yuksekLisans.YuksekLisansuniv=declarativeYuksekLisansGuncelle.guncelyukseklisansuniv.getValue().toString();
                    yuksekLisans.YuksekLisansbolum=declarativeYuksekLisansGuncelle.guncelyukseklisansbolum.getValue().toString();
                    yuksekLisans.YuksekLisansfakulte =declarativeYuksekLisansGuncelle.guncelyukseklisansfakulte.getValue().toString();
                    yuksekLisans.YuksekLisansbaslangic=simpleDateFormat.format(declarativeYuksekLisansGuncelle.guncelyukseklisansbaslangic.getValue());
                    yuksekLisans.YuksekLisansbitis=simpleDateFormat.format(declarativeYuksekLisansGuncelle.guncelyukseklisansbitis.getValue());
                    akademisyenService.yukseklisansguncelle(yuksekLisans);
                    */

                        //a.getYukseklisans().YuksekLisansuniv=declarativeYuksekLisansGuncelle.guncelyukseklisansuniv.getValue().toString();
                        //akademisyenService.yukseklisansguncelle(a.getYukseklisans());
                    }
                });
            }
            else {
                new MyNotification("güncellemek istediğiniz yüksek lisansı seçin",MessageType.FAIL);
            }


        });
        guncelledoktora.addClickListener(clickEvent -> {


            DeclarativeDoktoraGuncelle declarativeDoktoraGuncelle= new DeclarativeDoktoraGuncelle();

            Doktora secilidoktora = (Doktora) doktorabilgisi.getSelectedRow();

            if(secilidoktora!=null) {


                declarativeDoktoraGuncelle.gunceldoktorauniv.setValue(akademisyenService.tekdoktoragetir(secilidoktora).getDoktorauniv().toString());
                declarativeDoktoraGuncelle.gunceldoktorafakulte.setValue(akademisyenService.tekdoktoragetir(secilidoktora).getDoktorafakulte().toString());
                declarativeDoktoraGuncelle.gunceldoktorabolum.setValue(akademisyenService.tekdoktoragetir(secilidoktora).getDoktorabolum().toString());

                declarativeDoktoraGuncelle.devam.setValue(akademisyenService.tekdoktoragetir(secilidoktora).getDevam());
                declarativeDoktoraGuncelle.gunceldoktorabaslangic.setDateFormat("dd-MM-yyyy");
                Date date = null;
                try {
                    date = simpleDateFormat.parse(akademisyenService.tekdoktoragetir(secilidoktora).getDoktorabaslangic());
                    declarativeDoktoraGuncelle.gunceldoktorabaslangic.setValue(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                declarativeDoktoraGuncelle.gunceldoktorabitis.setDateFormat("dd-MM-yyyy");


                try {
                    date = simpleDateFormat.parse(akademisyenService.tekdoktoragetir(secilidoktora).getDoktorabitis());
                    declarativeDoktoraGuncelle.gunceldoktorabitis.setValue(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                final Window window = new Window("guncelleme ekranı");
                window.setModal(true);
                window.setWidth(400.0f, Unit.PIXELS);
                window.setHeight(600.0f, Unit.PIXELS);
                window.setDraggable(false);
                window.setResizable(false);
                window.setContent(declarativeDoktoraGuncelle);

                UI.getCurrent().addWindow(window);
                declarativeDoktoraGuncelle.btndoktoraguncelle.addClickListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {

                        Doktora doktora = new Doktora();

                        if (declarativeDoktoraGuncelle.gunceldoktorauniv.getValue() == null) {
                            declarativeDoktoraGuncelle.gunceldoktorauniv.setRequired(true);
                        } else if (declarativeDoktoraGuncelle.gunceldoktorafakulte.getValue() == null) {
                            declarativeDoktoraGuncelle.gunceldoktorafakulte.setRequired(true);
                        } else if (declarativeDoktoraGuncelle.gunceldoktorabolum.getValue() == null) {
                            declarativeDoktoraGuncelle.gunceldoktorabolum.setRequired(true);
                        } else if (declarativeDoktoraGuncelle.gunceldoktorabaslangic.getValue() == null) {
                            declarativeDoktoraGuncelle.gunceldoktorabaslangic.setRequired(true);
                        } else if (declarativeDoktoraGuncelle.gunceldoktorabitis.getValue() == null && declarativeDoktoraGuncelle.devam.getValue() == false) {
                            declarativeDoktoraGuncelle.gunceldoktorabitis.setRequired(true);
                        } else {


                            secilidoktora.devam = declarativeDoktoraGuncelle.devam.getValue();
                            if (declarativeDoktoraGuncelle.devam.getValue() == true) {
                                secilidoktora.setDoktorabitis("devam ediyor");
                            } else {
                                secilidoktora.doktorabitis = simpleDateFormat.format(declarativeDoktoraGuncelle.gunceldoktorabitis.getValue());
                            }
                            secilidoktora.doktorauniv = declarativeDoktoraGuncelle.gunceldoktorauniv.getValue().toString();
                            secilidoktora.doktorabolum = declarativeDoktoraGuncelle.gunceldoktorabolum.getValue().toString();
                            secilidoktora.doktorafakulte = declarativeDoktoraGuncelle.gunceldoktorafakulte.getValue().toString();
                            secilidoktora.doktorabaslangic = simpleDateFormat.format(declarativeDoktoraGuncelle.gunceldoktorabaslangic.getValue());

                            akademisyenService.doktoraguncelle(secilidoktora);

                            for (int i = 0; i < 2; i++) {
                                Iterator<Window> window = getUI().getWindows().iterator();

                                getUI().removeWindow(window.next());
                                java.util.List<Doktora> yeni = null;
                                yeni= akademisyenService.gettumdoktoralar(a.getId());
                                akademisyenService.refreshGrid(yeni,Doktora.class,doktorabilgisi);

                                new MyNotification("güncelleme başarılı", MessageType.SUCCESS);


                            }
                        }




                    /*
                    doktora=akademisyenService.doktoraGetir(a.getId());
                    doktora.doktorauniv=declarativeDoktoraGuncelle.gunceldoktorauniv.getValue().toString();
                    doktora.doktorabolum=declarativeDoktoraGuncelle.gunceldoktorabolum.getValue().toString();
                    doktora.doktorafakulte =declarativeDoktoraGuncelle.gunceldoktorafakulte.getValue().toString();
                    doktora.doktorabaslangic=simpleDateFormat.format(declarativeDoktoraGuncelle.gunceldoktorabaslangic.getValue());
                    doktora.doktorabitis=simpleDateFormat.format(declarativeDoktoraGuncelle.gunceldoktorabitis.getValue());
                    akademisyenService.doktoraguncelle(doktora);
                    */

                        // a.getDoktora().doktorauniv=declarativeDoktoraGuncelle.gunceldoktorauniv.getValue().toString();
                        //akademisyenService.doktoraguncelle(a.getDoktora());
                    }
                });

            }
            else {
                new MyNotification("güncellemek istediğiniz doktorayı seçin",MessageType.FAIL);
            }

        });



        sillise.addClickListener( clickEvent -> {

            Collection<Object> secililise = lisebilgisi.getSelectedRows();
            if(secililise.size()!=0) {


                List<Lise> lises = new ArrayList<>();

                for (Object o : secililise) {
                    lises.add((Lise) o);
                }


                akademisyenService.lisesil(lises);
                java.util.List<Lise> yeni = null;
                yeni= akademisyenService.gettumliseler(a.getId());
                akademisyenService.refreshGrid(yeni,Lise.class,lisebilgisi);


                new MyNotification("silme başarılı", MessageType.SUCCESS);

            }
            else {
                new MyNotification("silmek istediğiniz liseyi seçin", MessageType.FAIL);
            }


        });

        sillisans.addClickListener( clickEvent -> {

            Collection<Object> secililisans = lisansbilgisi.getSelectedRows();
            if(secililisans.size()!=0 ) {


                List<Lisans> lisanss = new ArrayList<>();

                for (Object o : secililisans) {
                    lisanss.add((Lisans) o);
                }
                akademisyenService.lisanssil(lisanss);
                java.util.List<Lisans> yeni = null;
                yeni= akademisyenService.gettumlisanslar(a.getId());
                akademisyenService.refreshGrid(yeni,Lisans.class,lisansbilgisi);


                new MyNotification("silme başarılı", MessageType.SUCCESS);

            }
            else {
                new  MyNotification("silmek istediğiniz lisansı seçin",MessageType.FAIL);
            }


        });
        silyuksek.addClickListener( clickEvent -> {

            Collection<Object> seciliyuksek = yükseklisansbilgisi.getSelectedRows();
            if(seciliyuksek.size()!=0) {


                List<YuksekLisans> yuksekLisanss = new ArrayList<>();

                for (Object o : seciliyuksek) {
                    yuksekLisanss.add((YuksekLisans) o);
                }
                akademisyenService.yukseksil(yuksekLisanss);
                java.util.List<YuksekLisans> yeni = null;
                yeni= akademisyenService.gettumyuksekler(a.getId());
                akademisyenService.refreshGrid(yeni,YuksekLisans.class,yükseklisansbilgisi);


                new MyNotification("silme başarılı", MessageType.SUCCESS);


            }
            else {
                new MyNotification("silmek istediğiniz yüksek lisansı seçin",MessageType.FAIL);
            }


        });

        sildoktora.addClickListener( clickEvent -> {

            Collection<Object> secilidoktora =doktorabilgisi.getSelectedRows();
            if(secilidoktora.size()!=0) {


                List<Doktora> doktoras = new ArrayList<>();

                for (Object o : secilidoktora) {
                    doktoras.add((Doktora) o);
                }
                akademisyenService.doktorasil(doktoras);

                java.util.List<Doktora> yeni = null;
                yeni= akademisyenService.gettumdoktoralar(a.getId());
                akademisyenService.refreshGrid(yeni,Doktora.class,doktorabilgisi);

                new MyNotification("silme başarılı", MessageType.SUCCESS);
            }
            else {
                new MyNotification("silmek istediğiniz doktorayı seçin",MessageType.FAIL);
            }

        });

    }


}
