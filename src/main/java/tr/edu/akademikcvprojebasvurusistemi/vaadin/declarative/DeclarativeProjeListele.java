package tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.StreamResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.*;
import com.vaadin.ui.declarative.Design;
import com.vaadin.ui.renderers.ButtonRenderer;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.types.date.DateRangeSQLEqualClause;
import net.sf.jasperreports.view.JasperViewer;
import org.neo4j.cypher.internal.frontend.v2_3.ast.functions.Str;
import org.springframework.beans.factory.annotation.Autowired;
import tr.edu.akademikcvprojebasvurusistemi.domain.Akademisyen;
import tr.edu.akademikcvprojebasvurusistemi.domain.Proje;
import tr.edu.akademikcvprojebasvurusistemi.domain.Yayinlar;
import tr.edu.akademikcvprojebasvurusistemi.raporlama.ProjeModel;
import tr.edu.akademikcvprojebasvurusistemi.raporlama.ProjeModelFactory;
import tr.edu.akademikcvprojebasvurusistemi.service.AkademisyenService;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.extras.MyNotification;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.extras.enums.MessageType;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.views.Gorevliveakademisyen;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.views.ProjeBasvurusu;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by LENOVO on 19.1.2017.
 */
@DesignRoot
public class DeclarativeProjeListele extends Panel {


    public Button formguncelle, ozet, yeniform, formsil,eskibilgi;
    public Grid tumprojeler;
    Proje seciliproje;
    Akademisyen yenii,yeni2,yenii3,yenii4,yenii5,yenii6;
    DeclarativeProjeOzet declarativeProjeOzet = new DeclarativeProjeOzet();

    DeclarativeEskiProjeBilgisi declarativeEskiProjeBilgisi=new DeclarativeEskiProjeBilgisi();


    @Autowired
    AkademisyenService akademisyenService;

    public DeclarativeProjeListele(AkademisyenService _akademisyen) {

        Akademisyen a = (Akademisyen) VaadinSession.getCurrent().getSession().getAttribute("akademisyen");


        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        akademisyenService = _akademisyen;
        Design.read(this);
        tumprojeler.setSelectionMode(Grid.SelectionMode.SINGLE);
        //tumprojeler.setSelectionMode(Grid.SelectionMode.MULTI);
        tumprojeler.setStyleName("anasayfagrid");
        formguncelle.setIcon(FontAwesome.EDIT);

        formsil.setIcon(FontAwesome.CLOSE);
        tumprojeler.getSelectionModel().reset();
        tumprojeler.setSizeFull();


        java.util.List<Proje> projeler = null;
        projeler = akademisyenService.onayveyaimza(a.getId());

        BeanItemContainer<Proje> projeBeanItemContainer = new BeanItemContainer<>(Proje.class);
        tumprojeler.setContainerDataSource(projeBeanItemContainer);
        tumprojeler.getContainerDataSource().removeAllItems();
        projeBeanItemContainer.addAll(projeler);
        tumprojeler.setContainerDataSource(projeBeanItemContainer);
        tumprojeler.removeAllColumns();
        tumprojeler.addColumn("projeadi");
        tumprojeler.addColumn("onaydurumu");

        tumprojeler.getColumn("projeadi").setHeaderCaption("PROJE ADI");
        tumprojeler.getColumn("onaydurumu").setHeaderCaption("ONAY DURUMU ");






        formsil.addClickListener(clickEvent -> {

            Collection<Object> seciliproje = tumprojeler.getSelectedRows();
            if (seciliproje.size() != 0) {


                List<Proje> projes = new ArrayList<>();

                for (Object o : seciliproje) {
                    projes.add((Proje) o);
                }


                akademisyenService.projesil(projes);
                java.util.List<Proje> yeni = null;
                yeni = akademisyenService.onayveyaimza(a.getId());
                akademisyenService.refreshGrid(yeni, Proje.class, tumprojeler);


                new MyNotification("silme başarılı", MessageType.SUCCESS);

            } else {
                new MyNotification("silmek istediğiniz projeyi seçin", MessageType.FAIL);
            }


        });


        formguncelle.addClickListener(clickEvent -> {

            DeclarativeProjeBasvuruGuncelle declarativeProjeBasvuruGuncelle = new DeclarativeProjeBasvuruGuncelle();


            List<Gorevliveakademisyen> gorevliveakademisyenList=new ArrayList<Gorevliveakademisyen>();

            Proje seciliproje = (Proje) tumprojeler.getSelectedRow();


            if (seciliproje != null) {
                if(seciliproje.getBasvuruyapanadi().equals(a.getAdi())) {


                    if (seciliproje.getOnaydurumu().equals("onaylanmış")) {
                        new MyNotification("seçtiğiniz proje artık güncellenemez", MessageType.FAIL);


                    } else {
                        declarativeProjeBasvuruGuncelle.txtProjeAdi.setValue(seciliproje.getProjeadi().toString());
                        declarativeProjeBasvuruGuncelle.txtProjeOzeti.setValue(seciliproje.getOzeti().toString());
                        declarativeProjeBasvuruGuncelle.cbProjeTuru.setValue(seciliproje.getProjeTuru().toString());

                        declarativeProjeBasvuruGuncelle.cbAr_geNiteligi.setValue(seciliproje.getAr_geNiteligi().toString());

                        declarativeProjeBasvuruGuncelle.cbProjeKapsamaAlani.setValue(seciliproje.getProjeKapsamaAlani().toString());
                        declarativeProjeBasvuruGuncelle.txtSehir.setValue(seciliproje.getSehir().toString());

                        declarativeProjeBasvuruGuncelle.txtUlke.setValue(seciliproje.getUlke().toString());

                        declarativeProjeBasvuruGuncelle.txtButcesi.setValue(seciliproje.getProjeButcesi().toString());


                        declarativeProjeBasvuruGuncelle.txtKurumKatkiPayi.setValue(seciliproje.getKurumKatkiPayi().toString());

                        declarativeProjeBasvuruGuncelle.cbParaBirimi.setValue(seciliproje.getParaBirimi().toString());

                        declarativeProjeBasvuruGuncelle.cbsanayiIsbirligiDurumu.setValue(seciliproje.getSanayiIsbirligiDurumu().toString());

                        declarativeProjeBasvuruGuncelle.cbProjePatenti.setValue(seciliproje.getProjePatenti().toString());

                        String isim =seciliproje.getBasvuruyapanadi();
                        String basgorev=null;
                        for(int i=0;i<2;i++){
                            String iliski= akademisyenService.iliskigetir(akademisyenService.adagoreakademisyengetir(isim).getId(),seciliproje.getId()).get(i);

                            if(iliski.equals("HAS_EKIP")){
                                System.out.println("birşey yapma");
                            }
                            else {
                                basgorev=iliski;
                            }
                        }


                        if(basgorev.equals("YURUTUCU")){
                            declarativeProjeBasvuruGuncelle.txtProjedeAlinanGorev.setValue("yürütücü");

                        }
                        else if(basgorev.equals("KOORDINATOR")){
                            declarativeProjeBasvuruGuncelle.txtProjedeAlinanGorev.setValue("koordinatör");

                        }
                        else if(basgorev.equals("BURSIYER")){
                            declarativeProjeBasvuruGuncelle.txtProjedeAlinanGorev.setValue("bursiyer");

                        }
                        else if(basgorev.equals("ARASTIRMACI")){
                            declarativeProjeBasvuruGuncelle.txtProjedeAlinanGorev.setValue("araştırmacı");

                        }


                        declarativeProjeBasvuruGuncelle.txtBaslangicTarihi.setDateFormat("dd-MM-yyyy");
                        Date date = null;
                        try {
                            date = simpleDateFormat.parse(seciliproje.getBaslangic());
                            declarativeProjeBasvuruGuncelle.txtBaslangicTarihi.setValue(date);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        declarativeProjeBasvuruGuncelle.txtBitisTarihi.setDateFormat("dd-MM-yyyy");
                        try {
                            date = simpleDateFormat.parse(seciliproje.getBitis());
                            declarativeProjeBasvuruGuncelle.txtBitisTarihi.setValue(date);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        if (seciliproje.getProjePatenti().toString().equals("var")) {

                            declarativeProjeBasvuruGuncelle.pnlPatent.setVisible(true);
                            declarativeProjeBasvuruGuncelle.txtPatentAdi.setValue(seciliproje.getPatentAdi().toString());
                            declarativeProjeBasvuruGuncelle.txtPatentKodu.setValue(seciliproje.getPatentKodu().toString());

                            declarativeProjeBasvuruGuncelle.cbPatentKapsami.setValue(seciliproje.getPatentKapsami().toString());

                            declarativeProjeBasvuruGuncelle.txtPatentYili.setDateFormat("dd-MM-yyyy");
                            try {
                                date = simpleDateFormat.parse(seciliproje.getPatentYili());
                                declarativeProjeBasvuruGuncelle.txtPatentYili.setValue(date);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }
                        if (seciliproje.getSanayiIsbirligiDurumu().toString().equals("var")) {

                            declarativeProjeBasvuruGuncelle.pnlSanayi.setVisible(true);
                            declarativeProjeBasvuruGuncelle.txtFirmaAdi.setValue(seciliproje.getFirmaAdi().toString());
                            declarativeProjeBasvuruGuncelle.cbFirmaParaBirimi.setValue(seciliproje.getFirmaParaBirimi().toString());

                            declarativeProjeBasvuruGuncelle.txtSagladigiFon.setValue(seciliproje.getSagladigiFon().toString());


                        }

                        List <Akademisyen> ekip= akademisyenService.ekibigetir(seciliproje);
                        if(ekip.size()>1)
                        {
                            declarativeProjeBasvuruGuncelle.pnlekip.setVisible(true);
                            declarativeProjeBasvuruGuncelle.projeekibi.setValue("var");


                            List<String> iliskiler=new ArrayList<String>();

                            for (int i=0;i<ekip.size();i++) {
                                Gorevliveakademisyen gorevliveakademisyen=new Gorevliveakademisyen();
                                gorevliveakademisyen.setAkademisyenadi(ekip.get(i).getAdi().toString());

                                iliskiler=akademisyenService.iliskigetir(ekip.get(i).getId(),seciliproje.getId());
                                for(int j=0;j<iliskiler.size();j++){
                                    if(iliskiler.get(j).equals("HAS_EKIP")){

                                    }
                                    else{
                                        if (iliskiler.get(j).equals("ARASTIRMACI")) {
                                            gorevliveakademisyen.setGorev("araştırmacı");
                                        } else {

                                        }
                                        if (iliskiler.get(j).equals("YURUTUCU")) {
                                            gorevliveakademisyen.setGorev("yürütücü");
                                        } else {
                                            //
                                        }

                                        if (iliskiler.get(j).equals("KOORDINATOR")) {
                                            gorevliveakademisyen.setGorev("koordinatör");
                                        } else {

                                        }
                                        if (iliskiler.get(j).equals("BURSIYER")) {
                                            gorevliveakademisyen.setGorev("bursiyer");
                                        } else {
                                            System.out.println("birşey yapma");
                                        }



                                    }

                                }
                                if(!gorevliveakademisyen.getAkademisyenadi().toString().equals(a.getAdi().toString())){
//

                                    gorevliveakademisyenList.add(gorevliveakademisyen);

                                }


                            }

                            BeanItemContainer<Gorevliveakademisyen> gorevliveakademisyenBeanItemContainer = new BeanItemContainer<>(Gorevliveakademisyen.class);
                            declarativeProjeBasvuruGuncelle.grdgorevliakademisyen.setContainerDataSource(gorevliveakademisyenBeanItemContainer);
                            declarativeProjeBasvuruGuncelle.grdgorevliakademisyen.getContainerDataSource().removeAllItems();
                            gorevliveakademisyenBeanItemContainer.addAll(gorevliveakademisyenList);
                            declarativeProjeBasvuruGuncelle.grdgorevliakademisyen.setContainerDataSource(gorevliveakademisyenBeanItemContainer);
                            declarativeProjeBasvuruGuncelle.grdgorevliakademisyen.removeAllColumns();
                            declarativeProjeBasvuruGuncelle.grdgorevliakademisyen.addColumn("akademisyenadi");
                            declarativeProjeBasvuruGuncelle.grdgorevliakademisyen.addColumn("gorev");

                            declarativeProjeBasvuruGuncelle.grdgorevliakademisyen.getColumn("akademisyenadi").setHeaderCaption("AKADEMİSYEN");
                            declarativeProjeBasvuruGuncelle.grdgorevliakademisyen.getColumn("gorev").setHeaderCaption("GÖREVİ ");


                        }
                        else if(ekip.size()==1){
                            declarativeProjeBasvuruGuncelle.pnlekip.setVisible(false);
                            declarativeProjeBasvuruGuncelle.projeekibi.setValue("yok");


                        }
                        declarativeProjeBasvuruGuncelle.btnekle.addClickListener(clickEvent1 -> {

                            Akademisyen akademisyen = akademisyenService.akademisyenVarMi(declarativeProjeBasvuruGuncelle.txtTcgorevli.getValue());
                            String gorev = declarativeProjeBasvuruGuncelle.cbgorev.getValue().toString();
                            String adi=akademisyen.getAdi();

                            if (akademisyen != null) {
                                if (!gorev.equals("") || gorev != null) {

                                    gorevliveakademisyenList.add(new Gorevliveakademisyen(adi,gorev));
                                    java.util.List<Gorevliveakademisyen> yeni = null;
                                    yeni = gorevliveakademisyenList;
                                    akademisyenService.refreshGrid(yeni, Gorevliveakademisyen.class, declarativeProjeBasvuruGuncelle.grdgorevliakademisyen);


                                }

                            }
                            gorevliveakademisyenList.size();



                        });
                        declarativeProjeBasvuruGuncelle.btncikar.addClickListener(clickEvent1 -> {

                            Gorevliveakademisyen cikarilan = (Gorevliveakademisyen) declarativeProjeBasvuruGuncelle.grdgorevliakademisyen.getSelectedRow();

                            gorevliveakademisyenList.remove(cikarilan);
                            java.util.List<Gorevliveakademisyen> yeni = null;
                            yeni = gorevliveakademisyenList;
                            akademisyenService.refreshGrid(yeni, Gorevliveakademisyen.class, declarativeProjeBasvuruGuncelle.grdgorevliakademisyen);

                            gorevliveakademisyenList.size();
                        });




                        final Window window = new Window();
                        window.setModal(true);
                        //window.setWidth(680.0f, Unit.PIXELS);
                        window.setWidth(740.0f,Unit.PIXELS);
                        window.setHeight(700.0f, Unit.PIXELS);
                        window.setDraggable(false);
                        window.setResizable(false);

                        window.setContent(declarativeProjeBasvuruGuncelle);

                        UI.getCurrent().addWindow(window);

                        declarativeProjeBasvuruGuncelle.cbProjePatenti.addValueChangeListener(new Property.ValueChangeListener() {
                            @Override
                            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                                if (declarativeProjeBasvuruGuncelle.cbProjePatenti.getValue().toString().equals("var")) {
                                    declarativeProjeBasvuruGuncelle.pnlPatent.setVisible(true);
                                } else if (declarativeProjeBasvuruGuncelle.cbProjePatenti.getValue().toString().equals("yok")) {
                                    declarativeProjeBasvuruGuncelle.pnlPatent.setVisible(false);
                                }
                            }
                        });
                        declarativeProjeBasvuruGuncelle.cbsanayiIsbirligiDurumu.addValueChangeListener(new Property.ValueChangeListener() {
                            @Override
                            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                                if (declarativeProjeBasvuruGuncelle.cbsanayiIsbirligiDurumu.getValue().toString().equals("var")) {
                                    declarativeProjeBasvuruGuncelle.pnlSanayi.setVisible(true);
                                } else if (declarativeProjeBasvuruGuncelle.cbsanayiIsbirligiDurumu.getValue().toString().equals("yok")) {
                                    declarativeProjeBasvuruGuncelle.pnlSanayi.setVisible(false);

                                }
                            }
                        });
                        declarativeProjeBasvuruGuncelle.projeekibi.addValueChangeListener(new Property.ValueChangeListener() {
                            @Override
                            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                                if (declarativeProjeBasvuruGuncelle.projeekibi.getValue().toString().equals("var")) {
                                    declarativeProjeBasvuruGuncelle.pnlekip.setVisible(true);
                                } else if (declarativeProjeBasvuruGuncelle.projeekibi.getValue().toString().equals("yok")) {
                                    declarativeProjeBasvuruGuncelle.pnlekip.setVisible(false);

                                }
                            }
                        });

                    }
                }
                else{
                    new MyNotification("seçtiğiniz projeyi güncelleme yetkiniz yoktur",MessageType.FAIL);
                }
                declarativeProjeBasvuruGuncelle.gbtnKaydet.addClickListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {


                        seciliproje.projeadi = declarativeProjeBasvuruGuncelle.txtProjeAdi.getValue().toString();
                        seciliproje.baslangic = simpleDateFormat.format(declarativeProjeBasvuruGuncelle.txtBaslangicTarihi.getValue());
                        seciliproje.bitis = simpleDateFormat.format(declarativeProjeBasvuruGuncelle.txtBitisTarihi.getValue());
                        seciliproje.projeTuru = declarativeProjeBasvuruGuncelle.cbProjeTuru.getValue().toString();
                        seciliproje.projeButcesi = declarativeProjeBasvuruGuncelle.txtButcesi.getValue().toString();
                        seciliproje.KurumKatkiPayi = declarativeProjeBasvuruGuncelle.txtKurumKatkiPayi.getValue().toString();
                        seciliproje.sehir = declarativeProjeBasvuruGuncelle.txtSehir.getValue().toString();
                        seciliproje.ulke = declarativeProjeBasvuruGuncelle.txtUlke.getValue().toString();
                        seciliproje.ozeti = declarativeProjeBasvuruGuncelle.txtProjeOzeti.getValue().toString();
                        seciliproje.paraBirimi = declarativeProjeBasvuruGuncelle.cbParaBirimi.getValue().toString();

                        seciliproje.ar_geNiteligi = declarativeProjeBasvuruGuncelle.cbAr_geNiteligi.getValue().toString();
                        seciliproje.projeKapsamaAlani = declarativeProjeBasvuruGuncelle.cbProjeKapsamaAlani.getValue().toString();
                        seciliproje.ProjePatenti = declarativeProjeBasvuruGuncelle.cbProjePatenti.getValue().toString();
                        seciliproje.sanayiIsbirligiDurumu = declarativeProjeBasvuruGuncelle.cbsanayiIsbirligiDurumu.getValue().toString();

                        if (declarativeProjeBasvuruGuncelle.cbProjePatenti.getValue().toString().equals("var")) {
                            seciliproje.PatentAdi = declarativeProjeBasvuruGuncelle.txtPatentAdi.getValue().toString();
                            seciliproje.PatentKodu = declarativeProjeBasvuruGuncelle.txtPatentKodu.getValue().toString();
                            seciliproje.PatentYili = simpleDateFormat.format(declarativeProjeBasvuruGuncelle.txtPatentYili.getValue()).toString();
                            seciliproje.PatentKapsami = declarativeProjeBasvuruGuncelle.cbPatentKapsami.getValue().toString();

                        } else if (declarativeProjeBasvuruGuncelle.cbProjePatenti.getValue().toString().equals("yok")) {
                            seciliproje.PatentAdi = "";
                            seciliproje.PatentKodu = "";
                            seciliproje.PatentYili = "";
                            seciliproje.PatentKapsami = "";

                        }
                        if (declarativeProjeBasvuruGuncelle.cbsanayiIsbirligiDurumu.getValue().toString().equals("var")) {
                            seciliproje.firmaAdi = declarativeProjeBasvuruGuncelle.txtFirmaAdi.getValue().toString();
                            seciliproje.firmaParaBirimi = declarativeProjeBasvuruGuncelle.cbFirmaParaBirimi.getValue().toString();
                            seciliproje.sagladigiFon = declarativeProjeBasvuruGuncelle.txtSagladigiFon.getValue().toString();

                        } else if (declarativeProjeBasvuruGuncelle.cbsanayiIsbirligiDurumu.getValue().toString().equals("yok")) {
                            seciliproje.firmaAdi = "";
                            seciliproje.firmaParaBirimi = "";
                            seciliproje.sagladigiFon = "";

                        }
                        String basvurangore = declarativeProjeBasvuruGuncelle.txtProjedeAlinanGorev.getValue().toString();
                        akademisyenService.iliskisil5(a.getId(),seciliproje.getId());
                        akademisyenService.iliskisil4(a.getId(),seciliproje.getId());
                        akademisyenService.iliskisil3(a.getId(),seciliproje.getId());
                        akademisyenService.iliskisil6(a.getId(),seciliproje.getId());

                        if (basvurangore.equals("araştırmacı")) {
                            akademisyenService.iliskiyarat1(a.getId(), seciliproje.getId());
                        } else {

                        }
                        if (basvurangore.equals("yürütücü")) {
                            akademisyenService.iliskiyarat5(a.getId(), seciliproje.getId());
                        } else {
                            //
                        }

                        if (basvurangore.equals("koordinatör")) {
                            akademisyenService.iliskiyarat3(a.getId(), seciliproje.getId());
                        } else {

                        }
                        if (basvurangore.equals("bursiyer")) {
                            akademisyenService.iliskiyarat2(a.getId(), seciliproje.getId());
                        } else {
                            System.out.println("birşey yapma");
                        }

                        if(declarativeProjeBasvuruGuncelle.projeekibi.getValue().equals("var")){

                            List<Akademisyen> liste =akademisyenService.ekibigetir(seciliproje);
                            for(int i=0;i<liste.size();i++){
                                if(!liste.get(i).getAdi().toString().equals(a.getAdi())){
                                    akademisyenService.iliskisil5(liste.get(i).getId(),seciliproje.getId());
                                    akademisyenService.iliskisil4(liste.get(i).getId(),seciliproje.getId());
                                    akademisyenService.iliskisil3(liste.get(i).getId(),seciliproje.getId());
                                    akademisyenService.iliskisil6(liste.get(i).getId(),seciliproje.getId());
                                    akademisyenService.iliskisil(liste.get(i).getId(),seciliproje.getId());
                                    akademisyenService.iliskisil2(liste.get(i).getId(),seciliproje.getId());
                                }
                            }
                            akademisyenService.ekibigetir(seciliproje);
                            gorevliveakademisyenList.size();


                            for(int i=0 ;i<gorevliveakademisyenList.size();i++){

                                String ad=gorevliveakademisyenList.get(i).getAkademisyenadi();
                                String gorev= gorevliveakademisyenList.get(i).getGorev();
                                Akademisyen b= akademisyenService.adagoreakademisyengetir(ad);


                            /*
                            for (int k=seciliproje.getEkiptekiler().size()-1;k>=0;k--){
                                seciliproje.getEkiptekiler().remove(k);
                            }*/

                                akademisyenService.iliskiyarat4(b.getId(),seciliproje.getId());
                                akademisyenService.ekibigetir(seciliproje);
                                akademisyenService.iliskiyaratproje(b.getId(),seciliproje.getId());


                                seciliproje.getEkiptekiler();


                                if (gorev.equals("araştırmacı")) {
                                    akademisyenService.iliskiyarat1(b.getId(), seciliproje.getId());
                                } else {

                                }
                                if (gorev.equals("yürütücü")) {
                                    akademisyenService.iliskiyarat5(b.getId(), seciliproje.getId());
                                } else {
                                    //
                                }

                                if (gorev.equals("koordinatör")) {
                                    akademisyenService.iliskiyarat3(b.getId(), seciliproje.getId());
                                } else {

                                }
                                if (gorev.equals("bursiyer")) {
                                    akademisyenService.iliskiyarat2(b.getId(), seciliproje.getId());
                                } else {
                                    System.out.println("birşey yapma");
                                }

                            }



                        }










                        akademisyenService.projeguncelle(seciliproje);
                        for (int i = 0; i < 1; i++) {
                            Iterator<Window> window = getUI().getWindows().iterator();
                            getUI().removeWindow(window.next());
                            /*
                            java.util.List<Proje> yeni = new ArrayList<Proje>();
                            yeni = akademisyenService.gettumprojeler(a.getId());
                            akademisyenService.refreshGrid(yeni, Proje.class, tumprojeler);
                            */

                            new MyNotification("guncellme başarılı", MessageType.SUCCESS);


                        }
                    }

                });


            } else {
                new MyNotification("güncellemek istediğiniz projeyi seçin", MessageType.FAIL);
            }


        });





        ozet.addClickListener(clickEvent -> {
            seciliproje = (Proje) tumprojeler.getSelectedRow();

            if (seciliproje != null) {
                declarativeProjeOzet.txtProjeAdi.setValue(seciliproje.getProjeadi().toString());
                declarativeProjeOzet.txtProjeOzeti.setValue(seciliproje.getOzeti().toString());
                declarativeProjeOzet.cbProjeTuru.setValue(seciliproje.getProjeTuru().toString());
                declarativeProjeOzet.cbAr_geNiteligi.setValue(seciliproje.getAr_geNiteligi().toString());
                declarativeProjeOzet.cbProjeKapsamaAlani.setValue(seciliproje.getProjeKapsamaAlani().toString());
                declarativeProjeOzet.txtSehir.setValue(seciliproje.getSehir().toString());
                declarativeProjeOzet.txtUlke.setValue(seciliproje.getUlke().toString());
                declarativeProjeOzet.txtButcesi.setValue(seciliproje.getProjeButcesi().toString());
                declarativeProjeOzet.txtKurumKatkiPayi.setValue(seciliproje.getKurumKatkiPayi().toString());
                declarativeProjeOzet.cbParaBirimi.setValue(seciliproje.getParaBirimi().toString());
                declarativeProjeOzet.cbsanayiIsbirligiDurumu.setValue(seciliproje.getSanayiIsbirligiDurumu().toString());
                declarativeProjeOzet.cbProjePatenti.setValue(seciliproje.getProjePatenti().toString());
                declarativeProjeOzet.txtBaslangicTarihi.setDateFormat("dd-MM-yyyy");

                String isim =seciliproje.getBasvuruyapanadi();
                String basgorev=null;
                for(int i=0;i<2;i++){
                    String iliski= akademisyenService.iliskigetir(akademisyenService.adagoreakademisyengetir(isim).getId(),seciliproje.getId()).get(i);

                    if(iliski.equals("HAS_EKIP")){
                        System.out.println("birşey yapma");
                    }
                    else {
                        basgorev=iliski;
                    }
                }


                if(basgorev.equals("YURUTUCU")){
                    declarativeProjeOzet.txtProjedeAlinanGorev.setValue("yürütücü");

                }
                else if(basgorev.equals("KOORDINATOR")){
                    declarativeProjeOzet.txtProjedeAlinanGorev.setValue("koordinatör");

                }
                else if(basgorev.equals("BURSIYER")){
                    declarativeProjeOzet.txtProjedeAlinanGorev.setValue("bursiyer");

                }
                else if(basgorev.equals("ARASTIRMACI")){
                    declarativeProjeOzet.txtProjedeAlinanGorev.setValue("araştırmacı");

                }
                Date date = null;
                try {
                    date = simpleDateFormat.parse(seciliproje.getBaslangic());
                    declarativeProjeOzet.txtBaslangicTarihi.setValue(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                declarativeProjeOzet.txtBitisTarihi.setDateFormat("dd-MM-yyyy");
                try {
                    date = simpleDateFormat.parse(seciliproje.getBitis());
                    declarativeProjeOzet.txtBitisTarihi.setValue(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }



                final Window window = new Window();
                window.setModal(true);
                window.setWidth(680.0f,Unit.PIXELS);
                window.setHeight(700.0f, Unit.PIXELS);
                window.setDraggable(false);
                window.setResizable(false);
                window.setContent(declarativeProjeOzet);

                UI.getCurrent().addWindow(window);

            } else {
                new MyNotification("özetini istediğiniz projeyi seçin", MessageType.FAIL);
            }
        });

        yeniform.addClickListener(clickEvent -> {

            getUI().getPage().open("http:#!Projebasvuruformu", "_blank",false);//formu yenı sekmede acıyor
            //getUI().getNavigator().navigateTo(ProjeBasvurusu.VIEW_NAME);//formu aynı sekmede acıyor
        });
        eskibilgi.addClickListener(clickEvent -> {
            getUI().getPage().open("http:#!EskiProjeBilgileri", "_blank",false);
        });

        declarativeProjeOzet.btncikti.addClickListener(clickEvent -> {

            List<ProjeModel> projeModels = new ArrayList<ProjeModel>();
            ProjeModel projeModel = new ProjeModel();

            projeModel.setProjeadi(seciliproje.getProjeadi());
            projeModel.setOzeti(seciliproje.getOzeti());
            projeModel.setAr_geNiteligi(seciliproje.getAr_geNiteligi());
            projeModel.setBaslangic(seciliproje.getBaslangic());
            projeModel.setBitis(seciliproje.getBitis());
            projeModel.setKurumKatkiPayi(seciliproje.getKurumKatkiPayi());
            projeModel.setParaBirimi(seciliproje.getParaBirimi());
            projeModel.setProjeButcesi(seciliproje.getProjeButcesi());
            projeModel.setProjeTuru(seciliproje.getProjeTuru());
            projeModel.setProjeKapsamaAlani(seciliproje.getProjeKapsamaAlani());
            projeModel.setSanayiIsbirligiDurumu(seciliproje.getSanayiIsbirligiDurumu());
            projeModel.setSehir(seciliproje.getSehir());
            projeModel.setUlke(seciliproje.getUlke());
            projeModel.setProjePatenti(seciliproje.getProjePatenti());

            projeModel.setSoyadi(a.getSoyadi());
            projeModel.setTc(a.getTc());
            projeModel.setAnabilimdali(a.getAnabilimdali());
            projeModel.setBolum(a.getBolum());
            projeModel.setIstel(a.getIstel());
            projeModel.setKadrounvani(a.getKadrounvani());
            projeModel.setEmail(a.getEmail());
            projeModel.setCeptel(a.getCeptel());
            projeModel.setBirim(a.getBirim());
            projeModel.setAdi(a.getAdi());


            projeModels.add(projeModel);

            StreamResource streamResource = raporuBas(projeModels);

            if (streamResource != null) {
                getUI().getCurrent().getPage().open(streamResource, "_blank", false);
            } else {
            }

        });

    }

    private StreamResource raporuBas(List<ProjeModel> projeModels) {
        String dosyaAdi = projeModels.get(0).getAdi() + "_BasvuruForumu" + ".pdf";
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        try {
            JasperReport jasperReport = null;
            JasperPrint jasperPrint = null;
            JasperDesign jasperDesign = null;

            Map parameters = new HashMap();
            InputStream fis = getClass().getResourceAsStream("/projerapor.jrxml");
            jasperDesign = JRXmlLoader.load(fis);
            jasperReport = JasperCompileManager.compileReport(jasperDesign);
            jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRBeanCollectionDataSource(projeModels));
            JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
        } catch (JRException e) {
            e.printStackTrace();
        }

        return new StreamResource(
                () -> {
                    // Return a stream from the buffer.
                    return new ByteArrayInputStream(outputStream.toByteArray());
                }, dosyaAdi);


    }

}

