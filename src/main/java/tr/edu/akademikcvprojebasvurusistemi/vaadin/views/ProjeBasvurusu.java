/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.edu.akademikcvprojebasvurusistemi.vaadin.views;

import com.sun.xml.internal.bind.v2.runtime.reflect.Accessor;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import javax.annotation.PostConstruct;

import com.vaadin.ui.declarative.converters.DesignDateConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import tr.edu.akademikcvprojebasvurusistemi.domain.*;
import tr.edu.akademikcvprojebasvurusistemi.service.AkademisyenService;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative.DeclarativeLoginView;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative.DeclarativeProjeBilgileri;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative.DeclarativeProjeListele;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.extras.MyNotification;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.extras.enums.MessageType;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author ubuntu
 */

@Data
@UIScope
@SpringView(name = ProjeBasvurusu.VIEW_NAME)
public class ProjeBasvurusu extends Panel implements View {
    @Autowired
    AkademisyenService akademisyenService;

    List<Gorevliveakademisyen> gorevliveakademisyenList=new ArrayList<Gorevliveakademisyen>();


    public static final String VIEW_NAME = "Projebasvuruformu";
    Akademisyen a = (Akademisyen) VaadinSession.getCurrent().getSession().getAttribute("akademisyen");

    Proje proje = new Proje();
    DeclarativeProjeBilgileri declarativeprojebilgileri = new DeclarativeProjeBilgileri();

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

        if (a == null) {
            getUI().getNavigator().navigateTo(LoginView.VIEW_NAME);
        }
        DeclarativeLoginView declarativeLoginView=new DeclarativeLoginView();

        declarativeprojebilgileri.txtAdi.setValue(a.getAdi());
        declarativeprojebilgileri.txtSoyadi.setValue(a.getSoyadi());
        declarativeprojebilgileri.txtEposta.setValue(a.getEmail());
        declarativeprojebilgileri.txtAnaBilimDali.setValue("");
        declarativeprojebilgileri.txtBirim.setValue("");
        declarativeprojebilgileri.txtBolum.setValue(a.getBolum());
        declarativeprojebilgileri.txtIsTel.setValue(a.getIstel());
        declarativeprojebilgileri.txtKadroUnvani.setValue("");
        declarativeprojebilgileri.txtAnaBilimDali.setValue("");
        declarativeprojebilgileri.txtCepTel.setValue("");
        declarativeprojebilgileri.txtTC.setValue("");

    }

    @PostConstruct
    public void init() {

        setSizeFull();

        VerticalLayout verticalLayout = new VerticalLayout();


        declarativeprojebilgileri.cbProjePatenti.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                if (declarativeprojebilgileri.cbProjePatenti.getValue().toString().equals("var")) {
                    declarativeprojebilgileri.pnlPatent.setVisible(true);
                } else if (declarativeprojebilgileri.cbProjePatenti.getValue().toString().equals("yok")) {
                    declarativeprojebilgileri.pnlPatent.setVisible(false);
                }
            }
        });
        declarativeprojebilgileri.cbsanayiIsbirligiDurumu.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                if (declarativeprojebilgileri.cbsanayiIsbirligiDurumu.getValue().toString().equals("var")) {
                    declarativeprojebilgileri.pnlSanayi.setVisible(true);
                } else if (declarativeprojebilgileri.cbsanayiIsbirligiDurumu.getValue().toString().equals("yok")) {
                    declarativeprojebilgileri.pnlSanayi.setVisible(false);

                }
            }
        });

        declarativeprojebilgileri.projeekibi.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                if (declarativeprojebilgileri.projeekibi.getValue().toString().equals("var")) {
                    declarativeprojebilgileri.pnlekip.setVisible(true);

                } else if (declarativeprojebilgileri.projeekibi.getValue().toString().equals("yok")) {
                    declarativeprojebilgileri.pnlekip.setVisible(false);

                }
            }
        });


        declarativeprojebilgileri.btnKaydet.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

                proje.setProjeadi(declarativeprojebilgileri.txtProjeAdi.getValue().toString());
                proje.setOzeti(declarativeprojebilgileri.txtProjeOzeti.getValue());
                proje.setProjeTuru(declarativeprojebilgileri.cbProjeTuru.getValue().toString());

                proje.setAr_geNiteligi(declarativeprojebilgileri.cbAr_geNiteligi.getValue().toString());
                proje.setProjeKapsamaAlani(declarativeprojebilgileri.cbProjeKapsamaAlani.getValue().toString());
                proje.setSehir(declarativeprojebilgileri.txtSehir.getValue());
                proje.setUlke(declarativeprojebilgileri.txtUlke.getValue());
                proje.setBaslangic(simpleDateFormat.format(declarativeprojebilgileri.txtBaslangicTarihi.getValue()));
                proje.setBitis(simpleDateFormat.format(declarativeprojebilgileri.txtBitisTarihi.getValue()));
                proje.setProjeButcesi(declarativeprojebilgileri.txtButcesi.getValue());
                proje.setKurumKatkiPayi(declarativeprojebilgileri.txtKurumKatkiPayi.getValue());
                proje.setParaBirimi(declarativeprojebilgileri.cbParaBirimi.getValue().toString());
                proje.setProjePatenti(declarativeprojebilgileri.cbProjePatenti.getValue().toString());
                proje.setSanayiIsbirligiDurumu(declarativeprojebilgileri.cbsanayiIsbirligiDurumu.getValue().toString());
                proje.setOnaydurumu("imza bekliyor");
                Date date = new Date();
                proje.setSimdikiZaman(date.getTime());

                proje.setBasvuruyapanadi(a.adi);
                proje.setBasvuruyapansoyadi(a.soyadi);
                if (declarativeprojebilgileri.cbProjePatenti.getValue().toString().equals("var")) {
                    proje.setPatentAdi(declarativeprojebilgileri.txtPatentAdi.getValue().toString());
                    proje.setPatentKodu(declarativeprojebilgileri.txtPatentKodu.getValue().toString());
                    proje.setPatentYili(simpleDateFormat.format(declarativeprojebilgileri.txtPatentYili.getValue()).toString());
                    proje.setPatentKapsami(declarativeprojebilgileri.cbPatentKapsami.getValue().toString());

                }
                if (declarativeprojebilgileri.cbsanayiIsbirligiDurumu.getValue().toString().equals("var")) {
                    proje.setFirmaAdi(declarativeprojebilgileri.txtFirmaAdi.getValue().toString());
                    proje.setFirmaParaBirimi(declarativeprojebilgileri.cbFirmaParaBirimi.getValue().toString());
                    proje.setSagladigiFon(declarativeprojebilgileri.txtSagladigiFon.getValue().toString());

                }

                String basvurangore = declarativeprojebilgileri.txtProjedeAlinanGorev.getValue().toString();
                if (declarativeprojebilgileri.projeekibi.getValue().equals("yok")) {
                    a.getProjeler().add(proje);
                    proje.getEkiptekiler().add(a);
                    akademisyenService.akademisyenEkle(a);

                    if (basvurangore.equals("araştırmacı")) {
                        akademisyenService.iliskiyarat1(a.getId(), proje.getId());
                    } else {

                    }
                    if (basvurangore.equals("yürütücü")) {
                        akademisyenService.iliskiyarat5(a.getId(), proje.getId());
                    } else {
                        //
                    }

                    if (basvurangore.equals("koordinatör")) {
                        akademisyenService.iliskiyarat3(a.getId(), proje.getId());
                    } else {

                    }
                    if (basvurangore.equals("bursiyer")) {
                        akademisyenService.iliskiyarat2(a.getId(), proje.getId());
                    } else {
                        System.out.println("birşey yapma");
                    }


                } else if (declarativeprojebilgileri.projeekibi.getValue().equals("var")) {

                    a.getProjeler().add(proje);
                    proje.getEkiptekiler().add(a);
                    akademisyenService.akademisyenEkle(a);
                    if (basvurangore.equals("araştırmacı")) {
                        akademisyenService.iliskiyarat1(a.getId(), proje.getId());
                    } else {

                    }
                    if (basvurangore.equals("yürütücü")) {
                        akademisyenService.iliskiyarat5(a.getId(), proje.getId());
                    } else {
                        //
                    }

                    if (basvurangore.equals("koordinatör")) {
                        akademisyenService.iliskiyarat3(a.getId(), proje.getId());
                    } else {

                    }
                    if (basvurangore.equals("bursiyer")) {
                        akademisyenService.iliskiyarat2(a.getId(), proje.getId());
                    } else {
                        System.out.println("birşey yapma");
                    }

                    for(int i=0 ;i<gorevliveakademisyenList.size();i++){

                        String ad=gorevliveakademisyenList.get(i).getAkademisyenadi();
                        String gorev= gorevliveakademisyenList.get(i).getGorev();
                        Akademisyen b= akademisyenService.adagoreakademisyengetir(ad);

                        proje.getEkiptekiler().add(b);
                        b.getProjeler().add(proje);
                        akademisyenService.akademisyenEkle(b);



                        if (gorev.equals("araştırmacı")) {
                            akademisyenService.iliskiyarat1(b.getId(), proje.getId());
                        } else {

                        }
                        if (gorev.equals("yürütücü")) {
                            akademisyenService.iliskiyarat5(b.getId(), proje.getId());
                        } else {
                            //
                        }

                        if (gorev.equals("koordinatör")) {
                            akademisyenService.iliskiyarat3(b.getId(), proje.getId());
                        } else {

                        }
                        if (gorev.equals("bursiyer")) {
                            akademisyenService.iliskiyarat2(b.getId(), proje.getId());
                        } else {
                            System.out.println("birşey yapma");
                        }

                    }



                }


                new MyNotification("kaydetme başarılı", MessageType.SUCCESS);


            }

        });


        declarativeprojebilgileri.don.addClickListener(clickEvent -> {
            getUI().getNavigator().navigateTo(AnaSayfa.VIEW_NAME);
            DeclarativeProjeListele declarativeProjeListele = new DeclarativeProjeListele(akademisyenService);
            java.util.List<Proje> yeni = null;
            yeni = akademisyenService.gettumprojeler(a.getId());
            akademisyenService.refreshGrid(yeni, Proje.class, declarativeProjeListele.tumprojeler);

            getUI().getNavigator().navigateTo(AnaSayfa.VIEW_NAME);

        });
        declarativeprojebilgileri.don2.addClickListener(clickEvent -> {
            getUI().getNavigator().navigateTo(AnaSayfa.VIEW_NAME);
            DeclarativeProjeListele declarativeProjeListele = new DeclarativeProjeListele(akademisyenService);
            java.util.List<Proje> yeni = null;
            yeni = akademisyenService.gettumprojeler(a.getId());
            akademisyenService.refreshGrid(yeni, Proje.class, declarativeProjeListele.tumprojeler);

            getUI().getNavigator().navigateTo(AnaSayfa.VIEW_NAME);

        });






        BeanItemContainer<Gorevliveakademisyen> gorevliveakademisyenBeanItemContainer = new BeanItemContainer<>(Gorevliveakademisyen.class);
        declarativeprojebilgileri.grdgorevliakademisyen.setContainerDataSource(gorevliveakademisyenBeanItemContainer);
        declarativeprojebilgileri.grdgorevliakademisyen.getContainerDataSource().removeAllItems();
        gorevliveakademisyenBeanItemContainer.addAll(gorevliveakademisyenList);
        declarativeprojebilgileri.grdgorevliakademisyen.setContainerDataSource(gorevliveakademisyenBeanItemContainer);
        declarativeprojebilgileri.grdgorevliakademisyen.removeAllColumns();
        declarativeprojebilgileri.grdgorevliakademisyen.addColumn("akademisyenadi");
        declarativeprojebilgileri.grdgorevliakademisyen.addColumn("gorev");

        declarativeprojebilgileri.grdgorevliakademisyen.getColumn("akademisyenadi").setHeaderCaption("AKADEMİSYEN");
        declarativeprojebilgileri.grdgorevliakademisyen.getColumn("gorev").setHeaderCaption("GÖREVİ ");


        declarativeprojebilgileri.btnekle.addClickListener(clickEvent -> {

            String tchash = DigestUtils.md5Hex(declarativeprojebilgileri.txtTcgorevli.getValue().toString());


            Akademisyen akademisyen = akademisyenService.akademisyenVarMi(tchash);
            String gorev = declarativeprojebilgileri.cbgorev.getValue().toString();
            String adi=akademisyen.getAdi();

            if (akademisyen != null) {
                if (!gorev.equals("") || gorev != null) {

                    gorevliveakademisyenList.add(new Gorevliveakademisyen(adi,gorev));
                    java.util.List<Gorevliveakademisyen> yeni = null;
                    yeni = gorevliveakademisyenList;
                    akademisyenService.refreshGrid(yeni, Gorevliveakademisyen.class, declarativeprojebilgileri.grdgorevliakademisyen);





                }

            }



        });
        declarativeprojebilgileri.btncikar.addClickListener(clickEvent1 -> {

            Gorevliveakademisyen cikarilan = (Gorevliveakademisyen) declarativeprojebilgileri.grdgorevliakademisyen.getSelectedRow();

            gorevliveakademisyenList.remove(cikarilan);
            java.util.List<Gorevliveakademisyen> yeni = null;
            yeni = gorevliveakademisyenList;
            akademisyenService.refreshGrid(yeni, Gorevliveakademisyen.class, declarativeprojebilgileri.grdgorevliakademisyen);

            gorevliveakademisyenList.size();
        });



        verticalLayout.addComponent(declarativeprojebilgileri);
        verticalLayout.setComponentAlignment(declarativeprojebilgileri, Alignment.MIDDLE_CENTER);
        setContent(verticalLayout);


    }

}
