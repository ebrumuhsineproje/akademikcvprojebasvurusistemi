/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.edu.akademikcvprojebasvurusistemi.vaadin.views;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import org.neo4j.driver.v1.exceptions.ServiceUnavailableException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.template.Neo4jOperations;
import tr.edu.akademikcvprojebasvurusistemi.domain.Akademisyen;
import tr.edu.akademikcvprojebasvurusistemi.exceptions.AkademisyenZatenEkliException;
import tr.edu.akademikcvprojebasvurusistemi.service.AkademisyenService;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative.DeclarativeEgitimBilgileriGirisView;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative.DeclarativeKisiselBilgilerGirisView;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative.DeclarativeLoginView;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.extras.MyNotification;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.extras.enums.Cinsiyet;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.extras.enums.MessageType;

/**
 *
 * @author LENOVO
 */
@UIScope
@SpringView(name = KisiselBilgilerGirisView.VIEW_NAME)
public class KisiselBilgilerGirisView extends Panel implements View{
    public static final String VIEW_NAME="KisiselBilgilerGirisView";
    
    @Autowired
    Neo4jOperations neo4jOperations;
    @Autowired
    AkademisyenService akademisyenService;

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @PostConstruct
    public void init(){
        setSizeFull();
       
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setSizeFull();
        DeclarativeKisiselBilgilerGirisView declarativeKisiselBilgilerGirisView = new DeclarativeKisiselBilgilerGirisView();
         
        declarativeKisiselBilgilerGirisView.btnKisiKaydet.addClickListener(clickEvent -> {
            Akademisyen a=new Akademisyen();
            a.setAdi("ebru");
            a.setSifre("123");
            a.setSoyadi("ogdur");
            a.setTc("456");
            a.setEmail("ebruogdur");
            
           // a.setCinsiyet(Cinsiyet.KADIN);
            neo4jOperations.save(a);
            
            
            
        });

        verticalLayout.addComponent(declarativeKisiselBilgilerGirisView);
        verticalLayout.setComponentAlignment(declarativeKisiselBilgilerGirisView, Alignment.MIDDLE_CENTER);

        
        setContent(verticalLayout);
     
    }
  

    
}
