package tr.edu.akademikcvprojebasvurusistemi.vaadin.views;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import tr.edu.akademikcvprojebasvurusistemi.service.AkademisyenService;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative.DeclarativeErrorView;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative.DeclarativeIstatistik;

import javax.annotation.PostConstruct;

/**
 * Created by LENOVO on 16.5.2017.
 */

@UIScope
@SpringView(name = IstatistikView.VIEW_NAME)
public class IstatistikView extends Panel implements View {

    @Autowired
    AkademisyenService akademisyenService;
    public static final String VIEW_NAME="Istatistikler";

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {



    }
    @PostConstruct
    public void init(){


        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setSizeFull();
        DeclarativeIstatistik declarativeIstatistik = new DeclarativeIstatistik(akademisyenService);

        verticalLayout.addComponent(declarativeIstatistik);
        verticalLayout.setComponentAlignment(declarativeIstatistik, Alignment.MIDDLE_CENTER);
        setContent(verticalLayout);


        //Akademisyen a= (Akademisyen) VaadinSession.getCurrent().getSession().getAttribute("akademisyen");

    }
}
