package tr.edu.akademikcvprojebasvurusistemi.vaadin.views;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.neo4j.driver.v1.exceptions.ServiceUnavailableException;

import org.springframework.beans.factory.annotation.Autowired;
import tr.edu.akademikcvprojebasvurusistemi.domain.Akademisyen;
import tr.edu.akademikcvprojebasvurusistemi.domain.Gorevli;
import tr.edu.akademikcvprojebasvurusistemi.service.AkademisyenService;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative.DeclarativeLoginView;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.extras.MyNotification;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.extras.enums.MessageType;

import javax.annotation.PostConstruct;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;


@SuppressWarnings("unchecked")
@UIScope
@SpringView(name = LoginView.VIEW_NAME)
public class LoginView extends Panel implements View {

    public static final String VIEW_NAME = "";

    @Autowired
    AkademisyenService akademisyenService;




    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {


/*
        String FILENAME ="C:\\Users\\LENOVO\\Desktop\\den\\abcde.json";

        try {
            JSONArray jsonArray = new JSONArray(readJsonFile(FILENAME));
            for(int i =0; i<jsonArray.length(); i++){
                jsonArray.get(i);

                JSONObject jsonObject=new JSONObject();
                jsonObject= (JSONObject) jsonArray.get(i);
            //   System.out.println( jsonObject.get("Name"));

                Akademisyen yeni=new Akademisyen();
               yeni.setAdi(jsonObject.get("Name").toString());
               yeni.setSoyadi(jsonObject.get("Surname").toString());
               yeni.setTc(jsonObject.get("TCHASH").toString());

                int b= (int) (Math.random()* 10);
                String c = String.valueOf(b);

                yeni.setSifre(c);


               yeni.setEmail(jsonObject.get("Email").toString());
               yeni.setBolum(jsonObject.get("Department").toString());
               yeni.setIstel(jsonObject.get("Phone").toString());
                akademisyenService.akademisyenEkle(yeni);




            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/

    }

    @PostConstruct

    public void initializeView() {
        setSizeFull();
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setSizeFull();
        DeclarativeLoginView declarativeLoginView = new DeclarativeLoginView();

        declarativeLoginView.btnLogin.addClickListener(clickEvent -> {
            if (declarativeLoginView.txtTc.getValue() == "") declarativeLoginView.txtTc.setRequired(true);
            else if (declarativeLoginView.txtPass.getValue() == "") declarativeLoginView.txtPass.setRequired(true);

            else {

                try {

                    String tchash = DigestUtils.md5Hex(declarativeLoginView.txtTc.getValue().toString());


                    Akademisyen a = akademisyenService.akademisyengetir(tchash);
                    Gorevli g = akademisyenService.gorevligetir(declarativeLoginView.txtTc.getValue());

                    if (a != null) {
                        if (declarativeLoginView.txtPass.getValue().equals(a.sifre)) {

                            VaadinSession.getCurrent().getSession().setAttribute("akademisyen", a);
                            //getSession().setAttribute("akademisyen",a);
                            getUI().getNavigator().navigateTo(AnaSayfa.VIEW_NAME);
                        } else {
                            new MyNotification("Hatali Giriş", MessageType.FAIL);
                        }

                    }
                    if (g != null) {
                        if (declarativeLoginView.txtPass.getValue().equals(g.sifre)) {
                            VaadinSession.getCurrent().getSession().setAttribute("Gorevli", g);
                            getUI().getNavigator().navigateTo(GorevliView.VIEW_NAME);
                        } else {
                            new MyNotification("Hatali Giriş", MessageType.FAIL);
                        }
                    }
                } catch (ServiceUnavailableException e) {
                    new MyNotification("DB ile bağlantı kurulamadı.", MessageType.FAIL);
                }
            }

        });
/*
        declarativeLoginView.btnLogin.addClickListener(clickEvent -> {

            Akademisyen a = akademisyenService.akademisyengetir(declarativeLoginView.txtTc.getValue());
            Gorevli g= akademisyenService.gorevliGetir(declarativeLoginView.txtTc.getValue());

            KisiSorgulaPortService kisiSorgulaPortService = new KisiSorgulaPortService();
            KisiSorgulaPort kisiSorgulaPort = kisiSorgulaPortService.getKisiSorgulaPortSoap11();
            GetKisiRequest kisiRequest = new GetKisiRequest();
            kisiRequest.setKullaniciAdi(declarativeLoginView.txtTc.getValue().toString());


            String parolaMd5Li = DigestUtils.md5Hex(declarativeLoginView.txtPass.getValue().toString());
            kisiRequest.setParola(parolaMd5Li);


            GetKisiResponse kisiResponse = kisiSorgulaPort.getKisi(kisiRequest);


            if (a != null) {
                if (kisiResponse.isSonuc() == true) {
                    VaadinSession.getCurrent().getSession().setAttribute("akademisyen", a);
                    getUI().getNavigator().navigateTo(AnaSayfa.VIEW_NAME);
                } else {
                    new MyNotification("Hatali Giriş", MessageType.FAIL);
                }
            }

            if(g!=null){
                if(declarativeLoginView.txtPass.getValue().equals(g.sifre)){
                    VaadinSession.getCurrent().getSession().setAttribute("Gorevli",g);
                    getUI().getNavigator().navigateTo(GorevliView.VIEW_NAME);
                }
                else {
                    new MyNotification("Hatali Giriş",MessageType.FAIL);
                }
            }


        });*/


        /*
        declarativeLoginView.btnLogin.addClickListener(clickEvent -> {
            if(declarativeLoginView.txtEmail.getValue()=="") declarativeLoginView.txtEmail.setRequired(true);
            else if(declarativeLoginView.txtPass.getValue()=="") declarativeLoginView.txtPass.setRequired(true);

            else {

                try {


                   Akademisyen a = akademisyenService.akademisyenGetir(declarativeLoginView.txtEmail.getValue());
                    Gorevli g= akademisyenService.gorevliGetir(declarativeLoginView.txtEmail.getValue());

                    if (a != null) {
                        if (declarativeLoginView.txtPass.getValue().equals(a.sifre)){

                            VaadinSession.getCurrent().getSession().setAttribute("akademisyen", a);
                            //getSession().setAttribute("akademisyen",a);
                            getUI().getNavigator().navigateTo(AnaSayfa.VIEW_NAME);
                        }
                        else{
                            new MyNotification("Hatali Giriş",MessageType.FAIL);
                        }

                    }
                    else if(g!=null){
                        if(declarativeLoginView.txtPass.getValue().equals(g.sifre)){
                            VaadinSession.getCurrent().getSession().setAttribute("Gorevli",g);
                            getUI().getNavigator().navigateTo(GorevliView.VIEW_NAME);
                        }
                        else {
                            new MyNotification("Hatali Giriş",MessageType.FAIL);
                        }
                    }
                } catch (ServiceUnavailableException e) {
                    new MyNotification("DB ile bağlantı kurulamadı.", MessageType.FAIL);
                }
            }

        });*/

        verticalLayout.addComponent(declarativeLoginView);
        verticalLayout.setComponentAlignment(declarativeLoginView, Alignment.MIDDLE_CENTER);
        setContent(verticalLayout);
    }

    public static String readJsonFile(String filename) throws IOException {
        try (InputStream is = new FileInputStream(filename)) {
            return IOUtils.toString(is, StandardCharsets.UTF_8);
        }
    }


}