package tr.edu.akademikcvprojebasvurusistemi.vaadin.views;

import tr.edu.akademikcvprojebasvurusistemi.domain.Entity;

public class Gorevliveakademisyen  {
    public String akademisyenadi;
    public String gorev;

    public Gorevliveakademisyen(String akademisyenadi, String gorev) {
        this.akademisyenadi   = akademisyenadi;
        this.gorev = gorev;
    }



    public String getAkademisyenadi() {
        return akademisyenadi;
    }

    public void setAkademisyenadi(String akademisyenadi) {
        this.akademisyenadi = akademisyenadi;
    }



    public String getGorev() {
        return gorev;
    }

    public void setGorev(String gorev) {
        this.gorev = gorev;
    }


    public Gorevliveakademisyen() {

    }
}
