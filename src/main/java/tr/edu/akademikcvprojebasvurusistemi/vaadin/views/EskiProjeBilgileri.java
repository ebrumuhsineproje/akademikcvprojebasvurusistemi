package tr.edu.akademikcvprojebasvurusistemi.vaadin.views;

import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import org.apache.commons.codec.digest.DigestUtils;
import org.neo4j.cypher.internal.frontend.v2_3.ast.False;
import org.springframework.beans.factory.annotation.Autowired;
import tr.edu.akademikcvprojebasvurusistemi.domain.Akademisyen;
import tr.edu.akademikcvprojebasvurusistemi.domain.Proje;
import tr.edu.akademikcvprojebasvurusistemi.service.AkademisyenService;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative.DeclarativeEskiProjeBilgisi;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative.DeclarativeProjeBilgileri;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative.DeclarativeProjeListele;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.extras.MyNotification;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.extras.enums.MessageType;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ubuntu on 3/6/17.
 */
@UIScope
@SpringView(name = EskiProjeBilgileri.VIEW_NAME)
public class EskiProjeBilgileri extends Panel implements View {
    @Autowired
    AkademisyenService akademisyenService;

    List<Gorevliveakademisyen> gorevliveakademisyenList=new ArrayList<Gorevliveakademisyen>();



    public static final String VIEW_NAME = "EskiProjeBilgileri";
    Akademisyen a= (Akademisyen) VaadinSession.getCurrent().getSession().getAttribute("akademisyen");
    Proje proje=new Proje();

    DeclarativeEskiProjeBilgisi declarativeEskiProjeBilgisi = new DeclarativeEskiProjeBilgisi();

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        if (a == null) {
            getUI().getNavigator().navigateTo(LoginView.VIEW_NAME);
        }
        declarativeEskiProjeBilgisi.txtAdi.setValue(a.getAdi());
        declarativeEskiProjeBilgisi.txtSoyadi.setValue(a.getSoyadi());
        declarativeEskiProjeBilgisi.txtEposta.setValue(a.getEmail());
        declarativeEskiProjeBilgisi.txtAnaBilimDali.setValue(a.getAnabilimdali());
        declarativeEskiProjeBilgisi.txtBirim.setValue(a.getBirim());
        declarativeEskiProjeBilgisi.txtBolum.setValue(a.getBolum());
        declarativeEskiProjeBilgisi.txtIsTel.setValue(a.getIstel());
        declarativeEskiProjeBilgisi.txtKadroUnvani.setValue(a.getKadrounvani());
        declarativeEskiProjeBilgisi.txtAnaBilimDali.setValue(a.getAnabilimdali());
        declarativeEskiProjeBilgisi.txtCepTel.setValue(a.getCeptel());
        declarativeEskiProjeBilgisi.txtTC.setValue(a.getTc());
    }

    @PostConstruct
    public void init(){

        setSizeFull();
        //setSizeUndefined();
        VerticalLayout verticalLayout = new VerticalLayout();
        //verticalLayout.setSizeFull();

        declarativeEskiProjeBilgisi.cbProjePatenti.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                if (declarativeEskiProjeBilgisi.cbProjePatenti.getValue().toString().equals("var")) {
                    declarativeEskiProjeBilgisi.pnlPatent.setVisible(true);
                } else if (declarativeEskiProjeBilgisi.cbProjePatenti.getValue().toString().equals("yok")) {
                    declarativeEskiProjeBilgisi.pnlPatent.setVisible(false);
                }
            }
        });
        declarativeEskiProjeBilgisi.cbsanayiIsbirligiDurumu.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                if (declarativeEskiProjeBilgisi.cbsanayiIsbirligiDurumu.getValue().toString().equals("var")) {
                    declarativeEskiProjeBilgisi.pnlSanayi.setVisible(true);
                } else if (declarativeEskiProjeBilgisi.cbsanayiIsbirligiDurumu.getValue().toString().equals("yok")) {
                    declarativeEskiProjeBilgisi.pnlSanayi.setVisible(false);

                }
            }
        });

        declarativeEskiProjeBilgisi.imzabekliyor.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                if (declarativeEskiProjeBilgisi.imzabekliyor.getValue()==true) {
                    declarativeEskiProjeBilgisi.onaylanmis.setVisible(false);


                } else if (declarativeEskiProjeBilgisi.imzabekliyor.getValue()== false) {
                    declarativeEskiProjeBilgisi.onaylanmis.setVisible(true);


                }
            }
        });
        declarativeEskiProjeBilgisi.onaylanmis.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                if (declarativeEskiProjeBilgisi.onaylanmis.getValue()==true) {
                    declarativeEskiProjeBilgisi.imzabekliyor.setVisible(false);

                } else if (declarativeEskiProjeBilgisi.onaylanmis.getValue()== false) {
                    declarativeEskiProjeBilgisi.imzabekliyor.setVisible(true);

                }
            }
        });

        declarativeEskiProjeBilgisi.projeekibi.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                if (declarativeEskiProjeBilgisi.projeekibi.getValue().toString().equals("var")) {
                    declarativeEskiProjeBilgisi.pnlekip.setVisible(true);

                } else if (declarativeEskiProjeBilgisi.projeekibi.getValue().toString().equals("yok")) {
                    declarativeEskiProjeBilgisi.pnlekip.setVisible(false);

                }
            }
        });





        declarativeEskiProjeBilgisi.btnKaydet.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                proje.setProjeadi(declarativeEskiProjeBilgisi.txtProjeAdi.getValue().toString());
                proje.setOzeti(declarativeEskiProjeBilgisi.txtProjeOzeti.getValue());
                proje.setProjeTuru(declarativeEskiProjeBilgisi.cbProjeTuru.getValue().toString());
                proje.setAr_geNiteligi(declarativeEskiProjeBilgisi.cbAr_geNiteligi.getValue().toString());
                proje.setProjeKapsamaAlani(declarativeEskiProjeBilgisi.cbProjeKapsamaAlani.getValue().toString());
                proje.setSehir(declarativeEskiProjeBilgisi.txtSehir.getValue());
                proje.setUlke(declarativeEskiProjeBilgisi.txtUlke.getValue());
                proje.setBaslangic(simpleDateFormat.format(declarativeEskiProjeBilgisi.txtBaslangicTarihi.getValue()));
                proje.setBitis(simpleDateFormat.format(declarativeEskiProjeBilgisi.txtBitisTarihi.getValue()));
                proje.setProjeButcesi(declarativeEskiProjeBilgisi.txtButcesi.getValue());
                proje.setKurumKatkiPayi(declarativeEskiProjeBilgisi.txtKurumKatkiPayi.getValue());
                proje.setParaBirimi(declarativeEskiProjeBilgisi.cbParaBirimi.getValue().toString());
                proje.setProjePatenti(declarativeEskiProjeBilgisi.cbProjePatenti.getValue().toString());
                proje.setSanayiIsbirligiDurumu(declarativeEskiProjeBilgisi.cbsanayiIsbirligiDurumu.getValue().toString());
                Date date = new Date();
                proje.setSimdikiZaman(date.getTime());

                proje.setBasvuruyapanadi(a.adi);
                proje.setBasvuruyapansoyadi(a.soyadi);
                if(declarativeEskiProjeBilgisi.cbProjePatenti.getValue().toString().equals("var")){
                    proje.setPatentAdi(declarativeEskiProjeBilgisi.txtPatentAdi.getValue().toString());
                    proje.setPatentKodu(declarativeEskiProjeBilgisi.txtPatentKodu.getValue().toString());
                    proje.setPatentYili(simpleDateFormat.format(declarativeEskiProjeBilgisi.txtPatentYili.getValue()).toString());
                    proje.setPatentKapsami(declarativeEskiProjeBilgisi.cbPatentKapsami.getValue().toString());

                }
                if(declarativeEskiProjeBilgisi.imzabekliyor.getValue()==true){
                    proje.setOnaydurumu("imza bekliyor");

                }
                if(declarativeEskiProjeBilgisi.onaylanmis.getValue()==true){
                    proje.setOnaydurumu("onaylanmış");

                }

                if(declarativeEskiProjeBilgisi.cbsanayiIsbirligiDurumu.getValue().toString().equals("var")){
                    proje.setFirmaAdi(declarativeEskiProjeBilgisi.txtFirmaAdi.getValue().toString());
                    proje.setFirmaParaBirimi(declarativeEskiProjeBilgisi.cbFirmaParaBirimi.getValue().toString());
                    proje.setSagladigiFon(declarativeEskiProjeBilgisi.txtSagladigiFon.getValue().toString());

                }
                String basvurangore = declarativeEskiProjeBilgisi.txtProjedeAlinanGorev.getValue().toString();
                if (declarativeEskiProjeBilgisi.projeekibi.getValue().equals("yok")) {
                    a.getProjeler().add(proje);
                    proje.getEkiptekiler().add(a);
                    akademisyenService.akademisyenEkle(a);

                    if (basvurangore.equals("araştırmacı")) {
                        akademisyenService.iliskiyarat1(a.getId(), proje.getId());
                    } else {

                    }
                    if (basvurangore.equals("yürütücü")) {
                        akademisyenService.iliskiyarat5(a.getId(), proje.getId());
                    } else {
                        //
                    }

                    if (basvurangore.equals("koordinatör")) {
                        akademisyenService.iliskiyarat3(a.getId(), proje.getId());
                    } else {

                    }
                    if (basvurangore.equals("bursiyer")) {
                        akademisyenService.iliskiyarat2(a.getId(), proje.getId());
                    } else {
                        System.out.println("birşey yapma");
                    }


                } else if (declarativeEskiProjeBilgisi.projeekibi.getValue().equals("var")) {

                    a.getProjeler().add(proje);
                    proje.getEkiptekiler().add(a);
                    akademisyenService.akademisyenEkle(a);
                    if (basvurangore.equals("araştırmacı")) {
                        akademisyenService.iliskiyarat1(a.getId(), proje.getId());
                    } else {

                    }
                    if (basvurangore.equals("yürütücü")) {
                        akademisyenService.iliskiyarat5(a.getId(), proje.getId());
                    } else {
                        //
                    }

                    if (basvurangore.equals("koordinatör")) {
                        akademisyenService.iliskiyarat3(a.getId(), proje.getId());
                    } else {

                    }
                    if (basvurangore.equals("bursiyer")) {
                        akademisyenService.iliskiyarat2(a.getId(), proje.getId());
                    } else {
                        System.out.println("birşey yapma");
                    }

                    for (int i = 0; i < gorevliveakademisyenList.size(); i++) {

                        String ad = gorevliveakademisyenList.get(i).getAkademisyenadi();
                        String gorev = gorevliveakademisyenList.get(i).getGorev();
                        Akademisyen b = akademisyenService.adagoreakademisyengetir(ad);

                        proje.getEkiptekiler().add(b);
                        b.getProjeler().add(proje);
                        akademisyenService.akademisyenEkle(b);


                        if (gorev.equals("araştırmacı")) {
                            akademisyenService.iliskiyarat1(b.getId(), proje.getId());
                        } else {

                        }
                        if (gorev.equals("yürütücü")) {
                            akademisyenService.iliskiyarat5(b.getId(), proje.getId());
                        } else {
                            //
                        }

                        if (gorev.equals("koordinatör")) {
                            akademisyenService.iliskiyarat3(b.getId(), proje.getId());
                        } else {

                        }
                        if (gorev.equals("bursiyer")) {
                            akademisyenService.iliskiyarat2(b.getId(), proje.getId());
                        } else {
                            System.out.println("birşey yapma");
                        }

                    }
                }




                new MyNotification("kaydetme başarılı", MessageType.SUCCESS);

            }



        });

        declarativeEskiProjeBilgisi.don.addClickListener(clickEvent -> {
            getUI().getNavigator().navigateTo(AnaSayfa.VIEW_NAME);
            DeclarativeProjeListele declarativeProjeListele=new DeclarativeProjeListele(akademisyenService);
            java.util.List<Proje> yeni = null;
            yeni= akademisyenService.gettumprojeler(a.getId());
            akademisyenService.refreshGrid(yeni,Proje.class,declarativeProjeListele.tumprojeler);

            getUI().getNavigator().navigateTo(AnaSayfa.VIEW_NAME);

        });
        declarativeEskiProjeBilgisi.don2.addClickListener(clickEvent -> {
            getUI().getNavigator().navigateTo(AnaSayfa.VIEW_NAME);
            DeclarativeProjeListele declarativeProjeListele=new DeclarativeProjeListele(akademisyenService);
            java.util.List<Proje> yeni = null;
            yeni= akademisyenService.gettumprojeler(a.getId());
            akademisyenService.refreshGrid(yeni,Proje.class,declarativeProjeListele.tumprojeler);

            getUI().getNavigator().navigateTo(AnaSayfa.VIEW_NAME);

        });

        BeanItemContainer<Gorevliveakademisyen> gorevliveakademisyenBeanItemContainer = new BeanItemContainer<>(Gorevliveakademisyen.class);
        declarativeEskiProjeBilgisi.grdgorevliakademisyen.setContainerDataSource(gorevliveakademisyenBeanItemContainer);
        declarativeEskiProjeBilgisi.grdgorevliakademisyen.getContainerDataSource().removeAllItems();
        gorevliveakademisyenBeanItemContainer.addAll(gorevliveakademisyenList);
        declarativeEskiProjeBilgisi.grdgorevliakademisyen.setContainerDataSource(gorevliveakademisyenBeanItemContainer);
        declarativeEskiProjeBilgisi.grdgorevliakademisyen.removeAllColumns();
        declarativeEskiProjeBilgisi.grdgorevliakademisyen.addColumn("akademisyenadi");
        declarativeEskiProjeBilgisi.grdgorevliakademisyen.addColumn("gorev");

        declarativeEskiProjeBilgisi.grdgorevliakademisyen.getColumn("akademisyenadi").setHeaderCaption("AKADEMİSYEN");
        declarativeEskiProjeBilgisi.grdgorevliakademisyen.getColumn("gorev").setHeaderCaption("GÖREVİ ");


        declarativeEskiProjeBilgisi.btnekle.addClickListener(clickEvent -> {

            String tchash = DigestUtils.md5Hex(declarativeEskiProjeBilgisi.txtTcgorevli.getValue().toString());

            Akademisyen akademisyen = akademisyenService.akademisyenVarMi(tchash);
            String gorev = declarativeEskiProjeBilgisi.cbgorev.getValue().toString();
            String adi=akademisyen.getAdi();

            if (akademisyen != null) {
                if (!gorev.equals("") || gorev != null) {

                    gorevliveakademisyenList.add(new Gorevliveakademisyen(adi,gorev));
                    java.util.List<Gorevliveakademisyen> yeni = null;
                    yeni = gorevliveakademisyenList;
                    akademisyenService.refreshGrid(yeni, Gorevliveakademisyen.class, declarativeEskiProjeBilgisi.grdgorevliakademisyen);





                }

            }



        });
        declarativeEskiProjeBilgisi.btncikar.addClickListener(clickEvent1 -> {

            Gorevliveakademisyen cikarilan = (Gorevliveakademisyen) declarativeEskiProjeBilgisi.grdgorevliakademisyen.getSelectedRow();

            gorevliveakademisyenList.remove(cikarilan);
            java.util.List<Gorevliveakademisyen> yeni = null;
            yeni = gorevliveakademisyenList;
            akademisyenService.refreshGrid(yeni, Gorevliveakademisyen.class, declarativeEskiProjeBilgisi.grdgorevliakademisyen);

            gorevliveakademisyenList.size();
        });




        verticalLayout.addComponent(declarativeEskiProjeBilgisi);
        verticalLayout.setComponentAlignment(declarativeEskiProjeBilgisi, Alignment.MIDDLE_CENTER);
        setContent(verticalLayout);



    }

}
