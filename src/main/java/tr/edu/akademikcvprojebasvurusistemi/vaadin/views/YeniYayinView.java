package tr.edu.akademikcvprojebasvurusistemi.vaadin.views;

import com.vaadin.data.Property;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import tr.edu.akademikcvprojebasvurusistemi.domain.Akademisyen;
import tr.edu.akademikcvprojebasvurusistemi.domain.Yayinlar;
import tr.edu.akademikcvprojebasvurusistemi.service.AkademisyenService;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative.DeclarativeErrorView;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative.DeclarativeYeniYayin;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.extras.MyNotification;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.extras.enums.MessageType;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;

@UIScope
@SpringView(name = YeniYayinView.VIEW_NAME)
public class YeniYayinView extends Panel implements View{

    @Autowired
    AkademisyenService akademisyenService;

    public static final String VIEW_NAME="YeniYayinView";

    Akademisyen a= (Akademisyen) VaadinSession.getCurrent().getSession().getAttribute("akademisyen");

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
    }

    @PostConstruct
    public void init(){

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setSizeFull();
        DeclarativeYeniYayin declarativeYeniYayin = new DeclarativeYeniYayin();

        declarativeYeniYayin.yayinturu.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                if (declarativeYeniYayin.yayinturu.getValue().toString().equals("kitap")) {
                    declarativeYeniYayin.pnlkitap.setVisible(true);
                }
                else{
                    declarativeYeniYayin.pnlkitap.setVisible(false);
                }
            }
        });
        declarativeYeniYayin.yayinturu.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                if (declarativeYeniYayin.yayinturu.getValue().toString().equals("bildiri")) {
                    declarativeYeniYayin.pnlbildiri.setVisible(true);
                }
                else{
                    declarativeYeniYayin.pnlbildiri.setVisible(false);
                }
            }
        });
        declarativeYeniYayin.yayinturu.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                    if (declarativeYeniYayin.yayinturu.getValue().toString().equals("dergide makale")){
                        declarativeYeniYayin.pnlmakale1.setVisible(true);
                    }
                    else{
                        declarativeYeniYayin.pnlmakale1.setVisible(false);
                    }



            }
        });
        declarativeYeniYayin.yayinturu.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                if (declarativeYeniYayin.yayinturu.getValue().toString().equals("kitapta bölüm")) {
                    declarativeYeniYayin.pnlkitaptabolum.setVisible(true);
                }
                else{
                    declarativeYeniYayin.pnlkitaptabolum.setVisible(false);
                }
            }
        });
        declarativeYeniYayin.ulusal.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                if (declarativeYeniYayin.ulusal.getValue() == true) {
                    declarativeYeniYayin.uluslararasi.setReadOnly(true);
                }
                else {
                    declarativeYeniYayin.uluslararasi.setReadOnly(false);
                }

            }
        });
        declarativeYeniYayin.uluslararasi.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                if (declarativeYeniYayin.uluslararasi.getValue() == true) {
                    declarativeYeniYayin.ulusal.setReadOnly(true);
                }
                else {
                    declarativeYeniYayin.ulusal.setReadOnly(false);
                }

            }
        });

        declarativeYeniYayin.btnkaydet.addClickListener(clickEvent -> {
            Yayinlar yayin =new Yayinlar();
            if(declarativeYeniYayin.ulusal.getValue()==true){
                yayin.setUlusallik("ulusal");

            }
            else if(declarativeYeniYayin.uluslararasi.getValue()==true){
                yayin.setUlusallik("uluslararası");
            }
            yayin.setYayinturu(declarativeYeniYayin.yayinturu.getValue().toString());

            if(declarativeYeniYayin.yayinturu.getValue().toString().equals("kitap")){

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                yayin.setKdil(declarativeYeniYayin.kdil.getValue().toString());
                yayin.setKtur(declarativeYeniYayin.ktur.getValue().toString());
                yayin.setKkonu(declarativeYeniYayin.kkonu.getValue().toString());
                yayin.setKrol(declarativeYeniYayin.krol.getValue().toString());
                yayin.setKyayinevi(declarativeYeniYayin.kyayinevi.getValue());
                yayin.setKyayinevigrup(declarativeYeniYayin.kyayinevigrup.getValue().toString());
                yayin.setKbaslik(declarativeYeniYayin.kbaslik.getValue());
                yayin.setKbasim(simpleDateFormat.format(declarativeYeniYayin.kbasim.getValue()));

                yayin.setKbaski(declarativeYeniYayin.kbaski.getValue().toString());
                yayin.setKsonbaski(simpleDateFormat.format(declarativeYeniYayin.ksonbaski.getValue()));

                yayin.setKsayfa(declarativeYeniYayin.ksayfa.getValue());

                yayin.setKisbn(declarativeYeniYayin.kisbn.getValue());
                yayin.setKsehir(declarativeYeniYayin.ksehir.getValue());
                yayin.setKulke(declarativeYeniYayin.kulke.getValue());
                yayin.setKdoi(declarativeYeniYayin.kdoi.getValue());
                yayin.setKlink(declarativeYeniYayin.klink.getValue());
            }
            if(declarativeYeniYayin.yayinturu.getValue().equals("dergide makale") && declarativeYeniYayin.ulusal.getValue().equals(true)){
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                yayin.setBtarih(simpleDateFormat.format(declarativeYeniYayin.btarih.getValue()));
                yayin.setDtur(declarativeYeniYayin.dtur.getValue().toString());
                yayin.setMtur(declarativeYeniYayin.mtur.getValue().toString());
                yayin.setMkonu(declarativeYeniYayin.mkonu.getValue().toString());
                yayin.setMdil(declarativeYeniYayin.mdil.getValue().toString());
                yayin.setDadi(declarativeYeniYayin.dadi.getValue());
                yayin.setMbaslik(declarativeYeniYayin.mbaslik.getValue());
                yayin.setBtur(declarativeYeniYayin.btur.getValue().toString());
                yayin.setCilt(declarativeYeniYayin.cilt.getValue());
                yayin.setSayi(declarativeYeniYayin.sayi.getValue());
                yayin.setNo(declarativeYeniYayin.no.getValue());
                yayin.setMdoi(declarativeYeniYayin.mdoi.getValue());
                yayin.setMlink(declarativeYeniYayin.mlink.getValue());

                            }

            new MyNotification("kaydetme başarılı", MessageType.SUCCESS);

            a.getYayinlarList().add(yayin);

            akademisyenService.akademisyenEkle(a);


        });

        verticalLayout.addComponent(declarativeYeniYayin);
        verticalLayout.setComponentAlignment(declarativeYeniYayin, Alignment.MIDDLE_CENTER);
        setContent(verticalLayout);


        //Akademisyen a= (Akademisyen) VaadinSession.getCurrent().getSession().getAttribute("akademisyen");

    }
}