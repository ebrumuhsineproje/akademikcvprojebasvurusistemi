package tr.edu.akademikcvprojebasvurusistemi.vaadin.views;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative.DeclarativeErrorView;

import javax.annotation.PostConstruct;

@UIScope
@SpringView(name = MyErrorView.VIEW_NAME)
public class MyErrorView extends Panel implements View{

    public static final String VIEW_NAME="MyErrorView";

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
    }

    @PostConstruct
    public void init(){

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setSizeFull();
        DeclarativeErrorView declarativeErrorView = new DeclarativeErrorView();

        verticalLayout.addComponent(declarativeErrorView);
        verticalLayout.setComponentAlignment(declarativeErrorView, Alignment.MIDDLE_CENTER);
        setContent(verticalLayout);


        //Akademisyen a= (Akademisyen) VaadinSession.getCurrent().getSession().getAttribute("akademisyen");

    }
}
