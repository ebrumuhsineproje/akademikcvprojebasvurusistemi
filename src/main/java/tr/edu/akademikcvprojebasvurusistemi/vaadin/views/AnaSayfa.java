package tr.edu.akademikcvprojebasvurusistemi.vaadin.views;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.*;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.template.Neo4jOperations;
import tr.edu.akademikcvprojebasvurusistemi.domain.Akademisyen;
import tr.edu.akademikcvprojebasvurusistemi.service.AkademisyenService;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative.*;

import javax.annotation.PostConstruct;
import java.awt.*;

/**
 * Created by LENOVO on 22.1.2017.
 */
@UIScope
@SpringView(name = AnaSayfa.VIEW_NAME)
public class AnaSayfa extends Panel implements View {
    public static final String VIEW_NAME = "anasayfa";
    @Autowired
    AkademisyenService akademisyenService;

    @Autowired
    Neo4jOperations neo4jOperations;


    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

        Akademisyen a = (Akademisyen) VaadinSession.getCurrent().getSession().getAttribute("akademisyen");
        if (a == null) {
            getUI().getNavigator().navigateTo(LoginView.VIEW_NAME);
        }

    }


    @PostConstruct
    public void init() {
        DeclarativeAnaSayfa declarativeAnaSayfa = new DeclarativeAnaSayfa();

        Akademisyen a = (Akademisyen) VaadinSession.getCurrent().getSession().getAttribute("akademisyen");

        int sayi2=akademisyenService.gettumprojeler(a.getId()).size();
        if(sayi2==0){
            declarativeAnaSayfa.sayi2.setValue("Proje Başvurunuz bulunmamaktadır.");

        }
        else{
            String b = String.valueOf(sayi2);
            declarativeAnaSayfa.sayi2.setValue("Toplam "+b+" tane projeye başvurdunuz");

        }

        int sayi1=akademisyenService.kabuledilmisprojeler(a.getId()).size();
        if(sayi1==0){
            declarativeAnaSayfa.sayi1.setValue("kabul edilen projeniz bulunmamaktadır");
        }
        if(sayi1==0 && sayi2==0){
            declarativeAnaSayfa.sayi1.setValue("");
        }
        if(sayi1!=0){
            String c=String.valueOf(sayi1);
            declarativeAnaSayfa.sayi1.setValue(c+" tanesi kabul edildi");

        }

        if(sayi2!=0){
            int sayi3= (sayi1*100)/sayi2;

            String d=String.valueOf(sayi3);
            declarativeAnaSayfa.sayi3.setValue("Başarı oranınız %"+d);
        }
        else{
            declarativeAnaSayfa.sayi3.setValue("");
        }


        declarativeAnaSayfa.proje.addItem("KABUL EDİLMEMİŞ PROJE", new MenuBar.Command() {

            @Override
            public void menuSelected(MenuBar.MenuItem menuItem) {

                declarativeAnaSayfa.panel2.setContent(new DeclarativeProjeListele(akademisyenService));
                declarativeAnaSayfa.panel2.setSizeFull();
            }

        });

        declarativeAnaSayfa.proje.addItem("KABUL EDİLMİŞ PROJE", new MenuBar.Command() {

            @Override
            public void menuSelected(MenuBar.MenuItem menuItem) {

                declarativeAnaSayfa.panel2.setContent(new DeclarativeKabulEdilmisProjeListesi(akademisyenService));
                declarativeAnaSayfa.panel2.setSizeFull();
            }

        });



        declarativeAnaSayfa.barmenu.addItem("YAYIN BİLGİLERİ", new MenuBar.Command() {

            public void menuSelected(MenuBar.MenuItem selectedItem) {
                declarativeAnaSayfa.panel2.setContent(new DeclarativeYayinListeleView(akademisyenService));
                declarativeAnaSayfa.panel2.setSizeFull();

            }
        });
        declarativeAnaSayfa.barmenu.addItem("EĞİTİM BİLGİLERİ", new MenuBar.Command() {

            public void menuSelected(MenuBar.MenuItem selectedItem) {
                declarativeAnaSayfa.panel2.setContent(new DeclarativeEgitimBilgileriGirisView(akademisyenService));


            }
        });
        declarativeAnaSayfa.barmenu.addItem("CV İNCELE", new MenuBar.Command() {

            public void menuSelected(MenuBar.MenuItem selectedItem) {
                declarativeAnaSayfa.panel2.setContent(new DeclarativeCvIncele(akademisyenService));


            }
        });


        declarativeAnaSayfa.barmenu.addItem("ÇIKIŞ", new MenuBar.Command() {

            public void menuSelected(MenuBar.MenuItem selectedItem) {

                VaadinSession.getCurrent().getSession().setAttribute("akademisyen", null);
                getUI().getPage().setLocation(LoginView.VIEW_NAME);

            }
        });

        setContent(declarativeAnaSayfa);


    }
}

