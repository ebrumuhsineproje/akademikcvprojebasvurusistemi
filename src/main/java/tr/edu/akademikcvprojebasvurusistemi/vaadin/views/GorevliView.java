package tr.edu.akademikcvprojebasvurusistemi.vaadin.views;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.DateRenderer;
import org.neo4j.cypher.internal.frontend.v2_3.ast.functions.Str;
import org.neo4j.kernel.impl.api.store.StoreIteratorRelationshipCursor;
import org.springframework.beans.factory.annotation.Autowired;
import tr.edu.akademikcvprojebasvurusistemi.domain.Akademisyen;
import tr.edu.akademikcvprojebasvurusistemi.domain.Gorevli;
import tr.edu.akademikcvprojebasvurusistemi.domain.Proje;
import tr.edu.akademikcvprojebasvurusistemi.domain.Yayinlar;
import tr.edu.akademikcvprojebasvurusistemi.repository.YayinRepository;
import tr.edu.akademikcvprojebasvurusistemi.service.AkademisyenService;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.declarative.DeclarativeGorevliSayfasi;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.extras.MyNotification;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.extras.enums.MessageType;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;


@UIScope
@SpringView(name = GorevliView.VIEW_NAME)
public class GorevliView extends Panel implements View {
    public static final String VIEW_NAME = "GorevliSayfasi";

    List<Proje> projeler=new ArrayList<>();
    @Autowired
    AkademisyenService akademisyenService;

    @Autowired
    YayinRepository yayinRepository;

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        Gorevli gorevli = (Gorevli) VaadinSession.getCurrent().getSession().getAttribute("Gorevli");
        if (gorevli == null) {
            getUI().getNavigator().navigateTo(LoginView.VIEW_NAME);
        }
    }

    @PostConstruct
    public void initializeView() {
        Akademisyen a = (Akademisyen) VaadinSession.getCurrent().getSession().getAttribute("akademisyen");
        //setSizeFull();

        VerticalLayout vly = new VerticalLayout();
        vly.setSizeFull();

        DeclarativeGorevliSayfasi declarativeGorevliSayfasi = new DeclarativeGorevliSayfasi(akademisyenService);

        java.util.List<Proje> plist = null;
        plist = akademisyenService.tumprojeler();



        BeanItemContainer<Proje> yayinlarBeanItemContainer = new BeanItemContainer<>(Proje.class);
        declarativeGorevliSayfasi.projeveakademisyenlistesi.setContainerDataSource(yayinlarBeanItemContainer);
        declarativeGorevliSayfasi.projeveakademisyenlistesi.getContainerDataSource().removeAllItems();
        yayinlarBeanItemContainer.addAll(plist);
        declarativeGorevliSayfasi.projeveakademisyenlistesi.setContainerDataSource(yayinlarBeanItemContainer);
        declarativeGorevliSayfasi.projeveakademisyenlistesi.removeAllColumns();
        declarativeGorevliSayfasi.projeveakademisyenlistesi.addColumn("basvuruyapanadi");
        declarativeGorevliSayfasi.projeveakademisyenlistesi.addColumn("basvuruyapansoyadi");
        declarativeGorevliSayfasi.projeveakademisyenlistesi.addColumn("simdikiZaman");
        declarativeGorevliSayfasi.projeveakademisyenlistesi.addColumn("projeadi");
        declarativeGorevliSayfasi.projeveakademisyenlistesi.addColumn("onaydurumu");
        declarativeGorevliSayfasi.projeveakademisyenlistesi.getColumn("basvuruyapanadi").setHeaderCaption("AKADEMİSYEN ADI");
        declarativeGorevliSayfasi.projeveakademisyenlistesi.getColumn("basvuruyapansoyadi").setHeaderCaption("AKADEMİSYEN SOYADI");
        declarativeGorevliSayfasi.projeveakademisyenlistesi.getColumn("projeadi").setHeaderCaption("PROJE ADI");
        declarativeGorevliSayfasi.projeveakademisyenlistesi.getColumn("simdikiZaman").setHeaderCaption("BAŞVURU ZAMANI");
        declarativeGorevliSayfasi.projeveakademisyenlistesi.getColumn("onaydurumu").setHeaderCaption("ONAY DURUMU");

        declarativeGorevliSayfasi.projeveakademisyenlistesi.getColumn("simdikiZaman").setRenderer(new DateRenderer(new SimpleDateFormat("dd/MM/yyyy")));

        declarativeGorevliSayfasi.btn.addClickListener(clickEvent -> {

            //DeclarativeLoginView declarativeLoginView=new DeclarativeLoginView();
            //Akademisyen a= akademisyenService.akademisyenGetir(declarativeLoginView.txtEmail.getValue());

            VaadinSession.getCurrent().getSession().setAttribute("Gorevli", null);
            //VaadinSession.getCurrent().close();
            //getUI().getSession().close();
            getUI().getPage().setLocation(LoginView.VIEW_NAME);

        });
        /*
        declarativeGorevliSayfasi.onayla.addClickListener(clickEvent -> {

                Collection<Object> seciliproje = declarativeGorevliSayfasi.projeveakademisyenlistesi.getSelectedRows();
                if (seciliproje.size() != 0) {


                    List<Proje> projes = new ArrayList<>();

                    for (Object o : seciliproje) {
                        projes.add((Proje) o);
                    }


                    akademisyenService.projesil(projes);
                    java.util.List<Proje> yeni = null;
                    yeni = akademisyenService.gettumprojeler(a.getId());
                    akademisyenService.refreshGrid(yeni, Proje.class, declarativeGorevliSayfasi.projeveakademisyenlistesi);


                    new MyNotification("onaylama başarılı", MessageType.SUCCESS);

                } else {
                    new MyNotification("onaylamak istediğiniz satırı seçin", MessageType.FAIL);
                }




        });*/

        declarativeGorevliSayfasi.onayla.addClickListener(clickEvent -> {


            Collection<Object> seciliproje = declarativeGorevliSayfasi.projeveakademisyenlistesi.getSelectedRows();

            if (seciliproje.size() != 0) {


                List<Proje> projes = new ArrayList<>();

                for (Object o : seciliproje) {
                    projes.add((Proje) o);
                }
                for (int i = 0; i < seciliproje.size(); i++) {
                    String durum = projes.get(i).getOnaydurumu();

                    akademisyenService.onayla(projes);


                    java.util.List<Proje> yeni = null;
                    yeni = akademisyenService.tumprojeler();
                    akademisyenService.refreshGrid(yeni, Proje.class, declarativeGorevliSayfasi.projeveakademisyenlistesi);

                }


            } else {
                new MyNotification("onaylamak istediğiniz projeyi seçin", MessageType.FAIL);
            }


        });
        declarativeGorevliSayfasi.kabuledilmis.addClickListener(clickEvent -> {


            Collection<Object> seciliproje = declarativeGorevliSayfasi.projeveakademisyenlistesi.getSelectedRows();

            if (seciliproje.size() != 0) {


                List<Proje> projes = new ArrayList<>();

                for (Object o : seciliproje) {
                    projes.add((Proje) o);
                }
                for (int i = 0; i < seciliproje.size(); i++) {
                    String durum = projes.get(i).getOnaydurumu();

                    akademisyenService.kabulet(projes);


                    java.util.List<Proje> yeni = null;
                    yeni = akademisyenService.tumprojeler();
                    akademisyenService.refreshGrid(yeni, Proje.class, declarativeGorevliSayfasi.projeveakademisyenlistesi);

                }


            } else {
                new MyNotification("kabul etmek  istediğiniz projeyi seçin", MessageType.FAIL);
            }


        });
        declarativeGorevliSayfasi.imzabekliyor.addClickListener(clickEvent -> {


            Collection<Object> seciliproje = declarativeGorevliSayfasi.projeveakademisyenlistesi.getSelectedRows();

            if (seciliproje.size() != 0) {


                List<Proje> projes = new ArrayList<>();

                for (Object o : seciliproje) {
                    projes.add((Proje) o);
                }
                for (int i = 0; i < seciliproje.size(); i++) {
                    String durum = projes.get(i).getOnaydurumu();

                    akademisyenService.imzabekliyor(projes);


                    java.util.List<Proje> yeni = null;
                    yeni = akademisyenService.tumprojeler();
                    akademisyenService.refreshGrid(yeni, Proje.class, declarativeGorevliSayfasi.projeveakademisyenlistesi);

                }


            } else {
                new MyNotification("imza durumuna getirmek istediğiniz projeyi seçin", MessageType.FAIL);
            }


        });

       /* declarativeGorevliSayfasi.btnara.addClickListener(clickEvent -> {

           Long baslangic= declarativeGorevliSayfasi.baslangic.getValue().getTime();
           Long Bitis=declarativeGorevliSayfasi.bitis.getValue().getTime();


            List<Proje> projeler=akademisyenService.tumprojeler();
            List<Proje> projeler2 = new ArrayList<>();
           for(int i=0;i<projeler.size();i++){
               if(projeler.get(i).getSimdikiZaman()>baslangic && projeler.get(i).getSimdikiZaman()<Bitis ) {
                   Proje temp = new Proje();
                   temp=projeler.get(i);

                   projeler2.add(temp);
                   System.out.println(projeler2.get(i).ar_geNiteligi);

               }



           }

            java.util.List<Proje> yeniproje = null;
            yeniproje = projeler2;
            akademisyenService.refreshGrid(yeniproje, Proje.class, declarativeGorevliSayfasi.projeveakademisyenlistesi);





        });*/
        declarativeGorevliSayfasi.btnara.addClickListener(clickEvent -> {

                    if (declarativeGorevliSayfasi.cbprojeturu.getValue() != null && declarativeGorevliSayfasi.cbonaydurumu.getValue() != null && declarativeGorevliSayfasi.baslangic.getValue() != null && declarativeGorevliSayfasi.bitis.getValue() != null) {
                        String onay = declarativeGorevliSayfasi.cbonaydurumu.getValue().toString();
                        String projeturu = declarativeGorevliSayfasi.cbprojeturu.getValue().toString();
                        Long baslangic = declarativeGorevliSayfasi.baslangic.getValue().getTime();
                        Long Bitis = declarativeGorevliSayfasi.bitis.getValue().getTime();


                        if (!declarativeGorevliSayfasi.cbprojeturu.getValue().equals("TÜMÜ") && !declarativeGorevliSayfasi.cbonaydurumu.getValue().equals("TÜMÜ") ) {
                            projeler=akademisyenService.turveonayagoregetir(onay,projeturu);


                            if (projeler.size() != 0) {
                                List<Proje> projeler2 = new ArrayList<>();
                                for (int i = 0; i < projeler.size(); i++) {
                                    if (projeler.get(i).getSimdikiZaman() > baslangic && projeler.get(i).getSimdikiZaman() < Bitis) {
                                        Proje temp = new Proje();
                                        temp = projeler.get(i);

                                        projeler2.add(temp);
                                    }

                                }
                                akademisyenService.refreshGrid(projeler2, Proje.class, declarativeGorevliSayfasi.projeveakademisyenlistesi);

                            } else {
                                akademisyenService.refreshGrid(projeler, Proje.class, declarativeGorevliSayfasi.projeveakademisyenlistesi);

                            }
                        }



                        else if(declarativeGorevliSayfasi.cbonaydurumu.getValue().equals("TÜMÜ") && !declarativeGorevliSayfasi.cbprojeturu.getValue().equals("TÜMÜ")) {
                            projeler = akademisyenService.turegoregetir(projeturu);

                            if (projeler.size() != 0) {
                                List<Proje> projeler2 = new ArrayList<>();
                                for (int i = 0; i < projeler.size(); i++) {
                                    if (projeler.get(i).getSimdikiZaman() > baslangic && projeler.get(i).getSimdikiZaman() < Bitis) {
                                        Proje temp = new Proje();
                                        temp = projeler.get(i);

                                        projeler2.add(temp);
                                        System.out.println(projeler2.get(i).ar_geNiteligi);

                                    }

                                }
                                akademisyenService.refreshGrid(projeler2, Proje.class, declarativeGorevliSayfasi.projeveakademisyenlistesi);

                            } else {
                                akademisyenService.refreshGrid(projeler, Proje.class, declarativeGorevliSayfasi.projeveakademisyenlistesi);

                            }


                        }
                        else if(!declarativeGorevliSayfasi.cbonaydurumu.getValue().equals("TÜMÜ") && declarativeGorevliSayfasi.cbprojeturu.getValue().equals("TÜMÜ")) {
                            projeler = akademisyenService.onayagoregetir(onay);

                            if (projeler.size() != 0) {
                                List<Proje> projeler2 = new ArrayList<>();
                                for (int i = 0; i < projeler.size(); i++) {
                                    if (projeler.get(i).getSimdikiZaman() > baslangic && projeler.get(i).getSimdikiZaman() < Bitis) {
                                        Proje temp = new Proje();
                                        temp = projeler.get(i);

                                        projeler2.add(temp);
                                        System.out.println(projeler2.get(i).ar_geNiteligi);

                                    }

                                }
                                akademisyenService.refreshGrid(projeler2, Proje.class, declarativeGorevliSayfasi.projeveakademisyenlistesi);

                            } else {
                                akademisyenService.refreshGrid(projeler, Proje.class, declarativeGorevliSayfasi.projeveakademisyenlistesi);

                            }


                        }

                        else{
                            projeler = akademisyenService.tumprojeler();

                            if (projeler.size() != 0) {
                                List<Proje> projeler2 = new ArrayList<>();
                                for (int i = 0; i < projeler.size(); i++) {
                                    if (projeler.get(i).getSimdikiZaman() > baslangic && projeler.get(i).getSimdikiZaman() < Bitis) {
                                        Proje temp = new Proje();
                                        temp = projeler.get(i);

                                        projeler2.add(temp);
                                        System.out.println(projeler2.get(i).ar_geNiteligi);

                                    }

                                }
                                akademisyenService.refreshGrid(projeler2, Proje.class, declarativeGorevliSayfasi.projeveakademisyenlistesi);

                            } else {
                                akademisyenService.refreshGrid(projeler, Proje.class, declarativeGorevliSayfasi.projeveakademisyenlistesi);

                            }


                        }







                        // List<Proje> projeler=akademisyenService.tumprojeler();

                    }
                    else {
                        new MyNotification("arama kriteri griniz",MessageType.FAIL);
                    }






        });

        declarativeGorevliSayfasi.istatistik.addClickListener(clickEvent -> {
            getUI().getPage().open("http:#!Istatistikler", "_blank",false);//formu yenı sekmede acıyor

        });


        vly.setStyleName("verticalgorevli");
        vly.addComponent(declarativeGorevliSayfasi);
        vly.setComponentAlignment(declarativeGorevliSayfasi, Alignment.MIDDLE_CENTER);
        setContent(vly);

        // setContent(declarativeGorevliSayfasi);
    }

    /*private String long2Tarih(long tarih) {
        long val = tarih;
        Date date = new Date(val);
        SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yy");
        String dateText = df2.format(date);
        return dateText;
    }*/


}
