package tr.edu.akademikcvprojebasvurusistemi.vaadin.extras;

import com.vaadin.server.Page;
import com.vaadin.ui.Notification;
import com.vaadin.ui.themes.ValoTheme;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.extras.enums.MessageType;

public class MyNotification {
    public MyNotification(String desc, MessageType messageType) {
        if (messageType == MessageType.FAIL) {
            Notification notification = new Notification("");
            notification.setDescription(desc);
            notification.setStyleName(ValoTheme.NOTIFICATION_FAILURE);
            notification.setDelayMsec(5000);
            notification.show(Page.getCurrent());
        } else if (messageType == MessageType.SUCCESS) {
            Notification notification = new Notification("");
            notification.setDescription(desc);
            notification.setStyleName(ValoTheme.NOTIFICATION_SUCCESS);
            notification.setDelayMsec(5000);
            notification.show(Page.getCurrent());
        } else if (messageType == MessageType.WARNING) {
            Notification notification = new Notification("");
            notification.setDescription(desc);
            notification.setStyleName(ValoTheme.NOTIFICATION_WARNING);
            notification.setDelayMsec(5000);
            notification.show(Page.getCurrent());
        }

    }


}
