package tr.edu.akademikcvprojebasvurusistemi.vaadin.extras.enums;


public enum MessageType {
    SUCCESS,
    FAIL,
    WARNING
}
