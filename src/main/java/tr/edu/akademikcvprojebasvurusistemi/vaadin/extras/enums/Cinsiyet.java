package tr.edu.akademikcvprojebasvurusistemi.vaadin.extras.enums;


public enum Cinsiyet {
    KADIN("Kadın" ,1), ERKEK("Erkek" ,2);

    String adi;
    int sirasi;

    public int getSirasi() {
        return sirasi;
    }

    Cinsiyet(String _adi , int _sirasi) {
        adi = _adi;
        sirasi = _sirasi;
    }

    public String getAdi() {
        return adi;
    }

    @Override
    public String toString() {
        return getAdi() + " " + getSirasi();
    }
}
