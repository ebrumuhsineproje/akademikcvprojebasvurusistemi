package tr.edu.akademikcvprojebasvurusistemi.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tr.edu.akademikcvprojebasvurusistemi.domain.Lisans;
import tr.edu.akademikcvprojebasvurusistemi.domain.Lise;

import java.util.List;

/**
 * Created by LENOVO on 15.12.2016.
 */
@Repository
public interface LisansRepository extends GraphRepository<Lisans> {
    @Query("MATCH (n:Akademisyen)-[:HAS_LISANS]->(lisans:Lisans) where id(n)={akademisyenID} return lisans;")
    Lisans lisansGetir(@Param("akademisyenID")Long ID);

    @Query("MATCH (n:Akademisyen)-[:HAS_LISANS]->(l:Lisans) where id(n)={akademisyenID} return l;")
    List<Lisans> akademisyeneaitlisanslarigetir(@Param("akademisyenID")Long ID);

    @Query("MATCH (l:Lisans) where id(l)={LisansID} return l;")
    Lisans teklisansgetir(@Param("LisansID")Long ID);

}
