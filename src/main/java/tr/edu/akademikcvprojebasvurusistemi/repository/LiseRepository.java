package tr.edu.akademikcvprojebasvurusistemi.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tr.edu.akademikcvprojebasvurusistemi.domain.Akademisyen;
import tr.edu.akademikcvprojebasvurusistemi.domain.Entity;
import tr.edu.akademikcvprojebasvurusistemi.domain.Lise;
import tr.edu.akademikcvprojebasvurusistemi.domain.Yayinlar;

import java.util.List;
import java.util.Locale;

/**
 * Created by LENOVO on 15.12.2016.
 */
@Repository
//id ye göre lise getiriyor
public interface LiseRepository extends GraphRepository<Lise> {
    @Query("MATCH (n:Akademisyen)-[:HAS_LISE]->(l:Lise) where id(n)={akademisyenID} return l;")
    Lise liseGetir(@Param("akademisyenID")Long ID);

    @Query("MATCH (n:Akademisyen)-[:HAS_LISE]->(l:Lise) where id(n)={akademisyenID} return l;")
    List<Lise> akademisyeneaitliseler(@Param("akademisyenID")Long ID);

    @Query("MATCH (l:Lise) where id(l)={liseID} return l;")
    Lise teklisegetir(@Param("liseID")Long ID);
    
    
   





}
