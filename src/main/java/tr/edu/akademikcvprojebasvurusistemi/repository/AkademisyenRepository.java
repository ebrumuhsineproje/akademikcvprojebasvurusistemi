package tr.edu.akademikcvprojebasvurusistemi.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tr.edu.akademikcvprojebasvurusistemi.domain.Akademisyen;
import tr.edu.akademikcvprojebasvurusistemi.domain.Yayinlar;

import java.util.List;


@Repository
public interface AkademisyenRepository extends GraphRepository<Akademisyen> {

    @Query("match (a:Akademisyen) return a;")
    List<Akademisyen> getTumAkademisyenler();

    @Query("MATCH (n:Akademisyen) where n.tc = {akademisyenTc} RETURN n;")
    Akademisyen akademisyenVarMi(@Param("akademisyenTc") String akademisyenTc);
    
    //mail e göre akademisyen getir
    @Query ("MATCH (n:Akademisyen) where n.email={email} RETURN n;")
    Akademisyen akademisyenGetir(@Param("email") String email);
    
    //tc ye gore akademisyen getir
    @Query ("MATCH (n:Akademisyen) where n.tc={tc} RETURN n;")
    Akademisyen akademisyengetir(@Param("tc") String tc);

    @Query ("MATCH (n:Akademisyen) where n.adi={adi} RETURN n;")
    Akademisyen adagoregetir(@Param("adi") String adi);

    @Query ("MATCH (n:Akademisyen) where n.adi={adi} and  n.soyadi={soyadi} and n.bolum={bolum} RETURN n;")
    List adsoyadbolumegoregetir(@Param("adi") String adi,@Param("soyadi") String soyadi,@Param("bolum") String bolum);

    @Query ("MATCH (n:Akademisyen) where n.adi={adi} and  n.soyadi={soyadi}  RETURN n;")
    List adasoyadagoregetir(@Param("adi") String adi,@Param("soyadi") String soyadi);

    @Query ("MATCH (n:Akademisyen) where n.adi={adi} and  n.bolum={bolum}  RETURN n;")
    List adabolume(@Param("adi") String adi,@Param("bolum") String bolum);

    @Query ("MATCH (n:Akademisyen) where n.soyadi={soyadi} and  n.bolum={bolum}  RETURN n;")
    List soyadabolume(@Param("soyadi") String soyadi,@Param("bolum") String bolum);

    @Query ("MATCH (n:Akademisyen) where  n.soyadi={soyadi}  RETURN n;")
    List soyadagoregetir(@Param("soyadi") String soyadi);

    @Query ("MATCH (n:Akademisyen) where n.bolum={bolum}   RETURN n;")
    List bolumegoregetir(@Param("bolum") String bolum);

    @Query ("MATCH (n:Akademisyen) where n.adi={adi}   RETURN n;")
    List adagore2(@Param("adi") String adi);



}
