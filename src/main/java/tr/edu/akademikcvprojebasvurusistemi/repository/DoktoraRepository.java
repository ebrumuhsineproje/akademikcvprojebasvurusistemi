package tr.edu.akademikcvprojebasvurusistemi.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tr.edu.akademikcvprojebasvurusistemi.domain.Doktora;
import tr.edu.akademikcvprojebasvurusistemi.domain.Lisans;
import tr.edu.akademikcvprojebasvurusistemi.domain.Lise;
import tr.edu.akademikcvprojebasvurusistemi.domain.YuksekLisans;

import java.util.List;

/**
 * Created by LENOVO on 15.12.2016.
 */
@Repository
public interface DoktoraRepository extends GraphRepository<Doktora> {
    @Query("MATCH (n:Akademisyen)-[:HAS_DOKTORA]->(doktora:Doktora) where id(n)={akademisyenID} return doktora;")
    Doktora doktoraGetir(@Param("akademisyenID")Long ID);

    @Query("MATCH (n:Akademisyen)-[:HAS_DOKTORA]->(doktora:Doktora) where id(n)={akademisyenID} return doktora;")
    List<Doktora> akademisyeneaitdoktoralarigetir(@Param("akademisyenID")Long ID);

    @Query("MATCH (doktora:Doktora) where id(doktora)={dID} return doktora;")
    Doktora tekdoktoragetir(@Param("dID")Long ID);




}
