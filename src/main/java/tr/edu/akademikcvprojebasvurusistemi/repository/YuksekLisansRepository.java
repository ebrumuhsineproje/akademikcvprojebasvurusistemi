package tr.edu.akademikcvprojebasvurusistemi.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tr.edu.akademikcvprojebasvurusistemi.domain.Lisans;
import tr.edu.akademikcvprojebasvurusistemi.domain.Lise;
import tr.edu.akademikcvprojebasvurusistemi.domain.YuksekLisans;

import java.util.List;

/**
 * Created by LENOVO on 15.12.2016.
 */
@Repository
public interface YuksekLisansRepository extends GraphRepository<YuksekLisans> {
    @Query("MATCH (n:Akademisyen)-[:HAS_YUKSEKLISANS]->(yukseklisans:YuksekLisans) where id(n)={akademisyenID} return yukseklisans;")
    YuksekLisans yukseklisansGetir(@Param("akademisyenID")Long ID);

    @Query("MATCH (n:Akademisyen)-[:HAS_YUKSEKLISANS]->(yukseklisans:YuksekLisans) where id(n)={akademisyenID} return yukseklisans;")
    List<YuksekLisans> akademisyeneaityukseklisanslarigetir(@Param("akademisyenID")Long ID);

    @Query("MATCH (yuksekLisans:YuksekLisans) where id(yuksekLisans)={yID} return yuksekLisans;")
    YuksekLisans tekyuksekgetir(@Param("yID")Long ID);



}

