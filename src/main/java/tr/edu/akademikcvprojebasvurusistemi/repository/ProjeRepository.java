/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.edu.akademikcvprojebasvurusistemi.repository;

import java.util.List;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tr.edu.akademikcvprojebasvurusistemi.domain.Akademisyen;
import tr.edu.akademikcvprojebasvurusistemi.domain.Lise;
import tr.edu.akademikcvprojebasvurusistemi.domain.Proje;
import tr.edu.akademikcvprojebasvurusistemi.domain.Yayinlar;

/**
 *
 * @author ubuntu
 */
@Repository
public interface ProjeRepository extends GraphRepository {

    @Query("MATCH (n:Akademisyen)-[:HAS_PROJE]->(p:Proje) where id(n)={akademisyenID} return p;")
    Proje projegetir(@Param("akademisyenID")Long ID);


    //kullanılmıyor
    @Query("Start n=NODE({id}), p=NODE({id}) MATCH(n) MATCH(p) CREATE (n)-[:YURUTUCU]->(P)")
    public void createRelationship(@Param("id") Long Id, @Param("id") Long ID);


    @Query("MATCH  (n:Akademisyen),(p:Proje) where  id(n)={akademisyenID} AND  id(p)={pID}   CREATE (p)-[r:YURUTUCU]->(n) return r")
    public void iliskiyarat5(@Param("akademisyenID") Long Id, @Param("pID") Long ID);

    @Query("MATCH  (n:Akademisyen),(p:Proje) where  id(n)={akademisyenID} AND  id(p)={pID}   CREATE (p)-[r:ARASTIRMACI]->(n) return r")
    public void iliskiyarat1(@Param("akademisyenID") Long Id, @Param("pID") Long ID);
    @Query("MATCH  (n:Akademisyen),(p:Proje) where  id(n)={akademisyenID} AND  id(p)={pID}   CREATE (p)-[r:BURSIYER]->(n) return r")
    public void iliskiyarat2(@Param("akademisyenID") Long Id, @Param("pID") Long ID);
    @Query("MATCH  (n:Akademisyen),(p:Proje) where  id(n)={akademisyenID} AND  id(p)={pID}   CREATE (p)-[r:KOORDINATOR]->(n) return r")
    public void iliskiyarat3(@Param("akademisyenID") Long Id, @Param("pID") Long ID);

    @Query("MATCH  (n:Akademisyen),(p:Proje) where  id(n)={akademisyenID} AND  id(p)={pID}   CREATE (p)-[r:HAS_EKIP]->(n) return r")
    public void iliskiyarat4(@Param("akademisyenID") Long Id, @Param("pID") Long ID);

    @Query("MATCH  (n:Akademisyen),(p:Proje) where  id(n)={akademisyenID} AND  id(p)={pID}   CREATE (p)<-[r:HAS_PROJE]-(n) return r")
    public void iliskiyaratproje(@Param("akademisyenID") Long Id, @Param("pID") Long ID);

    @Query("MATCH (n:Akademisyen)-[:HAS_PROJE]->(p:Proje) where id(n)={akademisyenID} return p;")
    List<Proje> akademisyeneaitprojeler(@Param("akademisyenID")Long ID);



    @Query("MATCH (n:Akademisyen)-[projeiliskisi:HAS_PROJE]->(p:Proje) where id(n)={akademisyenID} and id(p)={projeid}  delete projeiliskisi")
    public void iliskisil(@Param("akademisyenID")Long ID,@Param("projeid") Long Id);

    @Query("MATCH (p:Proje)-[r]->(n:Akademisyen) where id(n)={akademisyenID} and id(p)={projeid} return type(r);")
    public List<String> iliskigetir(@Param("akademisyenID")Long ID,@Param("projeid") Long Id);


    @Query("MATCH (p:Proje)-[r:HAS_EKIP]->(n:Akademisyen) where id(n)={akademisyenID} and id(p)={projeid}  delete r")
    public void iliskisil2(@Param("akademisyenID")Long ID,@Param("projeid") Long Id);

    @Query("MATCH (p:Proje)-[r:YURUTUCU]->(n:Akademisyen) where id(n)={akademisyenID} and id(p)={projeid}  delete r")
    public void iliskisil3(@Param("akademisyenID")Long ID,@Param("projeid") Long Id);


    //guncelleme çalışmıyor
    @Query("MATCH (p:Proje)-[r:]->(n:Akademisyen) where id(n)={akademisyenID} and id(p)={projeid} SET r.name='YURUTUCU' ")
    Iterable<Proje> guncelle1(@Param("akademisyenID")Long ID,@Param("projeid") Long Id);

    @Query("MATCH (p:Proje)-[r:ARASTIRMACI]->(n:Akademisyen) where id(n)={akademisyenID} and id(p)={projeid}  delete r")
    public void iliskisil4(@Param("akademisyenID")Long ID,@Param("projeid") Long Id);

    @Query("MATCH (p:Proje)-[r:BURSIYER]->(n:Akademisyen) where id(n)={akademisyenID} and id(p)={projeid}  delete r")
    public void iliskisil5(@Param("akademisyenID")Long ID,@Param("projeid") Long Id);

    @Query("MATCH (p:Proje)-[r:KOORDINATOR]->(n:Akademisyen) where id(n)={akademisyenID} and id(p)={projeid}  delete r")
    public void iliskisil6(@Param("akademisyenID")Long ID,@Param("projeid") Long Id);

    @Query("MATCH (p:Proje)-[:HAS_EKIP]->(n:Akademisyen) where id(p)={projeıd} return n;")
    List<Akademisyen> ekibigetir(@Param("projeıd")Long ID);


    //?
    @Query("MATCH (p:Proje)<-[:HAS_PROJE]-(n:Akademisyen) where id(p)={aid} return n;")
    List<Proje> ekibigetir2(@Param("aid")Long ID);




    @Query("MATCH (p:Proje) where id(p)={pID} return p;")
    Proje tekprojegetir(@Param("pID")Long ID);

    @Query("match (a:Proje) return a;")
    List<Proje> tumprojeler();

    @Query("MATCH (p:Proje) where  (p.onaydurumu='KABUL EDİLMİŞ') return p;")
    List<Proje> tumkabuledilenler();

    @Query("MATCH (n:Akademisyen)-[:HAS_PROJE]->(p:Proje) where (id(n)={akademisyenID}) AND ((p.onaydurumu='KABUL EDİLMİŞ') OR (p.onaydurumu ='onaylanmış')) return p;")
    List<Proje> onaylanmisprojeler(@Param("akademisyenID")Long ID);
    @Query("MATCH (n:Akademisyen)-[:HAS_PROJE]->(p:Proje) where (id(n)={akademisyenID}) AND (p.onaydurumu='KABUL EDİLMİŞ') return p;")
    List<Proje> kabuledilmisprojeler(@Param("akademisyenID")Long ID);
    @Query("MATCH (n:Akademisyen)-[:HAS_PROJE]->(p:Proje) where id(n)={akademisyenID} AND( p.onaydurumu ='onaylanmış' OR p.onaydurumu='imza bekliyor') return p;")
    List<Proje> onayveyaimza(@Param("akademisyenID")Long ID);


    //tür ve kabul edilen
    @Query("MATCH (p:Proje) where ( p.onaydurumu ='KABUL EDİLMİŞ')  AND (p.projeTuru={projeturu}) return p;")
    List<Proje> turkabul(@Param("projeturu") String projeturu);

    //proje var mı
    @Query("MATCH (n:Proje) where n.projeadi = {projeadi} RETURN n;")
    Proje projevarmi(@Param("projeadi") String projeadi);


    @Query ("MATCH (n:Proje) where n.onaydurumu={onaydurumu}   RETURN n;")
    List<Proje> onayagoregetir(@Param("onaydurumu") String onaydurumu);

    @Query ("MATCH (n:Proje) where n.projeTuru={projeturu}   RETURN n;")
    List<Proje> turegoregetir(@Param("projeturu") String projeturu);

    @Query ("MATCH (n:Proje) where n.onaydurumu={onaydurumu} AND n.projeTuru={projeturu}  RETURN n;")
    List<Proje> onayvedurum(@Param("onaydurumu") String onaydurumu ,@Param("projeturu") String projeturu);




}
