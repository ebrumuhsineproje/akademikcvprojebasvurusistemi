package tr.edu.akademikcvprojebasvurusistemi.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tr.edu.akademikcvprojebasvurusistemi.domain.Akademisyen;
import tr.edu.akademikcvprojebasvurusistemi.domain.Gorevli;

import tr.edu.akademikcvprojebasvurusistemi.domain.Lise;

/**
 * Created by LENOVO on 23.2.2017.
 */
@Repository
public interface GorevliRepository  extends GraphRepository<Gorevli> {
    @Query("MATCH (g:Gorevli) where g.email={email} RETURN g;")
    Gorevli gorevliGetir(@Param("email") String email);

    @Query("MATCH (g:Gorevli) where g.tc={tc} RETURN g;")
    Gorevli gorevligetir(@Param("tc") String tc);

}
