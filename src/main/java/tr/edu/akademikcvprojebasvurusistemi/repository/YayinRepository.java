package tr.edu.akademikcvprojebasvurusistemi.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tr.edu.akademikcvprojebasvurusistemi.domain.Akademisyen;
import tr.edu.akademikcvprojebasvurusistemi.domain.Lise;
import tr.edu.akademikcvprojebasvurusistemi.domain.Yayinlar;

import java.util.List;

/**
 * Created by LENOVO on 16.12.2016.
 */
@Repository
public interface YayinRepository extends GraphRepository<Yayinlar> {
    //akademisyene ait tek yayın getirir id ye gore
    @Query("MATCH (n:Akademisyen)-[:HAS_YAYIN]->(yayinlar:Yayinlar) where id(n)={akademisyenID} return yayinlar;")
    Yayinlar akademisyeneaityayinlariyayinGetir(@Param("akademisyenID")Long ID);

    //akademisyene ait tum yayınlar
    @Query("MATCH (n:Akademisyen)-[:HAS_YAYIN]->(yayinlar:Yayinlar) where id(n)={akademisyenID} return yayinlar;")
    List<Yayinlar> tumyayinlarigetir(@Param("akademisyenID")Long ID);

    @Query("MATCH (n:Akademisyen)-[:HAS_YAYIN]->(yayinlar:Yayinlar) where id(n)={akademisyenID} return yayinlar.yayinadi;")
    List<Yayinlar> tumyayinlarinadinigetir(@Param("akademisyenID")Long ID);

    //veritabanındaki tum yayınlar
    @Query("match (y:Yayinlar) return y;")
    List<Yayinlar> tumyayinlar();

    //id ye gore tek yayın getirir
    @Query("MATCH (y:Yayinlar) where id(y)={yayinID} return y;")
    Yayinlar yayingetir(@Param("yayinID")Long ID);


    //isme e göre yayın getir
    @Query ("MATCH (n:Yayinlar) where n.yayinadi={yayinadi} RETURN n;")
    List<Yayinlar> adagoregetir(@Param("yayinadi") String yayinadi);

    //türe gore getir
    @Query ("MATCH (n:Akademisyen)-[:HAS_YAYIN]->(y:Yayinlar) where y.yayinturu={yayinturu} and id(n)={akademisyenID} RETURN y;")
    List<Yayinlar> turegoregetir(@Param("yayinturu") String yayinturu,@Param("akademisyenID")Long id);
    //yayın var mı
    @Query("MATCH (n:Yayinlar) where n.yayinadi = {yayinadi} RETURN n;")
    Yayinlar yayinvarmi(@Param("yayinadi") String yayinadi);






}
