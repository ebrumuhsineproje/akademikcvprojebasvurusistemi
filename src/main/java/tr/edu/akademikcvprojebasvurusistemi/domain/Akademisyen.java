package tr.edu.akademikcvprojebasvurusistemi.domain;

import com.sun.javafx.scene.control.skin.VirtualFlow;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.neo4j.ogm.annotation.Relationship;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.springframework.stereotype.Repository;
import tr.edu.akademikcvprojebasvurusistemi.vaadin.extras.enums.Cinsiyet;

import java.util.ArrayList;
import java.util.List;


@NoArgsConstructor
@Getter
@Setter

public class Akademisyen extends Entity {
    public String adi, soyadi, tc,sifre,email,birim,bolum,anabilimdali,kadrounvani,ceptel,istel;

    @Relationship(type = "HAS_LISE", direction = Relationship.OUTGOING)
    //public Lise lise;
    public List<Lise> liseler=new ArrayList<>();

    @Relationship(type = "HAS_LISANS", direction = Relationship.OUTGOING)
    //public Lisans lisans;
    public List<Lisans> lisanslar=new ArrayList<>();

    @Relationship(type = "HAS_YUKSEKLISANS", direction = Relationship.OUTGOING)
    //public YuksekLisans yukseklisans;
    public List<YuksekLisans> yükseklisanslar=new ArrayList<>();

    @Relationship(type = "HAS_DOKTORA", direction = Relationship.OUTGOING)
    //public Doktora doktora;
    public List<Doktora> doktoralar=new ArrayList<>();

    @Relationship(type = "HAS_YAYIN", direction = Relationship.OUTGOING)
    public List<Yayinlar> yayinlarList= new ArrayList<>();
    
    @Relationship(type="HAS_PROJE",direction=Relationship.OUTGOING)
    public List<Proje> projeler=new ArrayList<>();
}
