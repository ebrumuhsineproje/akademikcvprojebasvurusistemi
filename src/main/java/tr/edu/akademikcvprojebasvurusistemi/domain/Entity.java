package tr.edu.akademikcvprojebasvurusistemi.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;


@NoArgsConstructor
@NodeEntity
public abstract class Entity {

    @Getter
    @GraphId
    Long id;

    public boolean equals(Object other) {
        if (this == other) return true;
        if (id == null) return false;
        if (!(other instanceof Entity)) return false;
        return id.equals(((Entity) other).id);
    }

    public int hashCode() {
        return id == null ? System.identityHashCode(this) : id.hashCode();
    }

}
