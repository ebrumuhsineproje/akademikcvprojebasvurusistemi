package tr.edu.akademikcvprojebasvurusistemi.domain;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.TextField;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by LENOVO on 16.12.2016.
 */
@NoArgsConstructor
@Getter
@Setter
public class Yayinlar extends Entity {
    public String yayinadi;
    public String ulusallik,yayinturu;

    public String btarih,dtur,mtur,mkonu,mdil,btur,dadi,mbaslik,cilt,sayi,no,mdoi,mlink,kdil,ktur,kkonu,krol,kyayinevigrup,kbaski,kyayinevi,kbaslik,ksayfa,kisbn,ksehir,kulke,kdoi,klink,kbasim,ksonbaski;

    Date simdikiZaman = new Date();

    //public  String yayinzamani=simdikiZaman.toString();


}

