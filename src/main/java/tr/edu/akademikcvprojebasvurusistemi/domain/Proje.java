/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.edu.akademikcvprojebasvurusistemi.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.neo4j.cypher.internal.frontend.v2_3.ast.functions.Str;
import org.neo4j.ogm.annotation.Relationship;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author ubuntu
 */
@NoArgsConstructor
@Getter
@Setter
public class Proje extends Entity{
    public String projeadi,ozeti,ar_geNiteligi,projeKapsamaAlani,projeTuru,sanayiIsbirligiDurumu
            ,paraBirimi,baslangic,bitis,ulke,sehir,projeButcesi,KurumKatkiPayi,
            firmaParaBirimi,firmaAdi,sagladigiFon;

    public String ProjePatenti,PatentAdi,PatentKodu,PatentYili,PatentKapsami,projeekibi;

    public String basvuruyapanadi,basvuruyapansoyadi;
    public  String onaydurumu,imzadurumu;

    Long simdikiZaman;


    @Relationship(type="HAS_EKIP",direction=Relationship.OUTGOING)
    public List<Akademisyen> ekiptekiler=new ArrayList<>();

}
