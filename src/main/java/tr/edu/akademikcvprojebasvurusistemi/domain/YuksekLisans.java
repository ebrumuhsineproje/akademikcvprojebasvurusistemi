/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.edu.akademikcvprojebasvurusistemi.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author LENOVO
 */
@NoArgsConstructor
@Getter
@Setter
public class YuksekLisans extends Entity{
    public  String  YuksekLisansuniv,YuksekLisansfakulte,YuksekLisansbolum,YuksekLisansbaslangic,YuksekLisansbitis;
}
