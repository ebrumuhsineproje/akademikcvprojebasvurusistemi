/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.edu.akademikcvprojebasvurusistemi;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.omg.CORBA.PUBLIC_MEMBER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.template.Neo4jOperations;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import tr.edu.akademikcvprojebasvurusistemi.domain.*;
import tr.edu.akademikcvprojebasvurusistemi.domain.Akademisyen;
import tr.edu.akademikcvprojebasvurusistemi.service.AkademisyenService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author LENOVO
 */
@ContextConfiguration(classes = {Neo4jConfigTest.class})
@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)

public class AkademisyenTest {
    @Autowired
    AkademisyenService akademisyenService;

    @Autowired
    Neo4jOperations neo4jOperations;

    /*
    @Before
    public void dummyDB() {
        Akademisyen akademisyen = new Akademisyen();
        akademisyen.setAdi("Cumhur");
        akademisyen.setSoyadi("KINACI");
        akademisyen.setTc("555");

        neo4jOperations.save(akademisyen);
    }*/

    @Test
    public void akademisyenEkleTest() {
        Akademisyen a = new Akademisyen();
        a.setAdi("Cumhur");
        a.setSoyadi("KINACI");
        a.setTc("555");
        assertEquals(true, akademisyenService.Control());
        assertEquals(a.getAdi(), akademisyenService.akademisyenEkle(a).getAdi());
        assertEquals(false, akademisyenService.Control());


    }

    @Test
    public void liseEklemeTest() {


        Lise lise = new Lise();
        lise.setLiseadi("deneme");
        Akademisyen akademisyen = new Akademisyen();
        akademisyen.setAdi("deneme");
        akademisyen.getLiseler().add(lise);

        akademisyenService.akademisyenEkle(akademisyen);

        assertEquals(lise.getLiseadi(),akademisyenService.liseGetir(akademisyen.getId()).getLiseadi());

    }

    @Test
    public void lisansEklemeTest() {

        Lisans lisans = new Lisans();
        lisans.setLisansuniv("deneme");
        Akademisyen akademisyen = new Akademisyen();
        akademisyen.setAdi("deneme");
        akademisyen.getLisanslar().add(lisans);
        akademisyenService.akademisyenEkle(akademisyen);

        assertEquals(lisans.getLisansuniv(),akademisyenService.lisansGetir(akademisyen.getId()).getLisansuniv());

    }
    @Test
    public void yukseklisansEklemeTest() {

        YuksekLisans yuksekLisans=new YuksekLisans();
        yuksekLisans.setYuksekLisansuniv("deneme");
        Akademisyen akademisyen = new Akademisyen();
        akademisyen.setAdi("deneme");
        akademisyen.getYükseklisanslar().add(yuksekLisans);
        akademisyenService.akademisyenEkle(akademisyen);

        assertEquals(yuksekLisans.getYuksekLisansuniv(),akademisyenService.yukseklisansGetir(akademisyen.getId()).getYuksekLisansuniv());



    }
    @Test
    public void doktoraEklemeTest() {

        Doktora doktora=new Doktora();
        doktora.setDoktorauniv("deneme");
        Akademisyen akademisyen = new Akademisyen();
        akademisyen.setAdi("deneme");
        akademisyen.getDoktoralar().add(doktora);
        akademisyenService.akademisyenEkle(akademisyen);

        assertEquals(doktora.getDoktorauniv(),akademisyenService.doktoraGetir(akademisyen.getId()).getDoktorauniv());

    }

    @Test
    public void getTumAkademisyenler() {
        Akademisyen akademisyen1= new Akademisyen();
        akademisyen1.setAdi("ebru");
        neo4jOperations.save(akademisyen1);
        assertEquals(1, akademisyenService.getTumAkademisyenler().size());
        Akademisyen akademisyen2 = new Akademisyen();
        akademisyen2.setAdi("Cumhur");
        akademisyen2.setSoyadi("KINACI");

        akademisyen2.setTc("555");

        neo4jOperations.save(akademisyen2);

        assertEquals(2, akademisyenService.getTumAkademisyenler().size());
    }


    @Test
    public void yayineklemeTest() {

        Yayinlar yayinlar=new Yayinlar();
        yayinlar.setYayinadi("deneme");
        Akademisyen akademisyen = new Akademisyen();
        akademisyen.setAdi("deneme");
        akademisyen.getYayinlarList().add(yayinlar);



        akademisyenService.akademisyenEkle(akademisyen);
        assertEquals(yayinlar.getYayinadi(),akademisyenService.yayinGetir(akademisyen.getId()).getYayinadi());
    }


    @Test
    public void yayinsilTest(){
        Yayinlar y=new Yayinlar();
        y.setYayinadi("deneme");
        Akademisyen a= new Akademisyen();
        a.setAdi("deneme");
        a.getYayinlarList().add(y);
        List<Yayinlar> yayinlistesi= new ArrayList<>();
        yayinlistesi.add(y);
        akademisyenService.akademisyenEkle(a);
        assertEquals(1,akademisyenService.gettumyayinlar(a.getId()).size());
        akademisyenService.yayinsil(yayinlistesi);
       //akademisyenService.yayiniliskisil(y.getId());
       //akademisyenService.yayinsil(y.getId());
        assertEquals(0,akademisyenService.gettumyayinlar(a.getId()).size());

    }

    @Test
    public  void yayingetirTest(){
        Yayinlar y=new Yayinlar();
        y.setYayinadi("deneme");
        Akademisyen a= new Akademisyen();
        a.setAdi("deneme");
        a.getYayinlarList().add(y);
        akademisyenService.akademisyenEkle(a);
        assertEquals(y.getYayinadi(),akademisyenService.yayingetir(y.getId()).getYayinadi());


    }
    @Test
    public  void yayinguncelleTest(){
        Yayinlar y= new Yayinlar();
        y.setYayinadi("yayin1");
        Akademisyen a= new Akademisyen();
        a.setAdi("ebru");
        a.getYayinlarList().add(y);
        akademisyenService.akademisyenEkle(a);
        akademisyenService.yayingetir(y.getId());
        assertEquals("yayin1",akademisyenService.yayingetir(y.getId()).getYayinadi());
        akademisyenService.yayinguncelle(y);
        //y.setYayinadi("yayin2");
        //a.getYayinlarList().add(y);
        //akademisyenService.akademisyenEkle(a);
        assertEquals("",akademisyenService.yayingetir(y.getId()).getYayinadi());



    }

    @Test
    public void gettumyayinlartest(){
        Yayinlar yayinlar=new Yayinlar();
        yayinlar.setYayinadi("deneme");
        Yayinlar yayinlar1=new Yayinlar();
        yayinlar.setYayinadi("deneme2");
        Akademisyen a=new Akademisyen();
        a.setAdi("ebru");
        a.getYayinlarList().add(yayinlar);
        a.getYayinlarList().add(yayinlar1);

        akademisyenService.akademisyenEkle(a);
        assertEquals(2,akademisyenService.gettumyayinlar(a.getId()).size());


    }



}
