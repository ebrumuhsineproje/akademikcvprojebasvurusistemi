package tr.edu.akademikcvprojebasvurusistemi;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tr.edu.comu.obs.kisisorgula.GetKisiRequest;
import tr.edu.comu.obs.kisisorgula.GetKisiResponse;
import tr.edu.comu.obs.kisisorgula.KisiSorgulaPort;
import tr.edu.comu.obs.kisisorgula.KisiSorgulaPortService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AkademikCvProjeBasvuruSistemiApplicationTests {

    @Test
    public void contextLoads () {
        KisiSorgulaPortService kisiSorgulaPortService = new KisiSorgulaPortService();
        KisiSorgulaPort kisiSorgulaPort = kisiSorgulaPortService.getKisiSorgulaPortSoap11();

        GetKisiRequest kisiRequest = new GetKisiRequest();
        kisiRequest.setKullaniciAdi("34471386028");
        String parolaMd5Li = DigestUtils.md5Hex("1uI{okdZ");
        kisiRequest.setParola(parolaMd5Li);

        GetKisiResponse kisiResponse = kisiSorgulaPort.getKisi(kisiRequest);
        System.out.println("Aradığınız kullanıcı : " + kisiResponse.isSonuc());
    }
}
